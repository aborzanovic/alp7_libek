<?php

namespace App\Providers;

use App\SeoPresenter;
use Cartalyst\Sentinel\Sentinel;
use Creitive\Asset\Factory as AssetFactory;
use Creitive\Dropzone\Dropzone;
use Creitive\Navigation\Factory as NavigationFactory;
use Creitive\Routing\LocaleResolver;
use Creitive\ViewData\ViewData;
use Illuminate\Support\ServiceProvider;

class ViewDataServiceProvider extends ServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        $this->configureViewData($this->app[ViewData::class]);
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {
    }

    /**
     * Configures all the basic view data properties.
     *
     * @param \Creitive\ViewData\ViewData $viewData
     * @return void
     */
    protected function configureViewData(ViewData $viewData)
    {
        $viewData->currentLanguage = $this->app[LocaleResolver::class]->getLanguage();
        $viewData->currentLocale = $this->app->getLocale();

        $viewData->breadcrumbs = $this->app['breadcrumbs'];
        $viewData->navigation = $this->app[NavigationFactory::class];
        $viewData->dropzone = $this->app[Dropzone::class];

        $viewData->assets = $this->app[AssetFactory::class];

        $viewData->pageTitle->setSiteName($this->app['translator']->get('common.siteName'));

        $viewData->meta->description = $this->app['translator']->get('common.metaDescription');

        $facebookAppId = $this->app['config']->get('facebook.appId');

        if ($facebookAppId) {
            $viewData->meta->og['fb:app_id'] = $facebookAppId;
        }

        $viewData->setSeo(new SeoPresenter(
            $this->app['translator']->get('common.siteName'),
            $this->app->getLocale(),
            $this->app['translator']->get('common.metaDescription'),
            $this->app['url']->to('/images/default/og.png')
        ));
    }
}

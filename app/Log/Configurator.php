<?php

namespace App\Log;

use Creitive\Monolog\Processor\ExtraDataProcessor;
use Illuminate\Contracts\Foundation\Application;
use InvalidArgumentException;
use Monolog\Handler\LogglyHandler;
use Monolog\Logger;
use Monolog\Processor\GitProcessor;

class Configurator
{
    /**
     * Configures the logging mechanism.
     *
     * @return void
     */
    public function configure(Logger $monolog, Application $app)
    {
        $this->addGitProcessor($monolog);
        $this->addExtraDataProcessor($monolog, $app);

        $this->configureSingleHandler($app);

        if ($logglyToken = $app['config']->get('loggly.token')) {
            $logglyTag = $app['config']->get('loggly.tag');

            $this->configureLogglyHandler($monolog, $logglyToken, $logglyTag);
        }
    }

    /**
     * Adds a Git processor to the Monolog instance.
     *
     * This adds the current Git branch/commit info to the log.
     *
     * @param \Monolog\Logger $monolog
     */
    protected function addGitProcessor(Logger $monolog)
    {
        $monolog->pushProcessor(new GitProcessor);
    }

    /**
     * Adds an extra data processor to the Monolog instance.
     *
     * This adds various metadata about the current request and/or execution
     * context.
     *
     * Note that artisan commands will always log the URL configured as the
     * `app.url`, but we can use the `console` entry to identify those.
     *
     * @param \Monolog\Logger $monolog
     * @param \Illuminate\Contracts\Foundation\Application $app
     */
    protected function addExtraDataProcessor(Logger $monolog, Application $app)
    {
        $extraDataProcessor = new ExtraDataProcessor([
            'environment' => $app->environment(),
            'console' => $app->runningInConsole(),
        ]);

        $monolog->pushProcessor($extraDataProcessor);

        $app->instance('log.extraDataProcessor', $extraDataProcessor);
    }

    /**
     * Configures a single log file.
     *
     * @param \Illuminate\Contracts\Foundation\Application $app
     * @return void
     */
    protected function configureSingleHandler(Application $app)
    {
        $app['log']->useFiles($app->storagePath().'/logs/laravel.log');
    }

    /**
     * Configures the Loggly handler.
     *
     * @param \Monolog\Logger $monolog
     * @param string $token
     * @param string $tag
     * @return void
     */
    public function configureLogglyHandler(Logger $monolog, $token, $tag)
    {
        if (!$tag) {
            throw new InvalidArgumentException('A Loggly tag must be configured in order to use logging!');
        }

        $handler = new LogglyHandler($token);
        $handler->setTag($tag);

        $monolog->pushHandler($handler);
    }
}

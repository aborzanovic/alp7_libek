<?php

namespace App\Log;

use Illuminate\Support\ServiceProvider;

class ExtraDataServiceProvider extends ServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $request = $this->app['request'];

        $this->app['log.extraDataProcessor']->addExtraData([
            'httpMethod' => $request->method(),
            'url' => $request->url(),
            'clientIps' => $request->getClientIps(),
            'referrer' => $request->server('HTTP_REFERER'),
            'userAgent' => $request->header('User-Agent'),
        ]);
    }
}

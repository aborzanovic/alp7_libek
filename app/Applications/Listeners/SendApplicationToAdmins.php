<?php

namespace App\Applications\Listeners;

use App\Applications\Events\ApplicationWasSubmitted;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Contracts\Mail\Mailer;
use Str;

class SendApplicationToAdmins
{
    /**
     * A Filesystem implementation.
     *
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    protected $filesystem;

    /**
     * A Mailer implementation.
     *
     * @var \Illuminate\Contracts\Mail\Mailer
     */
    protected $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Filesystem $filesystem, Mailer $mailer)
    {
        $this->filesystem = $filesystem;
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param \App\Applications\Events\ApplicationWasSubmitted $event
     * @return void
     */
    public function handle(ApplicationWasSubmitted $event)
    {
        $application = $event->application;
        $data = $application->data;
        $filename = $data['cv'];
        $contents = $this->filesystem->get($filename);

        $random = Str::random(8);

        $subject = trans('applications.email.subject', ['random' => $random]);

        $viewData = [
            'application' => $event->application,
            'data' => $event->application->data,
            'subject' => $subject,
        ];

        $this->mailer->send(
            'applications.emails.admin',
            $viewData,
            function ($message) use ($data, $subject, $filename, $contents) {
                $toAddress = trans('applications.email.to.address');
                $toName = trans('applications.email.to.name');

                $message
                    ->to($toAddress, $toName)
                    ->replyTo($data['email'], $data['fullName'])
                    ->attachData($contents, $filename)
                    ->subject($subject);
            }
        );
    }
}

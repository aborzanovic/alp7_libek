<?php

namespace App\Applications\Application;

use App\Applications\Application;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\Filesystem;
use Str;

class Repository
{
    /**
     * An ApplicationModel instance.
     *
     * @var \App\Applications\Application
     */
    protected $applicationModel;

    /**
     * A Filesystem implementation.
     *
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    protected $filesystem;

    public function __construct(Application $applicationModel, Filesystem $filesystem)
    {
        $this->applicationModel = $applicationModel;
        $this->filesystem = $filesystem;
    }

    /**
     * Creates a new application and returns it.
     *
     * @param array $inputData
     * @return \App\Applications\Application
     */
    public function create(array $inputData)
    {
        return $this->populateAndSave($this->applicationModel->newInstance(), $inputData);
    }

    /**
     * Populates the passed instance with the input data.
     *
     * @param \App\Applications\Application $application
     * @param array $inputData
     * @return \App\Applications\Application
     */
    protected function populate(Application $application, array $inputData)
    {
        $now = Carbon::now();

        $cv = $inputData['cv'];
        $uploadedPathname = $cv->getPathname();
        $filename = $now->format('U').'-'.Str::random(8).'.'.$cv->guessExtension();
        $contents = file_get_contents($uploadedPathname);
        $this->filesystem->put($filename, $contents);

        if (is_uploaded_file($uploadedPathname)) {
            unlink($uploadedPathname);
        }

        $application->data = [
            'fullName' => array_get($inputData, 'full_name'),
            'birthdate' => array_get($inputData, 'birthdate'),
            'sex' => array_get($inputData, 'sex'),
            'cv' => $filename,
            'email' => array_get($inputData, 'email'),
            'phone' => array_get($inputData, 'phone'),
            'faculty' => array_get($inputData, 'faculty'),
            'areaOfStudy' => array_get($inputData, 'area_of_study'),
            'history' => array_get($inputData, 'history'),
            'impressions' => array_get($inputData, 'impressions'),
            'libekHistory' => array_get($inputData, 'libek_history'),
            'memberships' => array_get($inputData, 'memberships'),
            'problems' => array_get($inputData, 'problems'),
            'solutions' => array_get($inputData, 'solutions'),
            'future' => array_get($inputData, 'future'),
            'ego' => array_get($inputData, 'ego'),
            'expectations' => array_get($inputData, 'expectations'),
            'reference' => array_get($inputData, 'reference'),
            'needs' => array_get($inputData, 'needs'),
        ];

        $application->submitted_at = $now;

        return $application;
    }

    /**
     * Populates the passed instance with the input data, saves it and returns
     * it.
     *
     * @param \App\Applications\Application $application
     * @param array $inputData
     * @return \App\Applications\Application
     */
    protected function populateAndSave(Application $application, array $inputData)
    {
        $application = $this->populate($application, $inputData);

        $application->save();

        return $application;
    }
}

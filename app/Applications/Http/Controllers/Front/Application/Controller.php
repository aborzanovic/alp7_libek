<?php

namespace App\Applications\Http\Controllers\Front\Application;

use App\Applications\Application\Repository as ApplicationRepository;
use App\Applications\Events\ApplicationWasSubmitted;
use App\Applications\Http\Requests\Front\Application\StoreRequest;
use App\Http\Controllers\Front\Controller as BaseController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Controller extends BaseController
{
    /**
     * Displays the application signup form.
     *
     * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
     * @return \Illuminate\View\View
     */
    public function index(SessionInterface $session)
    {
        if ($session->get('successfulApplication')) {
            return view('applications.success');
        }

        return view('applications.index');
    }

    /**
     * Stores the submitted application.
     *
     * @param \App\Applications\Http\Requests\Front\Application\StoreRequest $request
     * @param \App\Applications\Application\Repository $applicationRepository
     * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
     * @return \Illuminate\View\View
     */
    public function store(StoreRequest $request, ApplicationRepository $applicationRepository, SessionInterface $session)
    {
        $application = $applicationRepository->create($request->all());

        event(new ApplicationWasSubmitted($application));

        $session->flash('successfulApplication', true);

        return redirect()->action(static::class.'@index');
    }
}

<?php

namespace App\Applications\Http\Requests\Front\Application;

use App\Http\Requests\Request;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'email'],
            'full_name' => ['required'],
            'cv' => ['required', 'mimes:pdf,doc,docx,odt', 'max:10240'],
        ];
    }
}

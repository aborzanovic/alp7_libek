<?php

namespace App\Redactor;

use Creitive\Module\ServiceProvider as BaseServiceProvider;
use Illuminate\Contracts\Routing\Registrar as RegistrarContract;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        $this->registerAdminRoutes($this->app['router']);
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {
    }

    /**
     * Registers admin panel routes.
     *
     * @param \Illuminate\Contracts\Routing\Registrar $router
     * @return void
     */
    protected function registerAdminRoutes(RegistrarContract $router)
    {
        $attributes = [
            'prefix' => 'admin/redactor',
            'middleware' => ['auth', 'permissions'],
            'namespace' => 'App\Redactor\Http\Controllers\Admin',
        ];

        $router->group($attributes, function (RegistrarContract $router) {
            $router->post('files', 'File\Controller@store');
            $router->post('images', 'Image\Controller@store');
        });
    }
}

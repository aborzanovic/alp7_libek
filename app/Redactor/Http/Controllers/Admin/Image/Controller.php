<?php

namespace App\Redactor\Http\Controllers\Admin\Image;

use App\Http\Controllers\Admin\Controller as BaseController;
use App\Redactor\Http\Requests\Admin\Image\StoreRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Controller extends BaseController
{
    /**
     * The path to which the images will be uploaded.
     *
     * @var string
     */
    protected $uploadPath;

    public function __construct($uploadPath = '/upload/images/')
    {
        $this->uploadPath = $uploadPath;
    }

    /**
     * Uploads an image to the server, and returns its location.
     *
     * The location is returned as an URL within a `filelink` attribute of a
     * JSON object, which is what Redactor expects.
     *
     * @param \App\Redactor\Http\Requests\Admin\Image\StoreRequest $request
     * @return array
     */
    public function store(StoreRequest $request)
    {
        $file = $request->file('file');

        $parameters = $this->getDestination($file);

        $file->move($parameters['destination'], $parameters['name']);

        return [
            'filelink' => $parameters['imgSrc'],
        ];
    }

    /**
     * Generates a new filename for uploading files.
     *
     * Returns an array with the following keys:
     *
     * - `destination`: the path where the file should be moved
     * - `name`: the name that should be given to the file
     * - `imgSrc`: the string which should be used as the `src` in `<img>`
     *   elements when outputting HTML
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return array
     */
    protected function getDestination(UploadedFile $file)
    {
        $randomFileName = time() . '.' . str_random(16) . '.' . $file->guessExtension();

        return [
            'destination' => public_path() . $this->uploadPath,
            'name' => $randomFileName,
            'imgSrc' => $this->uploadPath . $randomFileName,
        ];
    }
}

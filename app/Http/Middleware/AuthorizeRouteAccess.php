<?php

namespace App\Http\Middleware;

use Cartalyst\Sentinel\Sentinel;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Checks if the user has permissions to access the current action, and throws
 * an `AccessDeniedHttpException` if they don't.
 */
class AuthorizeRouteAccess
{

    /**
     * The Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $sentinel;

    /**
     * Create a new middleware instance.
     *
     * @param \Cartalyst\Sentinel\Sentinel $sentinel
     * @return void
     */
    public function __construct(Sentinel $sentinel)
    {
        $this->sentinel = $sentinel;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     */
    public function handle(Request $request, Closure $next)
    {
        $route = $request->route();

        if (!$this->sentinel->hasAccess($route->getActionName())) {
            throw new AccessDeniedHttpException;
        }

        return $next($request);
    }
}

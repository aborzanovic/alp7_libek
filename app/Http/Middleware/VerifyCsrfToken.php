<?php

namespace App\Http\Middleware;

use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];

     /**
     * {@inheritDoc}
     */
    protected function shouldPassThrough($request)
    {
        if (parent::shouldPassThrough($request)) {
            return true;
        }

        return false;
    }
}

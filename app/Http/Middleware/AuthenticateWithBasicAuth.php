<?php

namespace App\Http\Middleware;

use Cartalyst\Sentinel\Sentinel;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Performs basic HTTP authentication.
 */
class AuthenticateWithBasicAuth
{
    /**
     * The Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $sentinel;

    /**
     * Create a new middleware instance.
     *
     * @param \Cartalyst\Sentinel\Sentinel $sentinel
     * @return void
     */
    public function __construct(Sentinel $sentinel)
    {
        $this->sentinel = $sentinel;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function handle(Request $request, Closure $next)
    {
        $this->sentinel->creatingBasicResponse(function () {
            $headers = ['WWW-Authenticate' => 'Basic'];

            return new Response('Invalid credentials.', 401, $headers);
        });

        $response = $this->sentinel->basic();

        if ($response !== null) {
            return $response;
        }

        return $next($request);
    }
}

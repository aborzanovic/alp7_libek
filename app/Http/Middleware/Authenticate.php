<?php

namespace App\Http\Middleware;

use Cartalyst\Sentinel\Sentinel;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Makes sure the user is authenticated.
 *
 * If they are, the request is passed on.
 *
 * Otherwise, the user will be redirected to the `/admin/login` path (this is
 * currently hard-coded), or throws an `AccessDeniedHttpException` in case of an
 * AJAX request.
 */
class Authenticate
{
    /**
     * The Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $sentinel;

    /**
     * Create a new middleware instance.
     *
     * @param \Cartalyst\Sentinel\Sentinel $sentinel
     * @return void
     */
    public function __construct(Sentinel $sentinel)
    {
        $this->sentinel = $sentinel;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->sentinel->guest()) {
            if ($request->ajax()) {
                throw new AccessDeniedHttpException;
            } else {
                return redirect()->guest('/admin/login');
            }
        }

        return $next($request);
    }
}

<?php namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Sets the base assets (scripts and styles).
     *
     * @return void
     */
    public function setBaseAssets()
    {
        $this->assets->loadAssetsForFront();
    }
}

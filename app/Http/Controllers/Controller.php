<?php

namespace App\Http\Controllers;

use App\AssetManager\AssetManager;
use Creitive\ViewData\ViewData;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->viewData = app(ViewData::class);
        $this->assets = app(AssetManager::class);

        $this->setBaseAssets();
    }

    /**
     * Sets the base assets (scripts and styles).
     *
     * @return void
     */
    abstract public function setBaseAssets();
}

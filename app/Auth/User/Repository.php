<?php

namespace App\Auth\User;

use App\Auth\User;
use Cartalyst\Sentinel\Sentinel;
use Illuminate\Database\Eloquent\Builder;
use Str;

class Repository
{
    /**
     * A User model instance.
     *
     * @var \App\Auth\User
     */
    protected $userModel;

    /**
     * A Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $sentinel;

    public function __construct(User $userModel, Sentinel $sentinel)
    {
        $this->userModel = $userModel;
        $this->sentinel = $sentinel;
    }

    /**
     * Gets all users.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->userModel->all();
    }

    /**
     * Gets user by email.
     *
     * @param string $email
     * @return \App\Auth\User
     */
    public function findByEmail($email)
    {
        return $this->userModel->where('email', $email)->get()->first();
    }

    /**
     * Finds the user by ID, or throws an exception if the ID doesn't exist.
     *
     * @param mixed $id
     * @return \App\Auth\User
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findOrFail($id)
    {
        return $this->userModel->findOrFail($id);
    }

    /**
     * Gets all users with any of the specified roles.
     *
     * @param array $roles
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getUsersWithAnyRoles(array $roles = [])
    {
        if (empty($roles)) {
            return $this->getUsersWithoutRoles();
        }

        /*
         * The nesting below may seem a bit weird, but it's actually there so as
         * to generate the correct query with regards to the grouping of `AND`
         * and `OR` conditions.
         */
        return $this->userModel->whereHas('roles', function (Builder $query) use ($roles) {
            return $query->where(function (Builder $query) use ($roles) {
                foreach ($roles as $role) {
                    $query->orWhere('slug', $role);
                }

                return $query;
            });
        })->get();
    }

    /**
     * Returns all users that have no roles.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getUsersWithoutRoles()
    {
        return $this->userModel->withoutRoles()->get();
    }

    /**
     * Creates and activates a new user and returns it.
     *
     * @param array $inputData
     * @return \App\Auth\User
     */
    public function create(array $inputData)
    {
        $userConfig = [
            'email' => array_get($inputData, 'email', ''),
            'password' => array_get($inputData, 'password', Str::random(8)),
            'first_name' => array_get($inputData, 'first_name', ''),
            'last_name' => array_get($inputData, 'last_name', '')
        ];

        $user = $this->sentinel->registerAndActivate($userConfig);

        $role = $this->sentinel->findRoleBySlug($inputData['role']);
        $role->users()->attach($user);

        return $user;
    }

    /**
     * Updates the passed user and returns it.
     *
     * @param \App\Auth\User $user
     * @param array $inputData
     * @return \App\Auth\User
     */
    public function update(User $user, array $inputData)
    {
        $userConfig = [
            'email' => array_get($inputData, 'email', $user->email),
            'first_name' => array_get($inputData, 'first_name', $user->first_name),
            'last_name' => array_get($inputData, 'last_name', $user->last_name),
        ];

        if (!empty($inputData['password'])) {
            $userConfig['password'] = $inputData['password'];
        }

        $this->sentinel->update($user, $userConfig);

        if (!empty($inputData['role'])) {
            $role = $this->sentinel->findRoleBySlug($inputData['role']);
            $user->roles()->sync([$role->id]);
        }

        return $user;
    }

    /**
     * Deletes the passed user from the system.
     *
     * @param \App\Auth\User $user
     * @return bool|null
     */
    public function delete(User $user)
    {
        return $user->delete();
    }

    /**
     * Changes the password for a user and returns it.
     *
     * @param \App\Auth\User $user
     * @param string $password
     * @return \App\Auth\User
     */
    public function setPassword(User $user, $password)
    {
        $credentials = [
            'password' => $password,
        ];

        $this->sentinel->update($user, $credentials);

        return $user;
    }
}

<?php

namespace App\Auth\Console;

use App\Auth\User;
use Cartalyst\Sentinel\Sentinel;
use Illuminate\Console\Command;
use Str;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class CreateUserCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new user';

    /**
     * A Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $sentinel;

    /**
     * A User model instance.
     *
     * @var \App\Auth\User
     */
    protected $userModel;

    public function __construct(Sentinel $sentinel, User $userModel)
    {
        parent::__construct();

        $this->sentinel = $sentinel;
        $this->userModel = $userModel;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        if ($this->userModel->whereEmail($this->argument('email'))->exists()) {
            $this->error('The user with the specified email already exists!');

            return 1;
        }

        if (!filter_var($this->argument('email'), FILTER_VALIDATE_EMAIL)) {
            $this->error('The provided email address is not valid!');

            return 1;
        }

        return $this->registerUser(
            $this->argument('email'),
            $this->argument('password'),
            !$this->option('inactive')
        );
    }

    /**
     * Registers the user with the provided credentials.
     *
     * If `$password` is set to `null`, a random 32-character password will be
     * generated.
     *
     * If `$activate` is set to `true`, the user will be automatically
     * activated; otherwise, they will have to manually go through the
     * activation process.
     *
     * @param string $email
     * @param string|null $password
     * @param boolean $activate
     * @return integer
     */
    public function registerUser($email, $password = null, $activate = true)
    {
        $password = $password ?: Str::random(32);

        $credentials = [
            'email' => $email,
            'password' => $password,
        ];

        $user = $this->sentinel->register($credentials, $activate);

        if ($user) {
            if ($activate) {
                $this->info("Created and activated user {$email} with password: {$password}");
            } else {
                $this->info("Created user {$email} with password: {$password}");
                $this->info('Please activate the user manually.');
            }
        } else {
            $this->error('Unable to create the specified user.');
        }

        return $user ? 0 : 1;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['email', InputArgument::REQUIRED, 'Email address'],
            ['password', InputArgument::OPTIONAL, 'Password (if one is not provided, a random 32-character string will be generated)'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['inactive', null, InputOption::VALUE_NONE, 'Use this flag to force manual user activation'],
        ];
    }
}

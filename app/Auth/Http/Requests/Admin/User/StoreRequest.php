<?php

namespace App\Auth\Http\Requests\Admin\User;

use App\Auth\Role\Repository as RoleRepository;
use App\Http\Requests\Request;
use Cartalyst\Sentinel\Sentinel;
use Illuminate\Validation\Factory as ValidationFactory;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param \App\Auth\Role\Repository $roleRepository
     * @return array
     */
    public function rules(RoleRepository $roleRepository)
    {
        $rules = [
            'email' => ['required', 'email', 'unique:users'],
            'first_name' => ['required'],
            'last_name' => ['required'],
            'password' => ['required'],
            'role' => ['required'],
        ];

        $roles = implode(',', array_keys($roleRepository->getOptions()));
        $rules['role'][] = "in:{$roles}";

        return $rules;
    }
}

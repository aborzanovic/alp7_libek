<?php

namespace App\Auth\Http\Requests\Admin\Password;

use App\Http\Requests\Request;
use Cartalyst\Sentinel\Sentinel;
use Illuminate\Validation\Factory as ValidationFactory;

class UpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => ['required', 'current_password'],
            'new_password' => ['required', 'confirmed'],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function validator(Sentinel $sentinel, ValidationFactory $factory)
    {
        $input = $this->all();

        $input['real_current_password'] = $sentinel->getUser()->password;

        return $factory->make(
            $input,
            $this->container->call([$this, 'rules']),
            $this->messages(),
            $this->attributes()
        );
    }

    /**
     * {@inheritDoc}
     */
    public function messages()
    {
        return array_dot(trans('admin/password.errorMessages.update'));
    }
}

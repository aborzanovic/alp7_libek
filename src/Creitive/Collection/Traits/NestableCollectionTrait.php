<?php

namespace Creitive\Collection\Traits;

trait NestableCollectionTrait
{
    /**
     * {@inheritDoc}
     */
    public function nest()
    {
        $rootCollection = new static;

        foreach ($this as $item) {
            /*
             * Initializes the children on the item as an empty collection, in
             * case it hasn't been initialized yet.
             */
            $item->initializeChildren();

            /*
             * If the item has no parent, it belongs in the root collection.
             */
            if (!$item->hasParent()) {
                $item->setParent(null);

                $rootCollection->push($item);

                continue;
            }

            /*
             * Otherwise, we need to find the parent so we could set the mutual
             * relationship.
             */
            $parent = $this->find($item->parent_id);

            /*
             * In case we haven't found the parent, this is likely an error, for
             * example if the parent item hasn't been fetched from the database.
             *
             * However, in order to avoid dereferencing null pointers, we'll
             * just continue, and the item won't be added to the final nested
             * collection.
             */
            if ($parent === null) {
                continue;
            }

            /*
             * Finally, set the relationships between the items.
             */
            $item->setParent($parent);
            $parent->addChild($item);
        }

        return $rootCollection;
    }

    /**
     * Recursively finds a model in the nestable collection by key.
     *
     * @param mixed $key
     * @param mixed $default
     * @return mixed
     */
    public function find($key, $default = null)
    {
        $item = parent::find($key, null);

        if (!is_null($item)) {
            return $item;
        }

        foreach ($this as $item) {
            $child = $item->children->find($key);

            if (!is_null($child)) {
                return $child;
            }
        }

        return $default;
    }

    /**
     * Returns a flattened array of primary keys.
     *
     * @return array
     */
    public function modelKeys()
    {
        $keys = [];

        foreach ($this as $item) {
            $keys[] = $item->getKey();

            if ($item->hasChildren()) {
                $keys = array_merge($keys, $item->children->modelKeys());
            }
        }

        return $keys;
    }
}

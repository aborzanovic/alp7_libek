<?php

namespace Creitive\Collection;

interface NestableSluggableCollectionInterface
{
    /**
     * Finds the item in the nested collection with the specified slug path,
     * starting from the root level.
     *
     * Throws a `LengthException` if an empty array is passed.
     *
     * Returns the default value if the item isn't found.
     *
     * @param array $path
     * @param mixed $default
     * @return \Creitive\Models\NestableSluggableInterface|mixed
     * @throws \LengthException
     */
    public function findBySlugPath(array $path, $default = null);
}

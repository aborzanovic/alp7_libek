<?php

namespace Creitive\Support;

class Filesystem
{
    /**
     * Ensures that the specified path can be written to, optionally creating it
     * if it does not yet exist, but can be created. Returns `false` if the path
     * is either unwriteable or it doesn't exist and cannot be created; returns
     * `true` otherwise.
     *
     * @param string $path
     * @param boolean $createIfNotExists
     * @param integer $mode
     * @return boolean
     */
    public static function ensureWritablePath($path = '', $createIfNotExists = false, $mode = 0777)
    {
        if (is_writable($path)) {
            return true;
        }

        if (!$createIfNotExists) {
            return false;
        }

        return mkdir($path, $mode, true);
    }
}

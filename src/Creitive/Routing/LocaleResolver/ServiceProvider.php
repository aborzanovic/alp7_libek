<?php

namespace Creitive\Routing\LocaleResolver;

use Creitive\Routing\LocaleResolver;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->app->singleton(LocaleResolver::class, function ($app) {
            $availableLocales = $app['config']->get('app.locales');
            $defaultLocale = $app['config']->get('app.locale');

            return new LocaleResolver($availableLocales, $defaultLocale);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        $this->setLocaleByUrl();
    }

    /**
     * Sets the application locale according to the first URL segment.
     *
     * @return void
     */
    protected function setLocaleByUrl()
    {
        $localeResolver = $this->app->make(LocaleResolver::class);

        $firstSegment = $this->app['request']->segment(1);

        $locale = $localeResolver->getLocaleByLanguage($firstSegment);

        if (!is_null($locale)) {
            $this->app->setLocale($locale);
            $localeResolver->setLocale($locale);
        }
    }
}

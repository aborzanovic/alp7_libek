<?php

namespace Creitive\ViewData;

use Config;
use Creitive\Html\CssClassHandler;
use Creitive\Html\PageTitle;
use Creitive\Html\SeoPresenter;
use stdClass;

class ViewData
{
    /**
     * The title of the current page.
     *
     * @var \Creitive\Html\PageTitle
     */
    public $pageTitle;

    /**
     * The `[data-page]` attribute embedded into the `<body>` element.
     *
     * @var string
     */
    public $bodyDataPage;

    /**
     * The CSS classes to be applied to the `<body>` element.
     *
     * @var string
     */
    public $bodyClasses;

    /**
     * The current user.
     *
     * Should be attached later in the app lifecycle, via a middleware.
     *
     * @var \App\Auth\User|null
     */
    public $currentUser;

    public function __construct()
    {
        $this->pageTitle = new PageTitle;
        $this->bodyClasses = new CssClassHandler();
        $this->bodyDataPage = '';

        $this->meta = new stdClass;
        $this->meta->description = '';
        $this->meta->opengraph = [];
        $this->meta->applinks = [];

        $this->currentUser = null;
    }

    /**
     * Sets the SEO tags.
     *
     * @param \Creitive\Html\SeoPresenter $presenter
     */
    public function setSeo(SeoPresenter $presenter)
    {
        $this->meta->opengraph['og:type'] = $presenter->getType();

        if ($presenter->hasSiteName()) {
            $this->meta->opengraph['og:site_name'] = $presenter->getSiteName();
        }

        if ($presenter->hasLocale()) {
            $this->meta->opengraph['og:locale'] = $presenter->getLocale();
        }

        if ($presenter->hasTitle()) {
            $title = $presenter->getTitle();

            if ($presenter->overridesTitle()) {
                $this->pageTitle->setOverride($title);
            } else {
                $this->pageTitle->setPage($title);
            }

            $this->meta->opengraph['og:title'] = $this->pageTitle->render();
        }

        if ($presenter->hasDescription()) {
            $description = $presenter->getDescription();

            $this->meta->description = $description;
            $this->meta->opengraph['og:description'] = $description;
        }

        if ($presenter->hasImage()) {
            $this->meta->opengraph['og:image'] = $presenter->getImage();
        }
    }

    /**
     * Sets the App Links protocol configuration.
     *
     * @param string|null $iosUrl
     * @return void
     */
    public function setApplinks($iosUrl = null)
    {
        if ($iosUrl !== null) {
            $this->meta->applinks['al:ios:url'] = $iosUrl;
            $this->meta->applinks['al:ios:app_store_id'] = Config::get('applinks.ios.appStoreId');
            $this->meta->applinks['al:ios:app_name'] = Config::get('applinks.ios.appName');
        }

        $this->meta->applinks['al:web:should_fallback'] = 'true';
    }
}

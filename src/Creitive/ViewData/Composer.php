<?php

namespace Creitive\ViewData;

use Creitive\ViewData\ViewData;
use Illuminate\View\View;

/**
 * This composer forwards some global view data into *all* loaded views.
 *
 * To attach data to this, use the `BaseController::viewData property`, which is
 * instantiated via `App::make('viewData')`, - since it's a singleton (as
 * defined in `Creitive\ViewData\ViewDataServiceProvider`), the same object will
 * be used everywhere.
 */
class Composer
{
    /**
     * A view data instance.
     *
     * @var \Creitive\ViewData\ViewData
     */
    protected $viewData;

    public function __construct(ViewData $viewData)
    {
        $this->viewData = $viewData;
    }

    /**
     * Composes a view.
     *
     * @param \Illuminate\View\View $view
     * @return void
     */
    public function compose(View $view)
    {
        foreach ((array) $this->viewData as $key => $value) {
            if (!isset($view[$key])) {
                $view->with($key, $value);
            }
        }
    }
}

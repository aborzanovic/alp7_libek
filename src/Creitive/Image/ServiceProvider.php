<?php

namespace Creitive\Image;

use Creitive\Image\Uploader;
use Creitive\Image\Uploader\FileArgumentParser;
use Creitive\Image\Uploader\FilenameManager;
use Creitive\Image\Uploader\ImageProcessor;
use Creitive\Image\UrlGenerator;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Imagick;
use PHPExif\Reader\Reader as ExifReader;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->fixImagickSegfault();
        $this->registerImageUploader();
        $this->registerImageProcessor();
        $this->registerUrlGenerator();
    }

    /**
     * Fixes Imagick causing PHP segfaults due to some PHP threading issues.
     *
     * The bug is confusing because any image manipulation usually finishes
     * without problems, but PHP only segfaults when finishing script execution.
     * So a user might see the "Segmentation fault" message and think that
     * something's wrong, but any generated images will in fact be created
     * correctly.
     *
     * See the linked bug reports for more info.
     *
     * @see https://bugs.php.net/bug.php?id=59752
     * @see https://bugs.php.net/bug.php?id=61122
     * @return void
     */
    protected function fixImagickSegfault()
    {
        /*
         * The bug only affects CLI execution, so if we are not running a
         * console application, we'll just skip this.
         */
        if (!$this->app->runningInConsole()) {
            return;
        }

        /*
         * This sets the limit to the number of threads used by `Imagick` to a
         * single thread. The magic number 6 references the thread resource, but
         * depending on which ImageMagick version the Imagick extension was
         * compiled against, the related constant `Imagick::RESOURCETYPE_THREAD`
         * might not be available. To avoid having to manually detect this,
         * we'll just hard-code the constant's actual value into the method
         * call.
         *
         * Additionally, since this is a non-static method, we need to
         * instantiate an object and call it on that instance.
         *
         * @see http://php.net/manual/en/imagick.constants.php#imagick.constants.resourcetypes
         * @see http://php.net/manual/en/imagick.setresourcelimit.php#106419
         */
        $image = new Imagick;
        $image->setResourceLimit(6, 1);
        $image->clear();
    }

    /**
     * Registers the image uploader instance.
     *
     * @return void
     */
    protected function registerImageUploader()
    {
        $this->app->singleton(Uploader::class, function ($app) {
            return new Uploader(
                $app[FileArgumentParser::class],
                $app[ImageProcessor::class],
                $app[ExifReader::class]
            );
        });
    }

    /**
     * Registers the image processor instance.
     *
     * @return void
     */
    protected function registerImageProcessor()
    {
        $this->app->singleton(ImageProcessor::class, function ($app) {
            return new ImageProcessor(
                $app,
                $app[FilenameManager::class],
                $app['filesystem']->drive('uploads.images')
            );
        });
    }

    /**
     * Registers the image URL generator.
     *
     * @return void
     */
    protected function registerUrlGenerator()
    {
        $this->app->singleton(UrlGenerator::class, function ($app) {
            return new UrlGenerator('upload/images', $app['url']);
        });
    }
}

<?php

namespace Creitive\Image\Facades;

use Creitive\Image\Uploader;
use Illuminate\Support\Facades\Facade;

class ImageUploader extends Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return Uploader::class;
    }
}

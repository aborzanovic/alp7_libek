<?php

namespace Creitive\Image;

use Illuminate\Contracts\Routing\UrlGenerator as BaseUrlGenerator;

class UrlGenerator
{
    /**
     * The root path to the images.
     *
     * @var string
     */
    protected $rootPath;

    /**
     * An UrlGenerator instance.
     *
     * @var \Illuminate\Contracts\Routing\UrlGenerator
     */
    protected $urlGenerator;

    public function __construct($rootPath, BaseUrlGenerator $urlGenerator)
    {
        $this->rootPath = $rootPath;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * Generates the URL for the image.
     *
     * @param string $filename
     * @return string
     */
    public function generate($filename)
    {
        return $this->urlGenerator->to("/{$this->rootPath}/{$filename}");
    }
}

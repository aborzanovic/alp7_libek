<?php

namespace Creitive\Image;

use Creitive\Image\Uploader\FileArgumentParser;
use Creitive\Image\Uploader\ImageProcessor;
use Creitive\Support\Filesystem;
use Exception;
use Imagick;
use PHPExif\Reader\Reader as ExifReader;

class Uploader
{
    /**
     * The parser for the file argument.
     *
     * @todo Add getter and setter
     * @var \Creitive\Image\Uploader\FileArgumentParser
     */
    protected $fileArgumentParser;

    /**
     * The image processor.
     *
     * @todo Add getter and setter
     * @var \Creitive\Image\Uploader\ImageProcessor
     */
    protected $imageProcessor;

    /**
     * An ExifReader instance.
     *
     * @var \PHPExif\Reader\Reader
     */
    protected $exifReader;

    public function __construct(
        FileArgumentParser $fileArgumentParser,
        ImageProcessor $imageProcessor,
        ExifReader $exifReader
    ) {
        $this->fileArgumentParser = $fileArgumentParser;
        $this->imageProcessor = $imageProcessor;
        $this->exifReader = $exifReader;
    }

    /**
     * Uploads an image and returns the paths of all the variants in an array.
     *
     * @param mixed $file
     * @param string $version
     * @param array $versionConfiguration
     * @param string $prefix
     * @return array
     */
    public function upload($file, $version, array $versionConfiguration, $prefix = '')
    {
        return $this->generateAllImages(
            $this->fileArgumentParser->parse($file),
            $version,
            $versionConfiguration,
            $prefix
        );
    }

    /**
     * Performs all the actions needed to generate the required image variants.
     *
     * Returns an array in which each index is one of the required variants, and
     * the corresponding value is the path that should be added to the database
     * as the image location relative to the document root (though it's actually
     * an absolute URL, suitable for outputting directly as the `src` attribute
     * of an `<img>` element).
     *
     * @param array $file
     * @param string $version
     * @param array $versionConfiguration
     * @param string $prefix
     * @return array
     */
    protected function generateAllImages(array $file, $version, array $versionConfiguration, $prefix)
    {
        try {
            $exif = $this->exifReader->read($file['tmp_name']);
        } catch (Exception $exception) {
            /*
             * We'll just silently ignore all exceptions that might happen when
             * trying to read EXIF data. We don't care if the file is invalid,
             * or there is no file, or the universe has imploded,
             */
            $exif = null;
        }

        /*
         * Process images and get the resulting filenames.
         */
        $filenames = $this->imageProcessor->processImages(
            $this->uploadAndGetImagick($file),
            $version,
            $versionConfiguration,
            $prefix
        );

        /*
         * Generate the paths for the database.
         */
        $return = [];

        foreach ($versionConfiguration as $variant => $parameters) {
            $return[$variant] = $filenames[$variant];
        }

        $return['exif'] = $exif;

        return $return;
    }

    /**
     * Uploads a file and returns an `Imagick` instance of it.
     *
     * @param array $file
     * @return \Imagick
     */
    protected function uploadAndGetImagick(array $file)
    {
        /*
         * We'll immediately load the file from its temporary location into an
         * `Imagick` instance.
         */
        $image = new Imagick($file['tmp_name']);

        /*
         * We don't need the uploaded file anymore, so we're deleting it.
         *
         * There is a slight gotcha with this - if the upload script breaks for
         * some reason during the processing happening later (after this method
         * is finished), the uploaded file will be lost, as after this point, it
         * is only kept in the memory of the current process, within the
         * previously created `Imagick` object. However, in that case, we need
         * to inform the user that an error has occured anyway, so it's not a
         * problem.
         *
         * Additionally, sometimes (when uploading an image from a local path),
         * the file is not uploaded via a web server, it already exists in a
         * local location, so in those cases, we shouldn't delete it. However,
         * this is easy to check with the `is_uploaded_file` function. In fact,
         * this code is maybe a bit out of place, and shouldn't happen in this
         * class at all, but it is what it is for now.
         */
        if (is_uploaded_file($file['tmp_name'])) {
            unlink($file['tmp_name']);
        }

        /*
         * Strip image metadata, because we don't want it, and for security
         * purposes, because we don't want to accidentally leak any information
         * we weren't meant to.
         *
         * This should ideally be performed by a transformer, but we don't
         * currently have a way of applying transformers globally across all
         * images, so to prevent security leaks in case of someone forgetting to
         * apply the transformer, we're hardcoding it here for now.
         */
        $image->stripImage();

        return $image;
    }
}

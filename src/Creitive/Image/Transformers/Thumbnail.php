<?php

namespace Creitive\Image\Transformers;

use Creitive\Image\Transformers\Transformer;
use Exception;
use Imagick;

/**
 * Creates a thumbnail from the original image, with the dimensions configured
 * via the `width` and `height` entries in the `$parameters` array.
 *
 * If `exact` is passed and `true`, the image will be scaled to the exact
 * dimensions; otherwise, the passed width/height will be used as maximum
 * allowed dimensions, and a best fit will be used.
 */
class Thumbnail implements Transformer
{
    /**
     * {@inheritDoc}
     */
    public function transform(Imagick $image, array $parameters)
    {
        $tmpImage = clone $image;

        $width = isset($parameters['width']) ? $parameters['width'] : null;
        $height = isset($parameters['height']) ? $parameters['height'] : null;
        $bestFit = !isset($parameters['exact']) || !$parameters['exact'];

        if ((!$width || !$height) && $bestfit) {
            throw new Exception('Both width and height must be given for a bestfit thumbnail image.');
        }

        $tmpImage->thumbnailImage($width, $height, $bestFit);

        return $tmpImage;
    }
}

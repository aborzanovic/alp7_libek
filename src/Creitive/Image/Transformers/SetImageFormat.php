<?php

namespace Creitive\Image\Transformers;

use Creitive\Image\Transformers\Transformer;
use Imagick;

/**
 * Sets the image format to the one passed in the `format` parameter.
 */
class SetImageFormat implements Transformer
{
    /**
     * {@inheritDoc}
     */
    public function transform(Imagick $image, array $parameters)
    {
        $tmpImage = clone $image;

        $tmpImage->setImageFormat($parameters['format']);

        return $tmpImage;
    }
}

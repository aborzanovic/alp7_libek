<?php

namespace Creitive\Image\Transformers;

use Creitive\Image\Transformers\Transformer;
use Imagick;

/**
 * Creates a cropped thumbnail from the original image, with the exact
 * dimensions configured via the `width` and `height` entries in the
 * `$parameters` array.
 */
class CropThumbnail implements Transformer
{
    /**
     * {@inheritDoc}
     */
    public function transform(Imagick $image, array $parameters)
    {
        $tmpImage = clone $image;

        $tmpImage->cropThumbnailImage($parameters['width'], $parameters['height']);

        return $tmpImage;
    }
}

<?php

namespace Creitive\Image\Transformers;

use Creitive\Image\Transformers\Transformer;
use Imagick;

/**
 * Sets JPEG image compression with the quality in the `quality` parameter
 * (defaults to 80 if not present).
 */
class SetJpegImageCompression implements Transformer
{
    /**
     * {@inheritDoc}
     */
    public function transform(Imagick $image, array $parameters)
    {
        $tmpImage = clone $image;

        if (isset($parameters['quality'])) {
            $quality = $parameters['quality'];
        } else {
            $quality = 80;
        }

        $image->setImageCompression(Imagick::COMPRESSION_JPEG);
        $image->setImageCompressionQuality($quality);

        return $tmpImage;
    }
}

<?php

namespace Creitive\Image\Transformers;

use Creitive\Image\Transformers\Transformer;
use Imagick;

/**
 * Creates a thumbnail of an image, and fills the remaining pixels with a color
 * supplied as a parameter (which defaults to white, if no color is supplied).
 *
 * The `$parameters` array is expected to contain two keys: `width` and
 * `height`, representing the desired thumbnail dimensions in pixels. An
 * optional third key, `color`, is accepted, which should either be a `string`,
 * representing a color accepted by `Imagick::newImage()`, or an `ImagickPixel`
 * with the specified color. See this link for details:
 * http://php.net/imagick.newimage
 *
 * For a transparent background, set this parameter to the string `none`.
 */
class ThumbnailAndFillBackground implements Transformer
{
    /**
     * {@inheritDoc}
     */
    public function transform(Imagick $image, array $parameters)
    {
        $tmpImage = clone $image;

        $tmpImage->thumbnailImage($parameters['width'], $parameters['height'], true);

        $geometry = $tmpImage->getImageGeometry();
        $background = isset($parameters['color']) ? $parameters['color'] : '#fff';

        $x = ($parameters['width'] - $geometry['width']) / 2;
        $y = ($parameters['height'] - $geometry['height']) / 2;

        $canvas = new Imagick;
        $canvas->newImage($parameters['width'], $parameters['height'], $background, $tmpImage->getImageFormat());
        $canvas->compositeImage($tmpImage, Imagick::COMPOSITE_OVER, $x, $y);

        $tmpImage->destroy();

        return $canvas;
    }
}

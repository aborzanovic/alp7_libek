<?php

namespace Creitive\Image\Uploader;

use finfo;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileArgumentParser
{
    /**
     * Parses the passed file argument, and returns an array containing
     * predefined keys.
     *
     * This is used to normalize different ways in which a file can be passed,
     * and it accepts a path to a file (as a string), an instance of
     * `Symfony\Component\HttpFoundation\File\UploadedFile`, or the appropriate
     * files array, in PHP's native format, ie. `$_FILES['file']`.
     *
     * @param mixed $file
     * @return array
     */
    public function parse($file)
    {
        if ($file instanceof UploadedFile) {
            return $this->parseSymfonyUploadedFile($file);
        }

        if (is_readable($file) && is_file($file)) {
            return $this->parsePathToFile($file);
        }

        if ($this->validFilesArrayEntry($file)) {
            return $file;
        }

        throw new InvalidArgumentException('Bad $file argument passed.');
    }

    /**
     * Parses a `Symfony\Component\HttpFoundation\File\UploadedFile` to return a
     * normalized array.
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return array
     */
    protected function parseSymfonyUploadedFile(UploadedFile $file)
    {
        return [
            'name' => $file->getClientOriginalName(),
            'type' => $file->getClientMimeType(),
            'tmp_name' => $file->getPathName(),
            'error' => $file->getError(),
            'size' => $file->getClientSize(),
        ];
    }

    /**
     * Parses a file on the local filesystem and returns an array formatted like
     * a `$_FILES` entry would be.
     *
     * @param string $file
     * @return array
     */
    protected function parsePathToFile($file)
    {
        $finfo = new finfo(FILEINFO_MIME_TYPE);

        return [
            'name' => basename($file),
            'type' => $finfo->file($file),
            'tmp_name' => $file,
            'error' => UPLOAD_ERR_OK,
            'size' => filesize($file),
        ];
    }

    /**
     * Checks whether an array is a valid PHP `$_FILES` entry for a single
     * uploaded file.
     *
     * @param array $file
     * @return boolean
     */
    protected function validFilesArrayEntry(array $file)
    {
        return (
            (count($file) === 5)
            && (isset($file['name']))
            && (isset($file['type']))
            && (isset($file['tmp_name']))
            && (isset($file['error']))
            && (isset($file['size']))
            && (is_string($file['name']))
            && (is_string($file['type']))
            && (is_string($file['tmp_name']))
            && (is_int($file['error']))
            && (is_int($file['size']))
        );
    }
}

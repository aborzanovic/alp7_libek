<?php

namespace Creitive\Image\Uploader;

use Closure;
use Creitive\Image\Transformers\Transformer;
use Creitive\Image\Uploader\FilenameManager;
use Exception;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Filesystem\Filesystem;
use Imagick;

class ImageProcessor
{
    /**
     * An IoC container implementation.
     *
     * We need this in order to be able to instantiate transformers out of the
     * IoC (including automatic dependency resolution).
     *
     * @var \Illuminate\Contracts\Container\Container
     */
    private $container;

    /**
     * The filename manager.
     *
     * @var \Creitive\Image\Uploader\FilenameManager
     */
    protected $filenameManager;

    /**
     * A Filesystem implementation.
     *
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    protected $filesystem;

    public function __construct(Container $container, FilenameManager $filenameManager, Filesystem $filesystem)
    {
        $this->container = $container;
        $this->filenameManager = $filenameManager;
        $this->filesystem = $filesystem;
    }

    /**
     * Processes all of the image versionConfiguration.
     *
     * @param \Imagick $image
     * @param string $version
     * @param array $versionConfiguration
     * @param string $prefix
     * @return void
     */
    public function processImages(Imagick $image, $version, array $versionConfiguration, $prefix)
    {
        $randomFilename = $this->filenameManager->generateRandomFilename();

        $filenames = [];

        foreach ($versionConfiguration as $variant => $transformations) {
            $filenames[$variant] = $this->processImage(
                $image,
                $transformations,
                $version,
                $variant,
                $prefix,
                $randomFilename
            );
        }

        return $filenames;
    }

    /**
     * Processes a single image based on the transformations passed.
     *
     * @param \Imagick $original
     * @param array $transformations
     * @param string $version
     * @param string $variant
     * @param string $prefix
     * @param string $randomFilename
     * @return string
     */
    protected function processImage(Imagick $original, array $transformations, $version, $variant, $prefix, $randomFilename)
    {
        $image = $this->applyImageTransformations($original, $transformations);

        $targetFilename = $this->filenameManager->compileNewFilename(
            $prefix,
            $randomFilename,
            $version,
            $variant,
            strtolower($image->getImageFormat())
        );

        $this->filesystem->put($targetFilename, $image->getImagesBlob());

        $image->destroy();

        return $targetFilename;
    }

    /**
     * Applies all image transformation described in the transformations array.
     *
     * @param \Imagick $original
     * @param array $transformations
     * @return \Imagick
     */
    protected function applyImageTransformations(Imagick $original, array $transformations)
    {
        $image = clone $original;

        foreach ($transformations as $transformation) {
            if (!$this->isValidTransformation($transformation)) {
                throw new Exception('Invalid image transformation provided!');
            }

            if ($transformation instanceof Closure) {
                $image = $transformation($image);
            } else {
                $transformer = $this->container->make($transformation['transformer']);
                $image = $transformer->transform($image, $transformation);
            }
        }

        return $image;
    }

    /**
     * Checks whether a transformation is valid
     *
     * @param \Closure|array|mixed $transformation
     * @return boolean
     */
    protected function isValidTransformation($transformation)
    {
        if ($transformation instanceof Closure) {
            return true;
        }

        if (!is_array($transformation)) {
            return false;
        }

        if (!array_key_exists('transformer', $transformation)) {
            return false;
        }

        return is_a($transformation['transformer'], Transformer::class, true);
    }
}

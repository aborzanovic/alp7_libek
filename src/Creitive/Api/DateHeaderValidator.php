<?php

namespace Creitive\Api;

use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Log;
use Creitive\Api\Exceptions\DateHeaderNotExists;
use Creitive\Api\Exceptions\InvalidDateHeader;

class DateHeaderValidator
{
    /**
     * Max permitted interval (in seconds) for age of request.
     */
    static public $maxDateHeaderAge = 600;

    /**
     * Allowed difference between date on server and header date (in seconds).
     */
    static public $allowedDateHeaderAge = -60;

    /**
     * Local instance of a request
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * First check if Date header is sent, and than checks if it's valid
     *
     * @return void
     */
    public function validate()
    {
        if (!$this->checkDateHeader()) {
            throw new DateHeaderNotExists;
        }

        if (!$this->validateHeaders()) {
            throw new InvalidDateHeader;
        }
    }

    /**
     * Checks if date header is sent.
     *
     * @return boolean
     */
    protected function checkDateHeader()
    {
        return $this->request->headers->has('Date');
    }

    /**
     * Checks if date header is valid or empty
     *
     * @return boolean
     */
    protected function validateHeaders()
    {
        $date = $this->request->header('Date');

        if (empty($date) || !$this->validateDateHeader($date)) {
            Log::error('Date header isn\'t valid');

            return false;
        }

        return true;
    }

    /**
     * Date header validation logic
     *
     * @param string $requestDate
     * @return boolean
     */
    protected function validateDateHeader($requestDate)
    {
        $now = Carbon::now();

        $headerDatetime = Carbon::createFromFormat(
            DateTime::RFC2822,
            $requestDate
        );
        $nowTimestamp = $now->getTimestamp();
        $headerDatetimeTimestamp = $headerDatetime->getTimestamp();

        $timestampDifference = $nowTimestamp - $headerDatetimeTimestamp;

        if ($timestampDifference < static::$allowedDateHeaderAge
            || $timestampDifference - static::$maxDateHeaderAge > 0) {
            Log::debug('Invalid header request. Header time: '. Carbon::createFromTimestamp($headerDatetimeTimestamp). ' Server time: '. Carbon::createFromTimestamp($nowTimestamp). ' Time difference: '. $timestampDifference);

            return false;
        }

        return true;
    }
}

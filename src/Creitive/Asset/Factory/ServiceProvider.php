<?php

namespace Creitive\Asset\Factory;

use Creitive\Asset\Factory;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->app->singleton(Factory::class, function ($app) {
            $assetUrl = $app['config']->get('app.assetUrl', null);

            return new Factory($assetUrl, $app['filesystem']->drive('public'), $app['html']);
        });
    }
}

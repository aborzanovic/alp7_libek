<?php namespace Creitive\Asset;

use Exception;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Html\HtmlBuilder;
use League\Flysystem\FileNotFoundException;

class Container
{
    /**
     * The asset container name.
     *
     * @var string
     */
    protected $name;

    /**
     * The root URL for all of the assets.
     *
     * @var string|null
     */
    protected $assetUrl;

    /**
     * A Filesystem implementation.
     *
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    protected $filesystem;

    /**
     * A HtmlBuilder instance.
     *
     * @var \Illuminate\Html\HtmlBuilder
     */
    protected $htmlBuilder;

    /**
     * All of the registered assets.
     *
     * @var array
     */
    protected $assets = [];

    /**
     * Create a new asset container instance.
     *
     * @param string $name
     * @param string|null $assetUrl
     * @param \Illuminate\Contracts\Filesystem\Filesystem $filesystem
     * @param \Illuminate\Html\HtmlBuilder $htmlBuilder
     * @return void
     */
    public function __construct($name, $assetUrl, Filesystem $filesystem, HtmlBuilder $htmlBuilder)
    {
        $this->name = $name;
        $this->assetUrl = $assetUrl;
        $this->filesystem = $filesystem;
        $this->htmlBuilder = $htmlBuilder;
    }

    /**
     * Add an asset to the container.
     *
     * The extension of the asset source will be used to determine the type of
     * asset being registered (CSS or JavaScript). When using a non-standard
     * extension, the style/script methods may be used to register assets
     * explicitly.
     *
     * <code>
     *     // Add an asset to the container
     *     $container->add('jquery', 'js/jquery.js');
     *
     *     // Add an asset that has dependencies on other assets
     *     $container->add('jquery', 'js/jquery.js', ['jquery-ui']);
     *
     *     // Add an asset that should have attributes applied to its tags
     *     $container->add('jquery', 'js/jquery.js', [], ['defer']);
     * </code>
     *
     * @param string $name
     * @param string $source
     * @param array $dependencies
     * @param array $attributes
     * @return \Creitive\Asset\Container
     */
    public function add($name, $source, array $dependencies = [], array $attributes = [])
    {
        $type = (pathinfo($source, PATHINFO_EXTENSION) == 'css') ? 'style' : 'script';

        return $this->$type($name, $source, $dependencies, $attributes);
    }

    /**
     * Adds a "raw" asset to the container.
     *
     * The raw asset is basically a string which will be outputted as-is, as
     * opposed to styles and scripts, which are embedded within appropriate HTML
     * tags.
     *
     * @param string $name
     * @param string $source
     * @param array $dependencies
     * @param array $attributes
     * @return \Creitive\Asset\Container
     */
    public function addRaw($name, $source, $dependencies = [], $attributes = [])
    {
        $this->register('raw', $name, $source, $dependencies, $attributes);

        return $this;
    }

    /**
     * Remove an asset, optionally filtering by type.
     *
     * If the type is set to `all`, styles, scripts and raw assets with the
     * appropriate name will be removed.
     *
     * Otherwise, this argument may be set to either `raw`, `style`, or
     * `script`, to only remove a specific type of asset.
     *
     * @param string $name
     * @param string $type
     * @return void
     */
    public function remove($name, $type = 'all')
    {
        if ($type !== 'all') {
            $this->removeByType($type, $name);
        } else {
            $this->removeByType('raw', $name);
            $this->removeByType('style', $name);
            $this->removeByType('script', $name);
        }
    }

    /**
     * Removes an asset of a specified type and name.
     *
     * If such an asset or type doesn't exist, no action is taken.
     *
     * @param string $type
     * @param string $name
     * @return void
     */
    protected function removeByType($type, $name)
    {
        if (isset($this->assets[$type], $this->assets[$type][$name])) {
            unset($this->assets[$type][$name]);
        }
    }

    /**
     * Add a CSS file to the registered assets.
     *
     * @param string $name
     * @param string $source
     * @param array $dependencies
     * @param array $attributes
     * @return \Creitive\Asset\Container
     */
    public function style($name, $source, array $dependencies = [], array $attributes = [])
    {
        if (!array_key_exists('media', $attributes)) {
            $attributes['media'] = 'all';
        }

        if (!$this->isFullUrl($source)) {
            $source = $this->bustCache($source, 'css');
        }

        $this->register('style', $name, $source, $dependencies, $attributes);

        return $this;
    }

    /**
     * Add a JavaScript file to the registered assets.
     *
     * @param string $name
     * @param string $source
     * @param array $dependencies
     * @param array $attributes
     * @return \Creitive\Asset\Container
     */
    public function script($name, $source, array $dependencies = [], array $attributes = [])
    {
        if (!$this->isFullUrl($source)) {
            $source = $this->bustCache($source, 'js');
        }

        $this->register('script', $name, $source, $dependencies, $attributes);

        return $this;
    }

    /**
     * Busts the cache for a specified file, by appending the file's last
     * modified timestamp (left-padded to 10 places with `0`s, if necessary)
     * before the extension. For example, `styles.css` might be converted to
     * `styles.1234567890.css`.
     *
     * This assumes that a file "extension" is the segment of the filename after
     * the last occurence of a full stop.
     *
     * It also assumes that the file is located in the project's public path.
     *
     * @param string $source
     * @param string $type
     * @return string
     */
    protected function bustCache($source, $type)
    {
        if (!$this->filesystem->exists($source)) {
            $path = $this->filesystem->getAdapter()->applyPathPrefix($source);
            throw new FileNotFoundException($path);
        }

        $lastModified = $this->filesystem->lastModified($source);
        $timestamp = str_pad($lastModified, 10, '0', STR_PAD_LEFT);
        $pattern = "/\\.{$type}\$/D";
        $replacement = ".{$timestamp}.{$type}";

        return preg_replace($pattern, $replacement, $source);
    }

    /**
     * Returns the full-path for an asset.
     *
     * @param string $source
     * @return string
     */
    public function path($source)
    {
        /*
         * If a base asset URL is defined, use that. This allows the delivery of
         * assets through a different server or third-party content delivery
         * network.
         */
        if (!is_null($this->assetUrl)) {
            return rtrim($this->assetUrl, '/').'/'.ltrim($source, '/');
        }

        return $source;
    }

    /**
     * Add an asset to the array of registered assets.
     *
     * @param string $type
     * @param string $name
     * @param string $source
     * @param array $dependencies
     * @param array $attributes
     * @return void
     */
    protected function register($type, $name, $source, array $dependencies, array $attributes)
    {
        $this->assets[$type][$name] = compact('source', 'dependencies', 'attributes');
    }

    /**
     * Get the links to all of the registered CSS assets.
     *
     * @return string
     */
    public function styles()
    {
        return $this->group('style');
    }

    /**
     * Get the links to all of the registered JavaScript assets.
     *
     * @return string
     */
    public function scripts()
    {
        return $this->group('script');
    }

    /**
     * Returns all the raw assets in this container.
     *
     * @return string
     */
    public function raw()
    {
        return $this->group('raw');
    }

    /**
     * Get all of the registered assets for a given type/group.
     *
     * @param string $group
     * @return string
     */
    protected function group($group)
    {
        if (!isset($this->assets[$group]) || count($this->assets[$group]) == 0) {
            return '';
        }

        $assets = '';

        foreach ($this->arrange($this->assets[$group]) as $name => $data) {
            $assets .= $this->asset($group, $name);
        }

        return $assets;
    }

    /**
     * Get the HTML link to a registered asset.
     *
     * @param string $group
     * @param string $name
     * @return string
     */
    protected function asset($group, $name)
    {
        if (!isset($this->assets[$group][$name])) {
            return '';
        }

        if ($group === 'raw') {
            return $this->assets[$group][$name]['source'];
        }

        $asset = $this->assets[$group][$name];

        /*
         * If the bundle source is not a complete URL, we will go ahead and
         * prepend the bundle's asset path to the source provided with the
         * asset. This will ensure that we attach the correct path to the asset.
         */
        if (!$this->isFullUrl($asset['source'])) {
            $asset['source'] = $this->path($asset['source']);
        }

        return $this->htmlBuilder->$group($asset['source'], $asset['attributes']);
    }

    /**
     * Checks whether a source URL is a full URL (meaning it starts with a
     * protocol or it is a protocol-relative URL).
     *
     * @param string $source
     * @return boolean
     */
    protected function isFullUrl($source)
    {
        return preg_match('/^(\w+:)?\/\//i', $source);
    }

    /**
     * Sort and retrieve assets based on their dependencies.
     *
     * @param array $assets
     * @return array
     */
    protected function arrange(array $assets)
    {
        list($original, $sorted) = [$assets, []];

        while (count($assets) > 0) {
            foreach ($assets as $asset => $value) {
                $this->evaluateAsset($asset, $value, $original, $sorted, $assets);
            }
        }

        return $sorted;
    }

    /**
     * Evaluate an asset and its dependencies.
     *
     * @param string $asset
     * @param string $value
     * @param array $original
     * @param array $sorted
     * @param array $assets
     * @return void
     */
    protected function evaluateAsset($asset, $value, array $original, array &$sorted, array &$assets)
    {
        /*
         * If the asset has no more dependencies, we can add it to the sorted
         * list and remove it from the array of assets. Otherwise, we will
         * verify the asset's dependencies and determine if they've been sorted.
         */
        if (count($assets[$asset]['dependencies']) == 0) {
            $sorted[$asset] = $value;

            unset($assets[$asset]);
        } else {
            foreach ($assets[$asset]['dependencies'] as $key => $dependency) {
                if (!$this->dependencyIsValid($asset, $dependency, $original, $assets)) {
                    unset($assets[$asset]['dependencies'][$key]);

                    continue;
                }

                /*
                 * If the dependency has not yet been added to the sorted list,
                 * we can't remove it from this asset's array of dependencies.
                 * We'll try again on the next trip through the loop.
                 */
                if (!isset($sorted[$dependency])) {
                    continue;
                }

                unset($assets[$asset]['dependencies'][$key]);
            }
        }
    }

    /**
     * Verify that an asset's dependency is valid.
     *
     * A dependency is considered valid if it exists, is not a circular
     * reference, and is not a reference to the owning asset itself. If the
     * dependency doesn't exist, no error or warning will be given. For the
     * other cases, an exception is thrown.
     *
     * @param string $asset
     * @param string $dependency
     * @param array $original
     * @param array $assets
     * @return boolean
     * @throws \Exception
     */
    protected function dependencyIsValid($asset, $dependency, array $original, array $assets)
    {
        if (!isset($original[$dependency])) {
            return false;
        } elseif ($dependency === $asset) {
            throw new Exception("Asset [{$asset}] is dependent on itself.");
        } elseif (isset($assets[$dependency]) && in_array($asset, $assets[$dependency]['dependencies'])) {
            throw new Exception("Assets [{$asset}] and [{$dependency}] have a circular dependency.");
        }

        return true;
    }

    /**
     * Returns all the assets concatenated, in the following order:
     *
     * 1. Stylesheets
     * 2. Externally loaded scripts
     * 3. Raw assets
     *
     * @return string
     */
    public function render()
    {
        return $this->styles().$this->scripts().$this->raw();
    }

    /**
     * Magic method.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
}

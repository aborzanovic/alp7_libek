<?php

namespace Creitive\Database\Eloquent;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Model as EloquentModel;

class Collection extends EloquentCollection
{
    /**
     * Finds the first item that matches the given properties.
     *
     * @param array $properties
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function findByProperties(array $properties = [])
    {
        return $this->first(function ($key, EloquentModel $item) use ($properties) {
            foreach ($properties as $property => $value) {
                if ($item->$property !== $value) {
                    return false;
                }
            }

            return true;
        });
    }
}

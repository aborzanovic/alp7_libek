<?php

namespace Creitive\Database\Query\Grammars\MySqlGrammar;

use Creitive\Database\Query\Grammars\MySqlGrammar;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        /*
         * Use our own `MySqlGrammar` class, so as to be able to use the
         * `SQL_CALC_FOUND_ROWS` to count results.
         *
         * This code assumes the `mysql` connection, as it's what we use.
         *
         * However, if there is a need for customization, we'll update this
         * class with some way to configure the connection to use.
         */

        $defaultConnection = $this->app['config']->get('database.default');

        if ($defaultConnection !== 'mysql') {
            return;
        }

        $connection = $this->app['db']->connection('mysql');
        $grammar = new MySqlGrammar;
        $connection->setQueryGrammar($grammar);
    }
}

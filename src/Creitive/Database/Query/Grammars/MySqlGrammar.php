<?php

namespace Creitive\Database\Query\Grammars;

use Creitive\Database\Query\Builder as CreitiveBuilder;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Query\Grammars\MySqlGrammar as BaseMySqlGrammar;

class MySqlGrammar extends BaseMySqlGrammar
{
    /**
     * {@inheritdoc}
     */
    protected function compileColumns(Builder $query, $columns)
    {
        /*
         * If the query is actually performing an aggregating select, we will
         * let that compiler handle the building of the select clauses, as it
         * will need some more syntax that is best handled by that function to
         * keep things neat.
         */
        if (!is_null($query->aggregate)) {
            return;
        }

        $select = 'select ';

        if ($query instanceof CreitiveBuilder && $query->sqlCalcFoundRows) {
            $select .= 'SQL_CALC_FOUND_ROWS ';
        }

        if ($query->distinct) {
            $select .= 'distinct ';
        }

        return $select.$this->columnize($columns);
    }
}

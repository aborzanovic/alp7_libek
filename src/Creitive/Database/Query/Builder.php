<?php

namespace Creitive\Database\Query;

use Illuminate\Database\Query\Builder as BaseBuilder;

class Builder extends BaseBuilder
{
    /**
     * Whether to use `SQL_CALC_FOUND_ROWS` for the generated query.
     *
     * @var boolean
     */
    public $sqlCalcFoundRows = false;

    /**
     * Turns on the `SQL_CALC_FOUND_ROWS` for this query.
     *
     * Can be chained as all the other Builder methods.
     *
     * If `false` is passed as an argument, it will turn this *off* within a
     * query (in case it was turned on previously).
     *
     * @param boolean $on
     * @return \Creitive\Database\Query\Builder
     */
    public function sqlCalcFoundRows($on = true)
    {
        $this->sqlCalcFoundRows = $on;

        return $this;
    }
}

<?php

namespace Creitive\Mailchimp;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Mailchimp;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->app->singleton(Mailchimp::class, function ($app) {
            $config = $app['config']->get('mailchimp');

            return new Mailchimp(
                $config['apiKey'],
                [
                    'CURLOPT_FOLLOWLOCATION' => true,
                ]
            );
        });
    }
}

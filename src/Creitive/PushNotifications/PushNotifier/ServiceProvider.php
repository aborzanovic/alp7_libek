<?php

namespace Creitive\PushNotifications\PushNotifier;

use Creitive\PushNotifications\AndroidNotifier;
use Creitive\PushNotifications\IosNotifier;
use Creitive\PushNotifications\PushNotifier;
use App\Auth\User\Repository as UserRepository;
use App\Devices\Device\Repository as DeviceRepository;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->registerIosNotifier();
        $this->registerAndroidNotifier();
    }

    /**
     * Registers an IosNotifier.
     *
     * @return void
     */
    protected function registerIosNotifier()
    {
        $this->app->singleton(IosNotifier::class, function ($app) {
            return new IosNotifier(
                $app['config']->get('apns.certificate'),
                $app['config']->get('apns.passphrase')
            );
        });
    }

    /**
     * Registers an AndroidNotifier.
     *
     * @return void
     */
    protected function registerAndroidNotifier()
    {
        $this->app->singleton(AndroidNotifier::class, function ($app) {
            return new AndroidNotifier($app['config']->get('gcm.apiKey'));
        });
    }
}

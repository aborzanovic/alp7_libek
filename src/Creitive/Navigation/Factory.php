<?php

namespace Creitive\Navigation;

use Creitive\Navigation\Navigation;

class Factory
{
    /**
     * An array of individual navigation elements.
     *
     * @var array
     */
    protected $navigations = [];

    /**
     * Gets a named navigation, or creates one, if it doesn't exist yet.
     *
     * @param string $name
     * @return \Creitive\Navigation\Navigation
     */
    public function get($name)
    {
        if (isset($this->navigations[$name])) {
            return $this->navigations[$name];
        }

        return $this->navigations[$name] = $this->make();
    }

    /**
     * Creates a new Navigation instance.
     *
     * @return \Creitive\Navigation\Navigation
     */
    public function make()
    {
        return new Navigation;
    }

    /**
     * A helper method for easier fetching of named navigations.
     *
     * @param string $name
     * @return \Creitive\Navigation\Navigation
     */
    public function __invoke($name)
    {
        return $this->get($name);
    }
}

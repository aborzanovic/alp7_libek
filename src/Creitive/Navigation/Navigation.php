<?php

namespace Creitive\Navigation;

use Illuminate\Support\Collection;

class Navigation
{
    /**
     * Navigation items.
     *
     * @var \Illuminate\Support\Collection
     */
    protected $items;

    /**
     * The key of the active item.
     *
     * @var string|null
     */
    protected $active = null;

    public function __construct()
    {
        $this->items = new Collection;
    }

    /**
     * Returns the currently active item key.
     *
     * @return string|null
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Sets the currently active item key.
     *
     * @param string|null $name
     */
    public function setActive($name)
    {
        $this->active = $name;

        return $this;
    }

    /**
     * Returns the internal collection.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Appends an item onto the beginning, preserving keys.
     *
     * @param string $key
     * @param mixed $value
     * @return \Creitive\Navigation\Navigation
     */
    public function addFirst($key, $value)
    {
        $items = $this->items->all();

        array_reverse($items, true);
        $items[$key] = $value;
        $items = array_reverse($items, true);

        $this->items = new Collection($items);

        return $this;
    }

    /**
     * Appends an item onto the end, preserving keys.
     *
     * @param string $key
     * @param mixed $value
     * @return \Creitive\Navigation\Navigation
     */
    public function addLast($key, $value)
    {
        $this->items->put($key, $value);

        return $this;
    }

    /**
     * Adds a a new item before a specific key.
     *
     * @param string $beforeKey
     * @param string $newKey
     * @param mixed $value
     * @return \Creitive\Navigation\Navigation
     */
    public function addBefore($beforeKey, $newKey, $value)
    {
        $items = [];

        $added = false;

        foreach ($this->items as $key => $item) {
            if ($key === $beforeKey) {
                $items[$newKey] = $value;

                $added = true;
            }

            $items[$key] = $item;
        }

        if (!$added) {
            $items[$newKey] = $value;
        }

        $this->items = new Collection($items);

        return $this;
    }

    /**
     * Adds a new item after a specific key.
     *
     * @param string $afterKey
     * @param string $newKey
     * @param mixed $value
     * @return \Creitive\Navigation\Navigation
     */
    public function addAfter($afterKey, $newKey, $value)
    {
        $items = [];

        $added = false;

        foreach ($this->items as $key => $item) {
            $items[$key] = $item;

            if ($key === $afterKey) {
                $items[$newKey] = $value;

                $added = true;
            }
        }

        if (!$added) {
            $items[$newKey] = $value;
        }

        $this->items = new Collection($items);

        return $this;
    }
}

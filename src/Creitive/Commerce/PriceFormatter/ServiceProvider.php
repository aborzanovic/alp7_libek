<?php

namespace Creitive\Commerce\PriceFormatter;

use Creitive\Commerce\PriceFormatter;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->app->singleton(PriceFormatter::class, function ($app) {
            return new PriceFormatter(
                $app['config']->get('commerce.storedDecimals'),
                $app['translator']->get('common.decimalSeparator'),
                $app['translator']->get('common.thousandsSeparator')
            );
        });
    }
}

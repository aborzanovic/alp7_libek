<?php

namespace Creitive\Models\Traits;

use DB;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Log;

trait BaseSortableTrait
{
    /**
     * Boots the sortable trait.
     *
     * Hooks into the model's "creating" event, to automatically set the sort
     * value when needed.
     *
     * The actual trait that uses this trait must supply its own method that
     * calls this, due to the way how Laravel automatically boots traits based
     * on their class name.
     *
     * @return void
     */
    public static function bootBaseSortableTrait()
    {
        static::creating(function ($model) {
            if ($model->autoSetSortValue()) {
                return true;
            }

            return false;
        });
    }

    /**
     * Sorts the items by their sort order.
     *
     * The direction should be either 'asc' or 'desc'.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $direction
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSorted(Builder $query, $direction = 'asc')
    {
        return $query->orderBy('sort', $direction);
    }

    /**
     * Automatically sets the next available sort value to the current object's
     * `sort` attribute, if the object isn't saved in the database yet.
     *
     * In order for sorting to work consistently, this method must *always* be
     * called before saving a new model to the database. If using with Laravel,
     * it's recommended to hook it into the model's "saving" event, in the base
     * model's `boot()` method, instead of calling it manually.
     *
     * Returns `true` on success, or `false` on failure.
     *
     * @return bool
     */
    public function autoSetSortValue()
    {
        /*
         * If the model is saved already, or already has the `sort` attribute
         * set, just don't do anything and return `true` immediately.
         */
        if ($this->exists || $this->sort) {
            return true;
        }

        $sort = $this->getNextSortValue();

        if (is_null($sort)) {
            return false;
        }

        $this->sort = $sort;

        return true;
    }

    /**
     * Gets the next available sort value.
     *
     * Returns `null` if an error has occured during the query.
     *
     * IMPORTANT: This method currently only works on MySQL databases, and it
     * will likely fail if used anywhere else. At the moment, to use this model
     * with a different database, this method must be overriden and an
     * appropriate query should be written instead.
     *
     * @return null|integer
     */
    public function getNextSortValue()
    {
        $table = $this->getTable();

        try {
            $results = DB::select(
                "SELECT COALESCE(MAX(`sort`), 0) AS `highest_sort`
                FROM `{$table}`;"
            );

            if (count($results) !== 1) {
                throw new Exception('getNextSortValue() got a weird result from the database. Please check the logs for more info.');
            }

            $result = reset($results);

            return ((int) $result->highest_sort) + 1;
        } catch (Exception $e) {
            /*
             * Laravel's connections throw a basic PHP `Exception` if they
             * encounter an error, so that's what we need to catch here.
             */

            Log::error($e);

            return null;
        }
    }

    /**
     * Updates the sort order.
     *
     * Accepts an ordered array of IDs, which represents the new order.
     *
     * @param array $newSortOrder
     * @return boolean
     */
    abstract public function updateSortOrder(array $newSortOrder = []);
}

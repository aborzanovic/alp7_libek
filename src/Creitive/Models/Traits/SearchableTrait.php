<?php

namespace Creitive\Models\Traits;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use LogicException;

trait SearchableTrait
{
    /**
     * Gets the columns that are searchable.
     *
     * This method *must* be overriden on the model where the trait is used, to
     * return an array containg the names of columns that should be searched.
     *
     * @return array
     */
    abstract public function getSearchableColumns();

    /**
     * Limits the results to those that match the given search string.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $string
     * @param array|null $columns
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws \Exception
     */
    public function scopeSearch(Builder $query, $string, array $columns = null)
    {
        $columns = $columns ?: $this->getSearchableColumns();

        if (empty($columns)) {
            throw new LogicException('No searchable columns have been defined on this model!');
        }

        if (!$this->validSearchableString($string)) {
            throw new Exception('Invalid searchable string: '.$string);
        }

        $string = $this->escapeSearchableString($string);

        return $query->where(function (Builder $query) use ($string, $columns) {
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', "%{$string}%");
            }

            return $query;
        });
    }

    /**
     * Escapes the searchable string for use in `LIKE` conditions.
     *
     * @param string $string
     * @return string
     */
    protected function escapeSearchableString($string)
    {
        return str_replace('%', '\\%', $string);
    }

    /**
     * Checks whether the passed searchable string is valid.
     *
     * This method may be overriden on the model to provide custom string
     * validation (for example, if there is a minimum allowed length of the
     * search string).
     *
     * @param string $string
     * @return boolean
     */
    protected function validSearchableString($string)
    {
        return mb_strlen($string) > 0;
    }
}

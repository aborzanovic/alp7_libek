<?php

namespace Creitive\Models;

interface ChildImageableInterface
{
    /**
     * Gets the name of the attribute that returns the child images relation.
     *
     * This is often just `images`, and such an implementation is provided as a
     * default trait for this interface.
     *
     * @return string
     */
    public function getChildImagesAttributeName();
}

<?php

namespace Creitive\Models;

interface NestableInterface
{
    /**
     * Sets this item's parent.
     *
     * @param \Creitive\Models\NestableInterface|null $item
     * @return void
     */
    public function setParent(NestableInterface $item = null);

    /**
     * Adds a child of this item.
     *
     * @param \Creitive\Models\NestableInterface $item
     * @return void
     */
    public function addChild(NestableInterface $item);

    /**
     * Checks whether this item has a parent.
     *
     * @return boolean
     */
    public function hasParent();

    /**
     * Checks whether this item has children.
     *
     * @return boolean
     */
    public function hasChildren();

    /**
     * Eloquent relationship: an item may belong to a parent item.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent();

    /**
     * Eloquent relationship: an item may have many children.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children();

    /**
     * Returns the root parent item, starting from the current item.
     *
     * @return \Creitive\Models\NestableInterface
     */
    public function root();
}

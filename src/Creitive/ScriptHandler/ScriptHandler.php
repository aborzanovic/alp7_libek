<?php

namespace Creitive\ScriptHandler;

use App;
use Creitive\Asset\Factory as AssetFactory;
use Illuminate\Config\Repository as Config;
use Illuminate\View\Factory as ViewFactory;
use stdClass;

class ScriptHandler
{
    /**
     * The current application environment.
     *
     * @var string
     */
    protected $environment;

    /**
     * An Asset factory instance.
     *
     * @var \Creitive\Asset\Factory
     */
    protected $asset;

    /**
     * A View factory instance.
     *
     * @var \Illuminate\View\Factory
     */
    protected $view;

    /**
     * A Config repository instance.
     *
     * @var \Illuminate\Config\Repository
     */
    protected $config;

    public function __construct($environment, AssetFactory $asset, ViewFactory $view, Config $config)
    {
        $this->environment = $environment;
        $this->asset = $asset;
        $this->view = $view;
        $this->config = $config;
    }

    /**
     * Loads the jQuery assets used by the whole project.
     *
     * @return void
     */
    public function loadJQuery()
    {
        $this->asset->container('bodyEnd')->add(
            'jquery',
            'https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'
        );
    }

    /**
     * Loads the jQueryUI assets used by the whole project.
     *
     * @return void
     */
    public function loadJQueryUi()
    {
        /*
         * The CSS, using the `smoothnes` theme by default. Use Google for
         * finding other themes available via the CDN.
         */
        $this->asset->container('head')->add(
            'jquery-ui',
            'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.min.css',
            [
                'polyfill',
            ]
        );

        /*
         * The JS library.
         */
        $this->asset->container('bodyEnd')->add(
            'jquery-ui',
            'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js',
            [
                'jquery',
            ]
        );
    }

    /**
     * Loads polyfill.io browser polyfills.
     *
     * @return void
     */
    public function loadPolyfillIo()
    {
        $this->asset->container('head')->add(
            'polyfill',
            'https://cdn.polyfill.io/v1/polyfill.min.js'
        );
    }

    /**
     * Loads Google Analytics tracking code.
     *
     * Only works in the `production` environment, and when the appropriate
     * configuration parameters have been set.
     *
     * @return void
     */
    public function loadGoogleAnalytics()
    {
        if ($this->environment !== 'production' || ! $this->config->get('googleAnalytics.trackingId')) {
            return;
        }

        $this->asset->container('head')->addRaw(
            'google-analytics',
            $this->view->make('creitive/scripts::google-analytics', $this->config->get('googleAnalytics'))
        );
    }

    /**
     * Loads Google Maps scripts.
     *
     * Recommended to set the `googleMaps.key` configuration entry, for better
     * support, such as less strict rate limiting.
     *
     * @return void
     */
    public function loadGoogleMaps()
    {
        $key = $this->config->get('google.maps.key');

        if (!is_null($key)) {
            $url = "https://maps.googleapis.com/maps/api/js?key={$key}";
        } else {
            $url = "https://maps.googleapis.com/maps/api/js";
        }

        $this->asset->container('bodyEnd')->add(
            'google-maps',
            $url
        );
    }

    /**
     * Loads scripts for the Google+ "+1" social plugin.
     *
     * @return void
     */
    public function loadGooglePlusOneScripts()
    {
        $this->asset->container('bodyStart')->addRaw(
            'google-plus-one',
            $this->view->make('creitive/scripts::social-plugins.google-plus-one')
        );
    }

    /**
     * Loads scripts for the Pinterest "PinIt" button.
     *
     * @return void
     */
    public function loadPinterestScripts()
    {
        $this->asset->container('bodyStart')->addRaw(
            'pinterest',
            $this->view->make('creitive/scripts::social-plugins.pinterest')
        );
    }

    /**
     * Loads Twitter social plugin scripts.
     *
     * @return void
     */
    public function loadTwitterScripts()
    {
        $this->asset->container('bodyStart')->addRaw(
            'twitter',
            $this->view->make('creitive/scripts::social-plugins.twitter')
        );
    }

    /**
     * Loads LinkedIn social plugin scripts.
     *
     * @return void
     */
    public function loadLinkedInScripts()
    {
        $this->asset->container('bodyEnd')->add(
            'linked-in',
            '//platform.linkedin.com/in.js'
        );
    }

    /**
     * Loads Facebook's JavaScript SDK.
     *
     * Required for Facebook's social plugins.
     *
     * @return void
     */
    public function loadFacebookScripts()
    {
        $this->asset->container('bodyStart')->addRaw(
            'facebook',
            $this->getFacebookJavascriptInitializationCode()
        );
    }

    /**
     * Generates the initialization code needed for Facebook's JS SDK.
     *
     * @return \Illuminate\View\View
     */
    public function getFacebookJavascriptInitializationCode()
    {
        $initObject = $this->getFacebookJavascriptInitializationObject($this->config->get('facebook', []));

        return $this->view->make(
            'creitive/scripts::social-plugins.facebook',
            [
                'initObject' => $initObject,
            ]
        );
    }

    /**
     * Creates the initialization object for the Facebook JS API, which is later
     * JSON-encoded and passed to the Facebook's JS initialization function.
     *
     * @link https://developers.facebook.com/docs/javascript/reference/FB.init/v2.1
     * @param array $config
     * @return \stdClass
     */
    public function getFacebookJavascriptInitializationObject(array $config = [])
    {
        $facebookJavascriptInitializationObject = new stdClass;

        if ($config['appId']) {
            $facebookJavascriptInitializationObject->appId = $config['appId'];
        }

        $jsOptions = $config['jsOptions'];

        $facebookJavascriptInitializationObject->version = $jsOptions['version'];
        $facebookJavascriptInitializationObject->cookie = $jsOptions['cookie'];
        $facebookJavascriptInitializationObject->status = $jsOptions['status'];
        $facebookJavascriptInitializationObject->xfbml = $jsOptions['xfbml'];
        $facebookJavascriptInitializationObject->frictionlessRequests = $jsOptions['frictionlessRequests'];

        return $facebookJavascriptInitializationObject;
    }
}

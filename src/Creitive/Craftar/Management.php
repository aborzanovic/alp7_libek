<?php
// (C) Catchoom Technologies S.L.
// Licensed under the MIT license.
// https://github.com/Catchoom/craftar-php/blob/master/LICENSE
// All warranties and liabilities are disclaimed.

namespace Creitive\Craftar;

use Creitive\Craftar\ResponseTransformingTrait;
use GuzzleHttp\ClientInterface;

class Management
{
    use ResponseTransformingTrait;

    /**
     * An ClientInterface implementation.
     *
     * @var \GuzzleHttp\ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Gets the list of available collections.
     *
     * @param integer|null $limit
     * @param integer|null $offset
     * @return \Creitive\Craftar\Response
     */
    public function getCollectionList($limit = null, $offset = null)
    {
        return $this->getObjectList('collection', null, $limit, $offset);
    }

    /**
     * Gets a specific collection by its UUID.
     *
     * @param string $uuid
     * @return \Creitive\Craftar\Response
     */
    public function getCollection($uuid)
    {
        return $this->getObject('collection', $uuid);
    }

    /**
     * Creates a new collection with the passed name.
     *
     * @param string $name
     * @return \Creitive\Craftar\Response
     */
    public function createCollection($name)
    {
        return $this->createObject('collection', ['name' => $name]);
    }

    /**
     * Deletes a collection by its UUID.
     *
     * @param string $uuid
     * @return \Creitive\Craftar\Response
     */
    public function deleteCollection($uuid)
    {
        return $this->deleteObject('collection', $uuid);
    }

    /**
     * Updates the name of a collection.
     *
     * @param string $uuid
     * @param string $name
     * @return \Creitive\Craftar\Response
     */
    public function updateCollection($uuid, $name)
    {
        return $this->updateObject('collection', $uuid, ['name' => $name]);
    }

    /**
     * Gets the list of all available items.
     *
     * @param integer $limit
     * @param integer $offset
     * @return \Creitive\Craftar\Response
     */
    public function getItemList($limit = null, $offset = null)
    {
        return $this->getObjectList('item', null, $limit, $offset);
    }

    /**
     * Gets the list of all available items within a collection.
     *
     * @param string $collectionUuid
     * @param integer $limit
     * @param integer $offset
     * @return \Creitive\Craftar\Response
     */
    public function getItemListByCollection($collectionUuid, $limit = null, $offset = null)
    {
        return $this->getObjectList('item', $collectionUuid, $limit, $offset);
    }

    /**
     * Gets a specific item by its UUID.
     *
     * @param string $uuid
     * @return \Creitive\Craftar\Response
     */
    public function getItem($uuid)
    {
        return $this->getObject('item', $uuid);
    }

    /**
     * Creates a new item within a specific collection.
     *
     * @param string $collectionUuid
     * @param string $name
     * @param array $optionalData
     * @return \Creitive\Craftar\Response
     */
    public function createItem($collectionUuid, $name, array $optionalData)
    {
        $data = [
            'collection' => $this->buildResourceUri('collection', $collectionUuid),
            'name' => $name,
        ];

        return $this->createObject('item', array_merge($data, $optionalData));
    }

    /**
     * Updates a specific item.
     *
     * @param string $itemUuid
     * @param string $name
     * @param array $optionalData
     * @return \Creitive\Craftar\Response
     */
    public function updateItem($itemUuid, $name, array $optionalData = [])
    {
        $data = [
            'name' => $name,
        ];

        return $this->updateObject('item', $itemUuid, array_merge($data, $optionalData));
    }

    /**
     * Deletes an item by its UUID.
     *
     * @param string  $uuid
     * @return \Creitive\Craftar\Response
     */
    public function deleteItem($uuid)
    {
        return $this->deleteObject('item', $uuid);
    }

    /**
     * Gets the list of all reference images.
     *
     * @param integer $limit
     * @param integer $offset
     * @return \Creitive\Craftar\Response
     */
    public function getImageList($limit = null, $offset = null)
    {
        return $this->getObjectList('image', null, $limit, $offset);
    }

    /**
     * Gets the list of all reference images for an item.
     *
     * @param string $itemUuid
     * @param integer $limit
     * @param integer $offset
     * @return \Creitive\Craftar\Response
     */
    public function getImageListByItem($itemUuid, $limit = null, $offset = null)
    {
        return $this->getObjectList('image', $itemUuid, $limit, $offset);
    }

    /**
     * Gets a reference image by its UUID.
     *
     * @param string $uuid
     * @return \Creitive\Craftar\Response
     */
    public function getImage($uuid)
    {
        return $this->getObject('image', $uuid);
    }

    /**
     * Creates a new reference image for an item.
     *
     * The passed image should be the image blob.
     *
     * @param string $itemUuid
     * @param string $imageBlob
     * @return \Creitive\Craftar\Response
     */
    public function createImage($itemUuid, $imageBlob)
    {
        $options = [
            'multipart' => [
                [
                    'name' => 'item',
                    'contents' => $this->buildResourceUri('item', $itemUuid),
                ],
                [
                    'name' => 'file',
                    'contents' => $imageBlob,
                    /*
                     * It doesn't matter what the filename is, as long as there
                     * **is** a filename - otherwise, we get errors from the
                     * CraftAR API.
                     */
                    'filename' => 'image.jpg',
                ],
            ],
        ];

        return $this->transformResponse(
            $this->client->post('image/', $options)
        );
    }

    /**
     * Deletes a reference image by its UUID.
     *
     * @param string $uuid
     * @return \Creitive\Craftar\Response
     */
    public function deleteImage($uuid)
    {
        return $this->deleteObject('image', $uuid);
    }

    /**
     * Gets the list of all tokens.
     *
     * @param integer $limit
     * @param integer $offset
     * @return \Creitive\Craftar\Response
     */
    public function getTokenList($limit = null, $offset = null)
    {
        return $this->getObjectList('token', null, $limit, $offset);
    }

    /**
     * Gets the list of all tokens for a collection.
     *
     * @param string $collectionUuid
     * @param integer $limit
     * @param integer $offset
     * @return \Creitive\Craftar\Response
     */
    public function getTokenListByCollection($collectionUuid, $limit = null, $offset = null)
    {
        return $this->getObjectList('token', $collectionUuid, $limit, $offset);
    }

    /**
     * Gets a token by its UUID.
     *
     * @param string $uuid
     * @return \Creitive\Craftar\Response
     */
    public function getToken($uuid)
    {
        return $this->getObject('token', $uuid);
    }

    /**
     * Creates a new token for a collection.
     *
     * @param string $collectionUuid
     * @return \Creitive\Craftar\Response
     */
    public function createToken($collectionUuid)
    {
        return $this->createObject(
            'token',
            [
                'collection' => $this->buildResourceUri('collection', $collectionUuid),
            ]
        );
    }

    /**
     * Deletes a token by its UUID.
     *
     * @param string $uuid
     * @return \Creitive\Craftar\Response
     */
    public function deleteToken($uuid)
    {
        return $this->deleteObject('token', $uuid);
    }

    /**
     * Builds a URI for a specified resource.
     *
     * @param string $objectType
     * @param string|null $uuid
     * @return string
     */
    private function buildResourceUri($objectType, $uuid = null)
    {
        $url = "/api/v0/{$objectType}/";

        if ($uuid != null) {
            $url .= "{$uuid}/";
        }

        return $url;
    }

    /**
     * Gets a list of objects of a specified type, with optional pagination.
     *
     * @param string $objectType
     * @param string $filter
     * @param integer $limit
     * @param integer $offset
     * @return \Creitive\Craftar\Response
     */
    private function getObjectList($objectType, $filter, $limit = null, $offset = null)
    {
        $query = [];

        if ($limit !== null) {
            $query['limit'] = $limit;
        }

        if ($offset !== null) {
            $query['offset'] = $offset;
        }

        if ($filter !== null) {
            if ($objectType === 'item' || $objectType === 'token') {
                $query['collection__uuid'] = $filter;
            } elseif ($objectType === 'image') {
                $query['item__uuid'] = $filter;
            }
        }

        return $this->transformResponse(
            $this->client->get($objectType, ['query' => $query])
        );
    }

    /**
     * Gets a specific object by its type and UUID.
     *
     * @param string $objectType
     * @param string $uuid
     * @return \Creitive\Craftar\Response
     */
    private function getObject($objectType, $uuid)
    {
        return $this->transformResponse(
            $this->client->get("{$objectType}/{$uuid}")
        );
    }

    /**
     * Deletes a specific object by its type and UUID.
     *
     * @param string $objectType
     * @param string $uuid
     * @return \Creitive\Craftar\Response
     */
    private function deleteObject($objectType, $uuid)
    {
        return $this->transformResponse(
            $this->client->delete("{$objectType}/{$uuid}/")
        );
    }

    /**
     * Creates a new object of the specified type.
     *
     * @param string $objectType
     * @param array $data
     * @return \Creitive\Craftar\Response
     */
    private function createObject($objectType, array $data)
    {
        $options = [
            'json' => $data,
        ];

        return $this->transformResponse(
            $this->client->post("{$objectType}/", $options)
        );
    }

    /**
     * Updates a specific object.
     *
     * @param string $objectType
     * @param string $uuid
     * @param array $data
     * @return \Creitive\Craftar\Response
     */
    private function updateObject($objectType, $uuid, array $data)
    {
        $options = [
            'json' => $data,
        ];

        return $this->transformResponse(
            $this->client->put("{$objectType}/{$uuid}/", $options)
        );
    }
}

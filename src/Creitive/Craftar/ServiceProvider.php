<?php

namespace Creitive\Craftar;

use Creitive\Craftar\Management;
use Creitive\Craftar\Recognition;
use Creitive\Guzzle\OptionMergingClient;
use Creitive\Guzzle\RequestExceptionHandlingClient;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $userAgent = 'CreITive CraftAR client 1.0';

        /*
         * The Management API has a single API key used for access, so we'll
         * pass it a client that automatically uses the correct configuration.
         */

        $this->app->singleton(Management::class, function ($app) use ($userAgent) {
            $host = 'https://my.craftar.net';
            $apiVersion = 'v0';

            $baseUri = "{$host}/api/{$apiVersion}/";

            $guzzleClient = new GuzzleClient([
                'base_uri' => $baseUri,
                'headers' => [
                    'User-Agent' => $userAgent,
                ],
                'query' => [
                    'api_key' => $app['config']->get('creitive.craftar.managementApiKey'),
                ],
            ]);

            $optionMergingClient = new OptionMergingClient($guzzleClient);
            $requestExceptionHandlingClient = new RequestExceptionHandlingClient($optionMergingClient);

            return new Management($requestExceptionHandlingClient);
        });

        /*
         * The Image Recognition API, on the other hand, uses a per-collection
         * token with requests, so there's less sense, generally speaking, in
         * having this hard-coded this way. However, we will always query a
         * single collection, at least as far as the project is concerned at
         * this point, so this is okay. We just need to add that collection's
         * token to the appropriate environment variable, and we'll pass the
         * pre-configured client to our Recognition class.
         */

        $this->app->singleton(Recognition::class, function ($app) use ($userAgent) {
            $host = 'https://search.craftar.net';
            $apiVersion = 'v1';

            $baseUri = "{$host}/{$apiVersion}/";

            $guzzleClient = new GuzzleClient([
                'base_uri' => $baseUri,
                'headers' => [
                    'User-Agent' => $userAgent,
                ],
                'multipart' => [
                    [
                        'name' => 'token',
                        'contents' => $app['config']->get('creitive.craftar.collectionToken'),
                    ],
                ],
            ]);

            $optionMergingClient = new OptionMergingClient($guzzleClient);
            $requestExceptionHandlingClient = new RequestExceptionHandlingClient($optionMergingClient);

            return new Recognition($requestExceptionHandlingClient);
        });
    }
}

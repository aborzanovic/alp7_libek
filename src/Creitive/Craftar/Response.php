<?php
// (C) Catchoom Technologies S.L.
// Licensed under the MIT license.
// https://github.com/Catchoom/craftar-php/blob/master/LICENSE
// All warranties and liabilities are disclaimed.

namespace Creitive\Craftar;

class Response
{
    /**
     * The status code of the response.
     *
     * @var integer
     */
    private $status;

    /**
     * The response headers.
     *
     * @var array
     */
    private $headers;

    /**
     * The response body (JSON-decoded).
     *
     * @var array
     */
    private $body;

    public function __construct($status, array $headers, $body)
    {
        $this->status = $status;
        $this->headers = $headers;
        $this->body = json_decode($body);
    }

    /**
     * Returns the response status.
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the response headers.
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Gets the response body.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Converts the response to an array containing all the properties.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'status' => $this->status,
            'headers' => $this->headers,
            'body' => $this->body,
        ];
    }

    /**
     * Converts the response into a JSON string.
     *
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }

    /**
     * Returns a string representation of the response.
     *
     * Useful for logging/debugging.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toJson();
    }
}

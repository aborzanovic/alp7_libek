<div
    id="{{ $previewsId }}"
    class="dropzone-previews {{ $previewsId }} {{ $sortable }}"
>
</div>
<div
    id="{{ $uploaderId }}"
    class="col-xs-12 {{ $uploaderId }} image-uploader"
    data-dropzone
    data-upload-url="{{ $uploadUrl }}"
    data-existing-images="{{ $jsonImages }}"
    data-previews-container="{{ $previewsId }}"

    @if ($sortUrl)
        data-sort-url="{{ $sortUrl }}"
    @endif
>
    {{ $dropImageText }}
</div>

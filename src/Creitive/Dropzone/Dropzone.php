<?php

namespace Creitive\Dropzone;

use Closure;
use Illuminate\Contracts\View\Factory as View;
use Traversable;

class Dropzone
{
    /**
     * A View factory implementation.
     *
     * @var \Illuminate\Contracts\View\Factory
     */
    protected $view;

    public function __construct(View $view)
    {
        $this->view = $view;
    }

    /**
     * Creates some HTML for the Dropzone.js script.
     *
     * This only makes sense if used with the accompanying JavaScript
     * implementation.
     *
     * @param array $configuration
     * @return string
     */
    public function create(array $configuration)
    {
        $images = $this->getImagesArray($configuration['images'], $configuration['imageCallback']);
        $sortUrl = array_get($configuration, 'sortUrl');
        $dropImageText = array_get($configuration, 'dropImageText', 'Drop images or click here to upload them.');

        $data = [
            'jsonImages' => json_encode($images),
            'previewsId' => $configuration['entityName'].'-previews',
            'uploaderId' => $configuration['entityName'].'-uploader',
            'uploadUrl' => $configuration['uploadUrl'],
            'sortUrl' => $sortUrl,
            'sortable' => $sortUrl ? 'dropzone-sortable' : '',
            'dropImageText' => $dropImageText,
        ];

        return $this->view->make('creitive/dropzone::dropzone', $data);
    }

    /**
     * Applies the provided callback to each of the images, storing the result
     * into an array which is then returned. Basically an `array_map`, but we
     * can't use that because the first argument might not be an actual array.
     *
     * @param \Traversable|array $images
     * @param \Closure $callback
     * @return array
     */
    protected function getImagesArray(Traversable $images, Closure $callback)
    {
        $newImages = [];

        foreach ($images as $image) {
            $newImages[] = $callback($image);
        }

        return $newImages;
    }
}

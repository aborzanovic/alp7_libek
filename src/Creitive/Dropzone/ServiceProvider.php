<?php

namespace Creitive\Dropzone;

use Creitive\Dropzone\Dropzone;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->app->singleton(Dropzone::class, function ($app) {
            $app['view']->addNamespace('creitive/dropzone', __DIR__.'/views');

            return new Dropzone($app['view']);
        });
    }
}

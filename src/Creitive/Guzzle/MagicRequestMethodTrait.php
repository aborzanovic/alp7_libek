<?php

namespace Creitive\Guzzle;

use InvalidArgumentException;

trait MagicRequestMethodTrait
{
    /**
     * A helper method for making magic request methods.
     *
     * Copy-pasted directly from the Guzzle Client implementation.
     *
     * @link https://github.com/guzzle/guzzle/blob/3416e6108e5617ba8626e1cfc92b1ff6a7d1e0df/src/Client.php#L77-L89
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        if (count($args) < 1) {
            throw new InvalidArgumentException('Magic request methods require a URI and optional options array');
        }

        $uri = $args[0];
        $opts = isset($args[1]) ? $args[1] : [];

        return substr($method, -5) === 'Async'
            ? $this->requestAsync(substr($method, 0, -5), $uri, $opts)
            : $this->request($method, $uri, $opts);
    }
}

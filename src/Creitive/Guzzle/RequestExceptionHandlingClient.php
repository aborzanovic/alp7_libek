<?php

namespace Creitive\Guzzle;

use Closure;
use Creitive\Guzzle\MagicRequestMethodTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\RequestInterface;

class RequestExceptionHandlingClient implements ClientInterface
{
    use MagicRequestMethodTrait;

    /**
     * A ClientInterface implementation.
     *
     * @var \GuzzleHttp\ClientInterface
     */
    protected $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * {@inheritDoc}
     */
    public function send(RequestInterface $request, array $options = [])
    {
        return $this->handleRequestExceptions(function () use ($request, $options) {
            return $this->client->send($request, $options);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function sendAsync(RequestInterface $request, array $options = [])
    {
        return $this->handleRequestExceptions(function () use ($request, $options) {
            return $this->client->sendAsync($request, $options);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function request($method, $uri, array $options = [])
    {
        return $this->handleRequestExceptions(function () use ($method, $uri, $options) {
            return $this->client->request($method, $uri, $options);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function requestAsync($method, $uri, array $options = [])
    {
        return $this->handleRequestExceptions(function () use ($method, $uri, $options) {
            return $this->client->requestAsync($method, $uri, $options);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function getConfig($option = null)
    {
        return $this->client->getConfig($option);
    }

    /**
     * Handles `RequestException`s thrown by Guzzle, rethrows anything else.
     *
     * @param \Closure $closure
     * @return \GuzzleHttp\Psr7\Response
     */
    protected function handleRequestExceptions(Closure $closure)
    {
        try {
            return $closure();
        } catch (RequestException $exception) {
            if ($exception->hasResponse()) {
                return $exception->getResponse();
            } else {
                throw $exception;
            }
        }
    }
}

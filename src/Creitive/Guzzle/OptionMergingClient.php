<?php

namespace Creitive\Guzzle;

use Creitive\Guzzle\MagicRequestMethodTrait;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\RequestInterface;

class OptionMergingClient implements ClientInterface
{
    use MagicRequestMethodTrait;

    /**
     * A ClientInterface implementation.
     *
     * @var \GuzzleHttp\ClientInterface
     */
    protected $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * {@inheritDoc}
     */
    public function send(RequestInterface $request, array $options = [])
    {
        return $this->client->send($request, $this->mergeOptions($options));
    }

    /**
     * {@inheritDoc}
     */
    public function sendAsync(RequestInterface $request, array $options = [])
    {
        return $this->client->sendAsync($request, $this->mergeOptions($options));
    }

    /**
     * {@inheritDoc}
     */
    public function request($method, $uri, array $options = [])
    {
        return $this->client->request($method, $uri, $this->mergeOptions($options));
    }

    /**
     * {@inheritDoc}
     */
    public function requestAsync($method, $uri, array $options = [])
    {
        return $this->client->requestAsync($method, $uri, $this->mergeOptions($options));
    }

    /**
     * {@inheritDoc}
     */
    public function getConfig($option = null)
    {
        return $this->client->getConfig($option);
    }

    /**
     * Recursively merges all default options with the passed options.
     *
     * @param array $options
     * @return array
     */
    protected function mergeOptions(array $options)
    {
        return array_merge_recursive($options, $this->getConfig());
    }
}

<?php

namespace Creitive\Mandrill;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Mandrill;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->app->singleton(Mandrill::class, function ($app) {
            return new Mandrill($app['config']->get('mandrill.apiKey'));
        });
    }
}

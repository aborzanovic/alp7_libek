<?php

namespace Creitive\Mandrill\Mailer;

use Creitive\Mandrill\Mailer;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Mandrill;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->app->singleton(Mailer::class, function ($app) {
            $config = $app['config']->get('mandrill');
            $mandrill = $app->make(Mandrill::class);

            return new Mailer(
                $mandrill,
                $app['log'],
                $config['fromEmail'],
                $config['fromName'],
                $config['replyToEmail'],
                $config['replyToName'],
                $config['async'],
                $config['ipPool']
            );
        });
    }
}

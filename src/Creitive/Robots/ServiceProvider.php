<?php

namespace Creitive\Robots;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Illuminate\Contracts\Routing\ResponseFactory;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        /*
         * Configure the package.
         */

        $this->loadViewsFrom(__DIR__.'/views', 'creitive/robots');

        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/vendor/creitive/robots'),
        ], 'views');

        /*
         * Check if we have an environment-specific `robots.txt` view.
         */

        $env = $this->app->environment();

        if ($this->app['view']->exists("creitive/robots::{$env}/robots")) {
            $viewPath = "creitive/robots::{$env}/robots";
        } else {
            $viewPath = "creitive/robots::robots";
        }

        $contents = $this->app['view']->make($viewPath)->render();

        /*
         * Register the route.
         */

        $responseFactory = $this->app[ResponseFactory::class];

        $this->app['router']->get('robots.txt', function () use ($responseFactory, $contents) {
            $response = $responseFactory->make($contents, 200, ['Content-Type' => 'text/plain']);

            /*
             * @todo Figure out how to configure proper caching.
             */

            $response->setCache([
                'public' => true,
            ]);

            return $response;
        });
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {
    }
}

<?php

namespace Creitive\Html;

use InvalidArgumentException;

/**
 * A class to handle CSS class manipulation.
 *
 * @copyright ©2013 CreITive (http://www.creitive.rs)
 */
class CssClassHandler
{
    /**
     * The currently set classes.
     *
     * @var array
     */
    protected $classes = [];

    public function __construct($classes = '')
    {
        if ($classes) {
            $this->addClasses($classes);
        }
    }

    /**
     * Adds one or more classes.
     *
     * Accepts an array, or a string with space-separated classes.
     *
     * @param array|string $classes
     */
    public function addClasses($classes)
    {
        if (is_string($classes)) {
            $classes = explode(' ', $classes);
        }

        if (!is_array($classes)) {
            throw new InvalidArgumentException(
                'CssClassHandler::addClasses() only accepts strings or arrays, but '
                . (is_object($classes) ? get_class($classes) : gettype($classes))
                . ' given: ' . print_r($classes, true)
            );
        }

        foreach ($classes as $class) {
            if (!is_string($class)) {
                throw new InvalidArgumentException(
                    'CssClassHandler::addClasses() requires all array values to be strings, but '
                    . (is_object($class) ? get_class($class) : gettype($class))
                    . ' given: ' . print_r($class, true)
                );
            }

            if (strpos($class, ' ') !== false) {
                $this->addClasses($class);
            } elseif ($class) {
                $this->classes[] = $class;
                $this->classes = array_unique($this->classes);
            }
        }
    }

    /**
     * Removes one or more classes.
     *
     * Accepts an array, or a string with space-separated classes.
     *
     * @param  array|string $classes
     */
    public function removeClasses($classes)
    {
        if (is_string($classes)) {
            $classes = explode(' ', $classes);
        }

        if (!is_array($classes)) {
            throw new InvalidArgumentException(
                'CssClassHandler::removeClasses() only accepts strings or arrays, but '
                . (is_object($classes) ? get_class($classes) : gettype($classes))
                . ' given: ' . print_r($classes, true)
            );
        }

        foreach ($classes as $class) {
            if (!is_string($class)) {
                throw new InvalidArgumentException(
                    'CssClassHandler::removeClasses() requires all array values to be strings, but '
                    . (is_object($class) ? get_class($class) : gettype($class))
                    . ' given: ' . print_r($class, true)
                );
            }

            if (strpos($class, ' ') !== false) {
                $this->removeClasses($class);
            } else {
                $key = array_search($class, $this->classes);

                if ($key !== false) {
                    unset($this->classes[$key]);
                }
            }
        }
    }

    /**
     * Merges all the classes with a space character, as per the HTML standard.
     *
     * @return string
     */
    public function __toString()
    {
        return implode(' ', $this->classes);
    }
}

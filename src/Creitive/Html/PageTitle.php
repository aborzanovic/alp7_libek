<?php

namespace Creitive\Html;

use InvalidArgumentException;

/**
 * A class to handle page title generation.
 *
 * Four page title segments are provided: subpage, page, site name, and tagline.
 *
 * This class intentionally doesn't allow additional page title segments, since
 * they are useless in practice - most browsers' tab widths can rarely show even
 * the four provided properties, and search engines cut off page titles after a
 * little over 60 characters.
 *
 * Adding more page title segments serves no practical use, at least in
 * virtually *all* cases the developers of this class have needed to generate
 * page titles.
 *
 * @copyright ©2013 CreITive (http://www.creitive.rs)
 */
class PageTitle
{
    /**
     * The title tagline.
     *
     * This will be appended to the end of the title string.
     *
     * @var string
     */
    protected $tagline = '';

    /**
     * The site name.
     *
     * This is usually the last part of the page title, except for the tagline,
     * if it's present.
     *
     * @var string
     */
    protected $siteName = '';

    /**
     * The name of the current page.
     *
     * This should be descriptive of the page the user is currently viewing.
     *
     * @var string
     */
    protected $page = '';

    /**
     * The subpage name.
     *
     * In cases where the "page" variable is not enough to define where the user
     * is located, this may be used to provide further categorization, e.g:
     *
     *     Product name | Category | Site name
     *
     * In a case like this, the "Category" would be the page, while the "Product
     * name" would be the subpage.
     *
     * @var string
     */
    protected $subPage = '';

    /**
     * The divider between page title segments.
     *
     * Spaces around the divider are *not* automatically added, and should be
     * explicitly configured as a part of the divider.
     *
     * @var string
     */
    protected $divider = ' | ';

    /**
     * If this property is set, it will override the complete title, instead of
     * it being built with the provided parts.
     *
     * If it's set to `null`, the title will be built.
     *
     * Note that an empty string *will* override the complete title.
     *
     * @var string|null
     */
    protected $override = null;

    /**
     * Sets the tagline.
     *
     * @param string $tagline
     */
    public function setTagline($tagline)
    {
        if (!is_string($tagline)) {
            throw new InvalidArgumentException(
                'PageTitle::setTagline() only accepts strings, but '
                . (is_object($tagline) ? get_class($tagline) : gettype($tagline))
                . ' given: ' . print_r($tagline, true)
            );
        }

        $this->tagline = $tagline;
    }

    /**
     * Gets the tagline.
     *
     * @return string
     */
    public function getTagline()
    {
        return $this->tagline;
    }

    /**
     * Sets the site name.
     *
     * @param string $siteName
     */
    public function setSiteName($siteName)
    {
        if (!is_string($siteName)) {
            throw new InvalidArgumentException(
                'PageTitle::setSiteName() only accepts strings, but '
                . (is_object($siteName) ? get_class($siteName) : gettype($siteName))
                . ' given: ' . print_r($siteName, true)
            );
        }

        $this->siteName = $siteName;
    }

    /**
     * Gets the site name.
     *
     * @return string
     */
    public function getSiteName()
    {
        return $this->siteName;
    }

    /**
     * Sets the page.
     *
     * @param string $page
     */
    public function setPage($page)
    {
        if (!is_string($page)) {
            throw new InvalidArgumentException(
                'PageTitle::setPage() only accepts strings, but '
                . (is_object($page) ? get_class($page) : gettype($page))
                . ' given: ' . print_r($page, true)
            );
        }

        $this->page = $page;
    }

    /**
     * Gets the page.
     *
     * @return string
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Sets the subpage.
     *
     * @param string $subPage
     */
    public function setSubPage($subPage)
    {
        if (!is_string($subPage)) {
            throw new InvalidArgumentException(
                'PageTitle::setSubPage() only accepts strings, but '
                . (is_object($subPage) ? get_class($subPage) : gettype($subPage))
                . ' given: ' . print_r($subPage, true)
            );
        }

        $this->subPage = $subPage;
    }

    /**
     * Gets the subpage.
     *
     * @return string
     */
    public function getSubPage()
    {
        return $this->subPage;
    }

    /**
     * Sets the divider.
     *
     * Don't forget to explicitly add spaces around the divider, in case you
     * need them (very likely that you do).
     *
     * @param string $divider
     */
    public function setDivider($divider)
    {
        if (!is_string($divider)) {
            throw new InvalidArgumentException(
                'PageTitle::setDivider() only accepts strings, but '
                . (is_object($divider) ? get_class($divider) : gettype($divider))
                . ' given: ' . print_r($divider, true)
            );
        }

        $this->divider = $divider;
    }

    /**
     * Gets the divider.
     *
     * @return string
     */
    public function getDivider()
    {
        return $this->divider;
    }

    /**
     * Sets the title override.
     *
     * @param string|null $override
     */
    public function setOverride($override = null)
    {
        if (!is_string($override) && !is_null($override)) {
            throw new InvalidArgumentException(
                'PageTitle::setOverride() only accepts strings, or `null` but '
                . (is_object($override) ? get_class($override) : gettype($override))
                . ' given: ' . print_r($override, true)
            );
        }

        $this->override = $override;
    }

    /**
     * Gets the override.
     *
     * @return string|null
     */
    public function getOverride()
    {
        return $this->override;
    }

    /**
     * Merges all title segments into a single string (separated with the
     * configured divider), and returns it, to allow for easy output - just echo
     * the object anywhere you need (most likely in the `<title>` tags).
     *
     * @return string
     */
    public function render()
    {
        if (!is_null($this->override)) {
            return $this->override;
        }

        $titleParts = array();

        if ($this->subPage) {
            $titleParts[] = $this->subPage;
        }

        if ($this->page) {
            $titleParts[] = $this->page;
        }

        if ($this->siteName) {
            $titleParts[] = $this->siteName;
        }

        if ($this->tagline) {
            $titleParts[] = $this->tagline;
        }

        return implode($this->divider, $titleParts);
    }

    /**
     * __toString magic method.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }
}

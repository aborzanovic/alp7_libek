<?php

namespace Creitive\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class ForceSsl
{
    /**
     * Whether to force SSL connections.
     *
     * @var boolean
     */
    protected $forceSsl;

    /**
     * A Redirector instance.
     *
     * @var \Illuminate\Routing\Redirector
     */
    protected $redirect;

    public function __construct($forceSsl, Redirector $redirect)
    {
        $this->forceSsl = $forceSsl;
        $this->redirect = $redirect;
    }

    /**
     * Redirects the user to the SSL version of the current URI, if the the
     * current request is not made via SSL, and the middleware is configured to
     * force SSL connections.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->forceSsl && ! $request->secure()) {
            return $this->redirect->secure($request->getRequestUri());
        }

        return $next($request);
    }
}

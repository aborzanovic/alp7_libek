/**
 * Auto-aliasify configuration.
 *
 * These modules will automatically be aliasified relative to the root path for
 * script sources, so that we can more easily require them in other modules (as
 * if they were installed via npm).
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

module.exports = {
  aliases: [
    'creitive/cms',
    'creitive/cms/sortable',
    'creitive/core',
    'creitive/core/ajaxLoader',
    'creitive/core/api',
    'creitive/core/clickPropagators',
    'creitive/core/csrf',
    'creitive/core/environment',
    'creitive/core/errorDialog',
    'creitive/core/keyCode',
    'creitive/core/language',
    'creitive/core/logger',
    'creitive/core/util',
    'creitive/forms',
    'creitive/login'
  ],
  configDir: require('./scripts').source
};

'use strict';

var gulp = require('gulp');
var buildPlugins = require('../lib/buildPlugins');
var config = require('../config').scripts;

gulp.task('build-plugins', function() {
  for (var target in config.plugins) {
    if (config.plugins.hasOwnProperty(target)) {
      buildPlugins(target, config.vendorRoot, config.plugins[target], config.destination);
    }
  }
});

'use strict';

var gulp = require('gulp');

gulp.task(
  'default',
  [
    'build-plugins',
    'lint',
    'build-scripts'
  ]
);

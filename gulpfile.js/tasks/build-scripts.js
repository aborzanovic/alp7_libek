'use strict';

var gulp = require('gulp');
var buildScripts = require('../lib/buildScripts');
var basename = require('../lib/basename');
var withFilesIn = require('../lib/withFilesIn');
var config = require('../config');

gulp.task('build-scripts', function() {
  withFilesIn(config.scripts.source, function(filename) {
    buildScripts(
      basename(filename),
      config.scripts.source,
      config.scripts.destination,
      config.autoAliasify
    );
  });
});

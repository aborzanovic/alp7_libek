'use strict';

var gulp = require('gulp');
var watchScripts = require('../lib/watchScripts');
var basename = require('../lib/basename');
var withFilesIn = require('../lib/withFilesIn');
var config = require('../config');

gulp.task('watch-scripts', function() {
  withFilesIn(config.scripts.source, function(filename) {
    watchScripts(
      basename(filename),
      config.scripts.source,
      config.scripts.destination,
      config.autoAliasify
    );
  });
});

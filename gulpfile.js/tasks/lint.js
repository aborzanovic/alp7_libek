'use strict';

var gulp = require('gulp');
var argv = require('yargs')
  .boolean('lint')
  .default('lint', true)
  .argv;
var lint = require('../lib/lint');
var scripts = require('../config').scripts.linting;

gulp.task('lint', function() {
  if (!argv.lint) {
    console.log('Skipping linting because --no-lint was passed.');

    return;
  }

  lint(scripts);
});

'use strict';

var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var pngcrush = require('imagemin-pngcrush');

/**
 * Optimizes project images.
 *
 * @param {Array} source
 * @param {String} destination
 * @return {Void}
 */
module.exports = function(source, destination) {
  gulp.src(source)
    .pipe(imagemin({
      progressive: true,
      use: [pngcrush()]
    }))
    .pipe(gulp.dest(destination));
};

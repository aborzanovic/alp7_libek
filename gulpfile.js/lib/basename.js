'use strict';

/**
 * Returns the basename of the file, no matter the extension.
 *
 * @param {String} filename
 * @return {String}
 */
module.exports = function(filename) {
  return require('path').parse(filename).name;
};

'use strict';

var glob = require('glob');
var _ = require('lodash');

/**
 * Deglobs the passed arguments.
 *
 * @link http://stackoverflow.com/questions/28486866/gulp-src-using-sync-globbing/28548018#28548018
 * @return {Array}
 */
module.exports = function() {
  var syncGlob = glob.sync;
  var patterns = _.flatten(arguments, true);

  return _.flatten(patterns.map(function(pattern) {
    return syncGlob(pattern).map(function(file) {
      return pattern.charAt(0) === '!' ? ('!' + file) : file;
    });
  }), true);
};

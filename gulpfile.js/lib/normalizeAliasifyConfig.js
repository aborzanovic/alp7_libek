'use strict';

/**
 * Normalizes the paths in the aliasify config.
 *
 * @param {Object} aliasifyConfig
 * @return {Object}
 */
module.exports = function(aliasifyConfig) {
  var aliasifyAliases = {};

  aliasifyConfig.aliases.forEach(function(alias) {
    aliasifyAliases[alias] = './' + alias;
  });

  return {
    aliases: aliasifyAliases,
    configDir: aliasifyConfig.configDir
  };
};

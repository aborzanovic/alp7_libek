'use strict';

var gulp = require('gulp');
var jshint = require('gulp-jshint');
var jscs = require('gulp-jscs');

/**
 * Checks all source files to see if they conform to the required coding
 * standards, as defined by the .jshintrc and .jscsrc config files.
 *
 * @param {Array} scripts
 * @return {Void}
 */
module.exports = function(scripts) {
  gulp.src(scripts)
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(jshint.reporter('fail'))
    .pipe(jscs());
};

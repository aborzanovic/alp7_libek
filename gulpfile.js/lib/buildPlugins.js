'use strict';

/**
 * Plugin dependencies.
 */

var gulp = require('gulp');
var bower = require('bower');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var fs = require('fs');
var deglob = require('./deglob');
var isGlob = require('./isGlob');

/**
 * Compiles full paths to plugins.
 *
 * @param {String} vendorRoot
 * @param {Array} plugins
 * @return {Array}
 */
var compilePluginPaths = function(vendorRoot, plugins) {
  return plugins.map(function(plugin) {
    return vendorRoot + '/' + plugin;
  });
};

/**
 * Checks whether all of the passed files exist.
 *
 * @param {Array} files
 * @return {boolean}
 */
var foundAll = function(files) {
  var foundAll = true;

  files.forEach(function(file) {
    if (isGlob(file)) {
      return;
    }

    if (!fs.existsSync(file)) {
      console.log('File not found: ' + file);

      foundAll = false;
    }
  });

  return foundAll;
};

/**
 * Builds the specified plugins.
 *
 * @param {String} name
 * @param {String} vendorRoot
 * @param {Array} plugins
 * @param {String} destinationPath
 * @return {Void}
 */
module.exports = function(name, vendorRoot, plugins, destinationPath) {
  var completeFilename = 'plugins.' + name + '.js';
  var minifiedFilename = 'plugins.' + name + '.min.js';
  var concatConfig = {
    newLine: '\n'
  };

  plugins = compilePluginPaths(vendorRoot, plugins);

  bower.commands
    .install()
    .on('end', function() {
      if (!foundAll(plugins)) {
        console.log('Aborting build due to missing files!');

        return;
      }

      gulp.src(deglob(plugins))
        .pipe(concat(completeFilename, concatConfig))
        .pipe(gulp.dest(destinationPath))
        .pipe(uglify())
        .pipe(rename(minifiedFilename))
        .pipe(gulp.dest(destinationPath));
    });
};

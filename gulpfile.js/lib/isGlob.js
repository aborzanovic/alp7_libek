'use strict';

/**
 * Checks whether a path is a glob.
 *
 * Only does a poor man's check that works for our requirements, by checking
 * whether there is an asterisk in the string.
 *
 * @param {String} path
 * @return {Boolean}
 */
module.exports = function(path) {
  return (path.indexOf('*') !== -1);
};

(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
/**
 * Handles the login JS that autofocuses the first input field, and provides a
 * nice fade-in welcome effect for the login form.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = (typeof window !== "undefined" ? window.jQuery : typeof global !== "undefined" ? global.jQuery : null);

var creitive = {};

creitive.login = require('./creitive/login');
creitive.forms = require('./creitive/forms');

/**
 * Initializes the main module.
 *
 * @return {Void}
 */
creitive.initialize = function() {
  creitive.login.initialize();
  creitive.forms.initialize();
};

$(document).ready(function() {
  creitive.initialize();
});

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./creitive/forms":3,"./creitive/login":4}],2:[function(require,module,exports){
(function (global){
/**
 * Initializes Bootstrap's "loading" status for buttons with the
 * `data-loading-text` attribute configured.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = (typeof window !== "undefined" ? window.jQuery : typeof global !== "undefined" ? global.jQuery : null);

module.exports = {

  /**
   * Initializes the module.
   *
   * @return {Void}
   */
  initialize: function() {
    $('button[data-loading-text]').each(function(index, element) {
      /**
       * The button jQuery object.
       *
       * @type {Object}
       */
      var $element = $(element);

      /**
       * The parent form element, if one exists.
       *
       * @type {Object}
       */
      var $form = $element.closest('form');

      /**
       * Whether the button is within a form element.
       *
       * @type {Boolean}
       */
      var isInForm = $form.length;

      /**
       * When the button is clicked, we want to trigger the "loading" status,
       * and disable the button. However, we need to hack this up a bit so it
       * works correctly when a nested button element is clicked (for more
       * info, see the linked issue).
       *
       * Note that the Bootstrap version used when writing this plugin is
       * Bootstrap 3.2.0, but this problem was "fixed" (in a roundabout way)
       * in more recent Bootstrap versions.
       *
       * @link https://github.com/twbs/bootstrap/issues/14450
       * @link https://github.com/twbs/bootstrap/pull/14457
       */
      $element.on('click', function(event) {
        var $target = $(event.target);

        if (!$target.is('button')) {
          event.stopPropagation();

          $target.closest('button').click();

          return;
        }

        /**
         * We'll only trigger the "loading" state in case the button is not in
         * a form, or when the form is valid - otherwise the button will get
         * locked in the "loading" state, and the user won't be able to submit
         * the form in a user-friendly manner.
         */
        if (!isInForm || $form.get(0).checkValidity()) {
          $element.button('loading');
        }
      });

    });
  }

};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],3:[function(require,module,exports){
/**
 * Initializes form-related modules.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var forms = {};

forms.bootstrapButtonLoadingText = require('./bootstrapButtonLoadingText');

/**
 * Initializes the forms module.
 *
 * @return {Void}
 */
forms.initialize = function() {
  forms.bootstrapButtonLoadingText.initialize();
};

module.exports = forms;

},{"./bootstrapButtonLoadingText":2}],4:[function(require,module,exports){
(function (global){
/**
 * Creitive CMS login module.
 *
 * Displays a caps lock warning for the password input, and performs a fade-in
 * animation of the login form.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = (typeof window !== "undefined" ? window.jQuery : typeof global !== "undefined" ? global.jQuery : null);

var login = {};

/**
 * The password input element.
 *
 * @type {Object}
 */
var passwordInput;

/**
 * Whether caps lock is turned on.
 *
 * @type {Boolean|null}
 */
var capsLockOn = null;

/**
 * Whether the password input is focused.
 *
 * @type {Boolean}
 */
var passwordFocused = false;

/**
 * Displays the caps lock warning if the user is in the password input, and caps
 * lock is turned on - hides it otherwise.
 *
 * @return {Void}
 */
var updateCapsLockWarning = function() {
  if (passwordFocused && capsLockOn) {
    passwordInput.tipsy('show', { html: true });
  } else {
    passwordInput.tipsy('hide');
  }
};

/**
 * Initializes the module.
 *
 * @return {Void}
 */
login.initialize = function() {
  passwordInput = $('input.password-input');

  /*
   * Initialize the capslockstate plugin.
   */
  $(window).capslockstate();

  /**
   * Set up tipsy on the password input.
   */
  passwordInput.tipsy({
    trigger: 'manual',
    gravity: 'w'
  });

  /**
   * Set up caps lock warning events.
   */

  $(window).bind('capsOn', function() {
    capsLockOn = true;
    updateCapsLockWarning();
  });

  $(window).bind('capsOff', function() {
    capsLockOn = false;
    updateCapsLockWarning();
  });

  passwordInput.on('focus', function() {
    passwordFocused = true;
    updateCapsLockWarning();
  });

  passwordInput.on('blur', function() {
    passwordFocused = false;
    updateCapsLockWarning();
  });
};

module.exports = login;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}]},{},[1]);

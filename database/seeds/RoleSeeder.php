<?php

use Cartalyst\Sentinel\Roles\EloquentRole;
use Cartalyst\Sentinel\Sentinel;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * A Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $sentinel;

    /**
     * Role configuration.
     *
     * @var array
     */
    protected $roles = [
        [
            'name' => 'Admin',
            'slug' => 'admin',
            'permissions' => [
                'App\Http\Controllers\Admin\HomeController@index' => true,

                'App\Redactor\Http\Controllers\Admin\File\Controller@store' => true,
                'App\Redactor\Http\Controllers\Admin\Image\Controller@store' => true,

                'App\Auth\Http\Controllers\Admin\User\Controller@index' => true,
                'App\Auth\Http\Controllers\Admin\User\Controller@create' => true,
                'App\Auth\Http\Controllers\Admin\User\Controller@store' => true,
                'App\Auth\Http\Controllers\Admin\User\Controller@edit' => true,
                'App\Auth\Http\Controllers\Admin\User\Controller@update' => true,
                'App\Auth\Http\Controllers\Admin\User\Controller@confirmDelete' => true,
                'App\Auth\Http\Controllers\Admin\User\Controller@delete' => true,

                'App\Auth\Http\Controllers\Admin\Password\Controller@index' => true,
                'App\Auth\Http\Controllers\Admin\Password\Controller@update' => true,

                'App\Http\Controllers\Admin\LoginController@logout' => true,
            ],
        ],
        [
            'name' => 'User',
            'slug' => 'user',
            'permissions' => [],
        ],
    ];

    public function __construct(Sentinel $sentinel)
    {
        $this->sentinel = $sentinel;
    }

    /**
     * {@inheritDoc}
     */
    public function run()
    {
        foreach ($this->roles as $roleConfig)
        {
            $role = $this->sentinel->findRoleBySlug($roleConfig['slug']);

            if (is_null($role)) {
                $this->createRole($roleConfig);
            } else {
                $this->updateRole($role, $roleConfig);
            }
        }
    }

    /**
     * Creates the passed role.
     *
     * @param array $roleConfig
     * @return void
     */
    protected function createRole(array $roleConfig)
    {
        $this->sentinel->getRoleRepository()->createModel()->create($roleConfig);
    }

    /**
     * Updates the passed role with the specified parameters.
     *
     * @param \Cartalyst\Sentinel\Roles\EloquentRole $role
     * @param array $roleConfig
     * @return void
     */
    protected function updateRole(EloquentRole $role, array $roleConfig)
    {
        $role->name = $roleConfig['name'];
        $role->permissions = $roleConfig['permissions'];

        $role->save();
    }
}

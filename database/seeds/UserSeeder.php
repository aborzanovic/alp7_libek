<?php

use App\Auth\User;
use Cartalyst\Sentinel\Sentinel;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * A Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $sentinel;

    /**
     * Users to seed.
     *
     * @var array
     */
    protected $users = [
        [
            'email' => 'admin@libek.org.rs',
            'password' => 'admin',
            'first_name' => 'Super',
            'last_name' => 'Administrator',
            'roles' => ['admin'],
        ],
    ];

    public function __construct(Sentinel $sentinel)
    {
        $this->sentinel = $sentinel;
    }

    /**
     * {@inheritDoc}
     */
    public function run()
    {
        foreach ($this->users as $userConfig) {
            if (!$this->userExists($userConfig['email'])) {
                $this->createUser($userConfig);
            }
        }
    }

    /**
     * Checks whether a user with the given email exists.
     *
     * @param string $email
     * @return boolean
     */
    protected function userExists($email)
    {
        return User::whereEmail($email)->exists();
    }

    /**
     * Creates a new user according to the specified config, and assigns the
     * roles.
     *
     * @param array $userConfig
     * @return void
     */
    protected function createUser(array $userConfig)
    {
        $roles = $userConfig['roles'];

        unset($userConfig['roles']);

        $user = $this->sentinel->registerAndActivate($userConfig);

        foreach ($roles as $role) {
            $this->attachToRole($user, $role);
        }
    }

    /**
     * Attaches a user to a role.
     *
     * @param \App\Auth\User $user
     * @param string $roleSlug
     * @return void
     */
    protected function attachToRole(User $user, $roleSlug)
    {
        $role = $this->sentinel->findRoleBySlug($roleSlug);

        $role->users()->attach($user);
    }
}

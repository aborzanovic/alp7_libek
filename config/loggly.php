<?php

return [

    /**
     * The token for submitting data.
     */
    'token' => env('LOGGLY_TOKEN'),

    /**
     * The tag under which the data will be submitted.
     *
     * This is important in order to differentiate between different data
     * sources/applications within Loggly.
     */
    'tag' => env('LOGGLY_TAG'),

];

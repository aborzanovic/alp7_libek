<?php

namespace App\Validation\Validator;

use App\Validation\ValidatorTest;
use App\Validation\Validator;

class ValidateSlugTest extends ValidatorTest
{
    /**
     * Tests whether slug validation works.
     *
     * @return void
     */
    public function testValidateSlug()
    {
        $trans = $this->getTranslator();
        $trans->shouldReceive('trans')->andReturn('');
        $rules = ['slug' => ['slug']];

        $validator = new Validator($trans, ['slug' => 'foo-bar-baz'], $rules);
        $this->assertTrue($validator->passes(), 'Valid slugs must pass validation');

        $validator = new Validator($trans, ['slug' => 'Foo-Bar-Baz'], $rules);
        $this->assertFalse($validator->passes(), 'No uppercase letters are allowed in a slug');

        $validator = new Validator($trans, ['slug' => 'foo--bar--baz'], $rules);
        $this->assertFalse($validator->passes(), 'No consecutive hyphens are allowed in a slug');

        $validator = new Validator($trans, ['slug' => '-foo-bar-baz'], $rules);
        $this->assertFalse($validator->passes(), 'A slug must not start with a hyphen');
    }
}

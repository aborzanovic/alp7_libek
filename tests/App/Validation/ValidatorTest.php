<?php

namespace App\Validation;

use App\TestCase;
use App\Validation\Validator;
use Symfony\Component\Translation\TranslatorInterface;

abstract class ValidatorTest extends TestCase
{
    /**
     * Gets a mock translator for instantiating a validator.
     *
     * @return \Symfony\Component\Translation\TranslatorInterface
     */
    protected function getTranslator()
    {
        return \Mockery::mock(TranslatorInterface::class);
    }
}

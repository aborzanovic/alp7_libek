/**
 * Helper jQuery plugins for various stuff.
 *
 * All of these are defined as methods on the main jQuery object, so as to
 * enable directly calling them, e.g. on the results of selecting some DOM
 * elements via jQuery.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

( function( $ ) {

    /**
     * Flashes the element with a specified color (defaults to white) for the
     * specified duration (defaults to 500ms).
     * @param  {String} color    The requested color
     * @param  {Number} duration The requested duration (in milliseconds)
     * @return {Object}          The original object
     */
    $.fn.flash = function( color, duration ) {
        color = color || "#fff";
        duration = duration || 500;

        return this.effect(
            "highlight",
            {
                color: color,
                duration: duration
            }
        );
    };

    /**
     * Flashes the element with a green color for the specified duration
     * (defaults to 500ms).
     * @param  {Number} duration The requested duration (in milliseconds)
     * @return {Object}          The original object
     */
    $.fn.flashSuccess = function( duration ) {
        duration = duration || 500;
        return this.flash( "#6c6", duration );
    };

    /**
     * Flashes the element with a red color for the specified duration (defaults
     * to 500ms).
     * @param  {Number} duration The requested duration (in milliseconds)
     * @return {Object}          The original object
     */
    $.fn.flashError = function( duration ) {
        duration = duration || 500;
        return this.flash( "#c66", duration );
    };

    /**
     * Flashes the element with a dark gray color for the specified duration
     * (defaults to 500ms).
     * @param  {Number} duration The requested duration (in milliseconds)
     * @return {Object}          The original object
     */
    $.fn.flashInfo = function( duration ) {
        duration = duration || 500;
        return this.flash( "#222", duration );
    };

    /**
     * Creates a comma-concatenated list of all attributes "itemAttribute"
     * belonging to descendants of the current element that match the
     * "itemSelector".
     *
     * Mostly useful for retrieving the attributes of some items, which will
     * later be sent to the server via AJAX - for example, the sort order of
     * some elements, or a list of selected elements.
     * @param  {String} itemSelector  The selector to match against
     * @param  {String} itemAttribute The attribute to concatenate
     * @return {String}               The comma-concatenated list
     */
    $.fn.getItemIdList = function( itemSelector, itemAttribute ) {
        return this
            .find( itemSelector )
            .map( function() {
                return this.getAttribute( itemAttribute );
            })
            .get()
            .join( "," );
    };

    /**
     * Creates a comma-concatenated list of all checked inputs that are
     * descendants of the current element.
     *
     * The usual use-case is to call it from a form element or a fieldset, so as
     * to be able to easily retreive these values for further processing.
     * @return {Object} The original object
     */
    $.fn.getCheckedInputIdList = function() {
        return this.getItemIdList( "input:checked", "value" );
    };

    /**
     * This function can be used by the EWS sorting methods to mark an element
     * as moved after dragging.
     *
     * It basically colors the element's background with either a custom color
     * (which can be passed as an argument), or a default color (a
     * light-yellowish hue).
     *
     * Returns the original object for method chaining.
     * @param  {String} backgroundColor
     * @return {Object}                 The object that was moved
     */
    $.fn.markAsMoved = function( backgroundColor ) {
        backgroundColor = backgroundColor || "#edb";

        return this.animate(
            {
                "background-color": backgroundColor
            },
            400,
            "easeInOutExpo"
        );
    };

    /**
     * Gets a jQuery object's complete HTML code.
     *
     * The native method is better, but isn't supported in Firefox <11, so
     * there's a fallback for that situation.
     *
     * The fallback looks a little bit hackish, but it is actually the best way
     * to do this.
     * @link http://stackoverflow.com/questions/2419749/get-selected-elements-outer-html/11708139#11708139
     * @return {String}
     */
    $.fn.outerHTML = function() {
        var $this = $( this );

        if ( "outerHTML" in $this[0] ) {
            return $this[0].outerHTML;
        } else {
            return $this.clone().wrap( "<div/>" ).parent().html();
        }
    };

    /**
     * Filters a jQuery object collection by an attribute and a value.
     * @param  {String} attr  The attribute name
     * @param  {String} value The requested value
     * @return {Object}       A jQuery object containing the result
     */
    $.fn.filterByAttr = function( attr, value ) {
        /*jshint quotmark: false */

        if ( typeof( attr ) === "undefined" ) {
            throw new Error( "No attribute passed." );
        }

        if ( typeof( value ) === "undefined" ) {
            return this.filter( "[" + attr + "]" );
        }

        return this.filter( '[' + attr + '="' + value + '"]');
    };

    /**
     * Finds children in a jQuery object collection that match an attribute and
     * a value.
     * @param  {String} attr  The attribute name
     * @param  {String} value The requested value
     * @return {Object}       A jQuery object containing the result
     */
    $.fn.findByAttr = function( attr, value ) {
        /*jshint quotmark: false */

        if ( typeof( attr ) === "undefined" ) {
            throw new Error( "No attribute passed." );
        }

        if ( typeof( value ) === "undefined" ) {
            return this.find( "[" + attr + "]" );
        }

        return this.find( '[' + attr + '="' + value + '"]');
    };

    /**
     * Groups a jQuery object collection by a common attribute.
     *
     * The first (and only) argument is either a string or a function. If a
     * string is passed, that attribute will be retrieved for grouping purposes.
     * If a function is passed, it will be called for each item once (and the
     * items will be passed to it as a single argument), and its return value
     * (which must be a string) will be used for grouping.
     *
     * Returns an object with two properties - `items` (containing jQuery
     * objects with related elements in the same property, which corresponds to
     * the value retrieved from the elements), and `itemsUndefined`, which are
     * elements that don't have the attribute at all (contained within a jQuery
     * object for easy manipulation).
     * @param  {Mixed}  attr
     * @return {Object}
     */
    $.fn.groupBy = function( attr ) {
        var items = {};

        var itemsUndefined = $();

        var callback;

        if ( $.isFunction( attr ) ) {
            callback = attr;
        } else {
            callback = function( item ) {
                return $( item ).attr( attr );
            };
        }

        $.each( this, function( index, item ) {
            var value = callback( item );

            if ( typeof( value ) === "undefined" ) {
                itemsUndefined = itemsUndefined.add( $( item ) );
            } else {
                if ( ! items.hasOwnProperty( value ) ) {
                    items[value] = $();
                }

                items[value] = items[value].add( $( item ) );
            }
        });

        return {
            items: items,
            itemsUndefined: itemsUndefined
        };
    };

    /**
     * Returns the size of an "associative array".
     * @link http://stackoverflow.com/questions/5223/length-of-javascript-object-ie-associative-array/11346637#11346637
     * @link http://stackoverflow.com/questions/5223/length-of-javascript-object-ie-associative-array/6700#6700
     * @param  {Object} object
     * @return {Number}
     */
    $.assocArraySize = function( object ) {
        var size = 0;
        var key;

        for ( key in object ) {
            if ( object.hasOwnProperty( key ) ) {
                size++;
            }
        }

        return size;
    };

    /**
     * Returns an array containing the values of the requested attribute of each
     * element in the set of matched elements.
     *
     * This is a needed convenience method because jQuery's built-int `.attr()`
     * method only returns the value of the attribute for the first element in
     * the set of matched elements.
     * @param  {String} attr
     * @return {Array}
     */
    $.fn.allAttributes = function( attr ) {
        return this.map( function( index, element ) {
            return element.getAttribute( attr );
        }).get();
    };

    /**
     * Performs the same as jQuery's `.text()` method, but converting newlines
     * to HTML `<br>`s correctly, and vice-versa.
     * @link   http://stackoverflow.com/questions/4535888/jquery-text-and-newlines/6455874#6455874
     * @param  {String} text
     * @return {Object}
     */
    $.fn.textWithNewlines = function( text ) {
        var htmls = [];

        /**
         * The temporary `<div>` is to perform HTML entity encoding reliably.
         *
         * `document.createElement()` is *much* faster than `jQuery( "<div/>" )`
         * http://stackoverflow.com/questions/268490/
         */
        var tmpDiv = $( document.createElement( "div" ) );

        var lines;

        var i;

        if ( typeof text === "undefined" ) {
            lines = this.html().split( /<br\s*\/?>/g );

            for ( i = 0; i < lines.length; i++ ) {
                htmls.push( tmpDiv.html( lines[i] ).text() );
            }

            return htmls.join( "\n" );
        } else {
            lines = text.split(/\n/);

            for ( i = 0; i < lines.length; i++) {
                htmls.push( tmpDiv.text( lines[i] ).html() );
            }

            return this.html( htmls.join( "<br>" ) );
        }
    };

    /**
     * Merges multiple objects into a single object.
     *
     * Accepts any number of arguments, but all must be objects.
     *
     * Identically named properties from latter objects will overwrite
     * previously parsed ones.
     * @param  {Object} $object, ...
     * @return {Object}
     */
    $.mergeObjects = function() {
        var args = Array.prototype.slice.call( arguments );

        args.unshift( {} );

        return $.extend.apply( null, args );
    };

}( jQuery ));

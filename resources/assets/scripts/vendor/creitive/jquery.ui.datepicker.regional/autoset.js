/**
 * Datepicker regional settings
 *
 * Automatically sets the regional settings for the jQueryUI datepicker plugin,
 * based on the `lang` attribute of the document's `<html>` tag.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

( function( $ ) {

    "use strict";

    $( document ).on( "ready", function() {
        var language = $( "html" ).attr( "lang" );

        if ( language && $.datepicker.regional.hasOwnProperty( language ) ) {
            $.datepicker.setDefaults( $.datepicker.regional[ language ] );
        }
    });

})( jQuery );

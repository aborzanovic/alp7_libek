/**
 * Serbian language definitions for the jQuery UI Datepicker plugin.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

( function( $ ) {
    /*jshint sub: true */

    /**
     * This used to be "sr-SR", but we're renaming it to just "sr" to make it
     * easier to sync up the datepicker regional settings with the document
     * language as defined in the `lang` attribute of the `<html>` element.
     */
    $.datepicker.regional[ "sr" ] = {
        closeText: "Zatvorite",
        currentText: "Danas",
        dateFormat: "dd.mm.yy.",
        dayNames: [ "Nedelja", "Ponedeljak", "Utorak", "Sreda", "Četvrtak", "Petak", "Subota" ],
        dayNamesMin: [ "Ne", "Po", "Ut", "Sr", "Če", "Pe", "Su" ],
        dayNamesShort: [ "Ned", "Pon", "Uto", "Sre", "Čet", "Pet", "Sub" ],
        firstDay: 1,
        isRTL: false,
        monthNames: [ "Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar" ],
        monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec" ],
        nextText: "Sledeći",
        prevText: "Prethodni",
        showMonthAfterYear: false,
        weekHeader: "Sed",
        yearSuffix: ""
    };

})( jQuery );

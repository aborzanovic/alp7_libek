/**
 * `<noscript>` fix for Internet Explorer.
 *
 * Depends on jQuery (doesn't load otherwise).
 *
 * There are no words to describe the stupidity of having to hide the `noscript`
 * element with JavaScript, as that defeats the purpose of *having* that element
 * in the first place, but that's IE for you.
 *
 * Apparently, the problem is that the `<noscript>` element is styled, and IE
 * doesn't like this. The solution is to wrap the contents in a `<div>` within
 * the `<noscript>` tags, and style that element instead. Unfortunately, in that
 * case, we might have problems with the `z-index`, so we will put this problem
 * aside for now, and solve and test it some other time.
 *
 * There is a non-JS solution to this problem, and we'll implement it later, at
 * which point this script should be removed from the system.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

if ( jQuery ) {
    jQuery( "noscript" ).hide();
}

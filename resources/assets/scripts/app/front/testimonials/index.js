/**
 * Testimonials slider initialization.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

module.exports = {
  /**
   * Initializes the testimonials slider.
   *
   * @param {Object} $slider
   * @return {Void}
   */
  initialize: function($slider) {
    $slider.owlCarousel({
      autoPlay: 8000,
      stopOnHover : true,
      navigation: false,
      pagination: false,
      singleItem: true
    });
  }
};

/**
 * Front-end scripts.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');

module.exports = {
  /**
   * Initializes the front-end scripts.
   *
   * @return {Void}
   */
  initialize: function() {
    require('./testimonials').initialize($('.testimonials'));
  }
};

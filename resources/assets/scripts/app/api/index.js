/**
 * API methods.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var api = require('creitive/core/api');

module.exports = {
  /**
   * Posts the new pages sort order.
   *
   * @param {String} url
   * @param {Array} items
   * @param {Function} callbackSuccess
   * @param {Function} callbackError
   * @param {Function} callbackComplete
   * @return {Void}
   */
  putSort: function(url, items, callbackSuccess, callbackError, callbackComplete) {
    api.put({
      url: url,
      data: {
        items: items
      },
      callbackSuccess: callbackSuccess,
      callbackError: callbackError,
      callbackComplete: callbackComplete
    });
  }
};

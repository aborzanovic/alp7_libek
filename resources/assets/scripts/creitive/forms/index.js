/**
 * Initializes form-related modules.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var forms = {};

forms.bootstrapButtonLoadingText = require('./bootstrapButtonLoadingText');

/**
 * Initializes the forms module.
 *
 * @return {Void}
 */
forms.initialize = function() {
  forms.bootstrapButtonLoadingText.initialize();
};

module.exports = forms;

/**
 * EWS sorting plugin.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');
var ajaxLoader = require('creitive/core/ajaxLoader');
var csrf = require('creitive/core/csrf');
var errorDialog = require('creitive/core/errorDialog');

/**
 * Gets the item IDs within a container, which should be stored in a
 * `data-id` attribute of each item.
 *
 * @param {Object} $container
 * @return {Array}
 */
var getItemIds = function($container) {
  return $container
    .children()
    .map(function() {
      return this.getAttribute('data-id');
    })
    .get();
};

/**
 * Updates the sort order of the items within the passed container.
 *
 * @param {Object} $container
 * @return {Void}
 */
var updateOrder = function($container) {
  var data = {
    'items[]': getItemIds($container)
  };

  ajaxLoader.show();

  $.ajax({
    type: 'PUT',
    url: $container.data('sortUrl'),
    data: csrf.inject(data),

    /**
     * Flashes the item container as a "successful sort" notification.
     *
     * @return {Void}
     */
    success: function() {
      $container.flashSuccess();
    },

    /**
     * Displays the error message in case something went wrong.
     *
     * @return {Void}
     */
    error: function() {
      errorDialog.show();
    },

    /**
     * Hides the AJAX loader indicator once the request finishes.
     *
     * @return {Void}
     */
    complete: function() {
      ajaxLoader.hide();
    }
  });
};

/**
 * Initializes sorting on a container.
 *
 * @param {Object} container
 * @param {String} sortUrl
 * @param {String|Object} handle
 * @return {[type]}
 */
var sortable = function(container, sortUrl, handle) {
  var $container = $(container);

  $container.sortable({
    tolerance: 'pointer',
    revert: 100,
    distance: 5,
    containment: 'parent',
    cursor: 'move',

    /**
     * Updates the sort order.
     *
     * @return {Void}
     */
    update: function() {
      updateOrder($container);
    }
  });

  if (typeof handle !== 'undefined') {
    $container.sortable('option', 'handle', handle);
  }

  $container.data('sortUrl', sortUrl);
};

module.exports = sortable;

/**
 * This plugin initializes the Redactor editor on all text areas with the
 * `redactor` CSS class.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');
var errorDialog = require('creitive/core/errorDialog');
var csrf = require('creitive/core/csrf');
var language = require('creitive/core/language');

var translator = language.getTranslator();

translator.addTranslations(require('./redactor/_translations'));

/**
 * Initializes Redactor.
 *
 * @param {Object} textarea
 * @return {Void}
 */
var initializeRedactor = function(textarea) {
  var $textarea = $(textarea);
  var textAreaId = $textarea.attr('id');
  var $label = $('label').filterByAttr('for', textAreaId);
  var minHeight = $textarea.attr('data-min-height');

  if (minHeight) {
    minHeight = minHeight.toInteger();
  } else {
    minHeight = 400;
  }

  $textarea.redactor({
    minHeight: minHeight,
    iframe: true,
    css: '/styles/redactor-iframe.css',

    pastePlainText: true,

    imageUpload: '/admin/redactor/images',

    /**
     * The callback executed when an error is encountered during image upload.
     *
     * @return {Void}
     */
    imageUploadErrorCallback: function() {
      errorDialog.show(translator.get('imageUploadError'));
    },

    fileUpload: '/admin/redactor/files',

    /**
     * The callback executed when an error is encountered during file upload.
     *
     * @return {Void}
     */
    fileUploadErrorCallback: function() {
      errorDialog.show(translator.get('fileUploadError'));
    },

    uploadFields: csrf.inject({})
  });

  if ($label.length) {
    $label.on('click', function(event) {
      event.preventDefault();

      $textarea.redactor('focusEnd');
    });
  }
};

module.exports = {
  /**
   * Initializes the plugin on all detected instances.
   *
   * @return {Void}
   */
  initialize: function() {
    $('.redactor').each(function(index, textarea) {
      initializeRedactor(textarea);
    });
  }
};

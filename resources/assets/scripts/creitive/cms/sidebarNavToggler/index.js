/**
 * Togglable sidebar navigation menu.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');

/**
 * The button for toggling the sidebar navigation menu.
 *
 * @type {Object}
 */
var navToggleButton;

/**
 * The sidebar navigation.
 *
 * @type {Object}
 */
var sidebarNav;

/**
 * Whether the sidebar navigation is shown or hidden.
 *
 * @type {Boolean}
 */
var isHidden = true;

var sidebarOverlay;

/**
 * Some utility classes for toggling element state and animations.
 *
 * @type {String}
 */
var CLASS_SLIDEIN = 'sidebarNav--slideIn';
var CLASS_SLIDEOUT = 'sidebarNav--slideOut';
var CLASS_OPEN = 'is-open';

/**
 * Hides the sidebar navigation menu.
 *
 * @return {Void}
 */
var hideNav = function() {
  sidebarNav.removeClass(CLASS_SLIDEIN);
  sidebarNav.addClass(CLASS_SLIDEOUT);

  navToggleButton.removeClass(CLASS_OPEN);

  isHidden = true;
};

/**
 * Shows the sidebar navigation menu.
 *
 * @return {Void}
 */
var showNav = function() {
  sidebarNav.addClass(CLASS_SLIDEIN);
  sidebarNav.removeClass(CLASS_SLIDEOUT);

  navToggleButton.addClass(CLASS_OPEN);

  isHidden = false;
};

/**
 * Toggles the sidebar navigation menu.
 *
 * @type {[type]}
 */
var toggleNav = function() {
  if (isHidden) {
    showNav();
  } else {
    hideNav();
  }
};

module.exports = {
  /**
   * Initializes the toggle sidebar.
   *
   * @return {Void}
   */
  initialize: function() {
    navToggleButton = $('.header-navToggleButton');
    sidebarNav = $('.sidebarNav');
    sidebarOverlay = $('.sidebarNav-overlay');

    navToggleButton.on('click', function(event) {
      event.preventDefault();

      toggleNav();
    });

    sidebarOverlay.on('click', function() {
      toggleNav();
    });

    $('.sidebarNav-list').mCustomScrollbar({
      scrollInertia: 0
    });
  }
};

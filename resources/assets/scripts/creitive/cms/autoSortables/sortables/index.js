/**
 * Initializes the sortable logic for each instance found on the current page.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');
var sortable = require('./sortable');

module.exports = {
  /**
   * Initializes the module.
   *
   * @param {Object} apiReference
   * @return {Void}
   */
  initialize: function(apiReference) {
    $('.itemList--sortable').each(function(index, container) {
      sortable.initialize(container, apiReference);
    });
  }
};

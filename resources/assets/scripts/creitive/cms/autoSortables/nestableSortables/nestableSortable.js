/**
 * Initializes nestable sorting.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');
var errorDialog = require('creitive/core/errorDialog');

/**
 * The API reference.
 *
 * @type {Object}
 */
var api;

var buttonExpandTemplate = '<button class="itemList-expandCollapseButton rippleButton" data-action="expand"><i class="fa fa-plus"></i></button>';
var buttonCollapseTemplate = '<button class="itemList-expandCollapseButton rippleButton" data-action="collapse"><i class="fa fa-minus"></i></button>';

var unsavedSortable = false;

var $container;
var $confirmLink;
var $rootList;
var confirmUrl;

/**
 * Checks whether an item has children.
 *
 * @param {Object} item
 * @return {Boolean}
 */
var hasChildren = function(item) {
  return item.hasOwnProperty('children') && $.isArray(item.children) && item.children.length;
};

/**
 * Normalizes the serialized items into a flat array.
 *
 * @param {Array} nestedItems
 * @param {Number|null} parentId
 * @return {Array}
 */
var normalizeItems = function(nestedItems, parentId) {
  var items = [];

  if (typeof parentId === 'undefined') {
    parentId = null;
  }

  nestedItems.forEach(function(item) {
    items.push({
      id: item.id,
      parentId: parentId
    });

    if (hasChildren(item)) {
      items = items.concat(normalizeItems(item.children, item.id));
    }
  });

  return items;
};

/**
 * Handles changes to the list.
 *
 * @todo Mark items as moved.
 * @return {Void}
 */
var handleChange = function() {
  unsavedSortable = true;
};

/**
 * Handles clicking on the confirm button.
 *
 * @param {Event} event
 * @return {Void}
 */
var handleConfirm = function(event) {
  event.preventDefault();

  $confirmLink.button('loading');

  api.putSort(
    confirmUrl,
    normalizeItems($rootList.nestable('serialize')),
    function() {
      unsavedSortable = false;
    },

    function() {
      errorDialog.show();
    },

    function() {
      $confirmLink.button('reset');
    }

  );
};

module.exports = {
  /**
   * Initializes the nestableSortable logic on the passed container.
   *
   * @param {Object} container
   * @param {Object} apiReference
   * @return {Void}
   */
  initialize: function(container, apiReference) {
    $container = $(container);
    api = apiReference;

    $confirmLink = $container.find('.itemList-confirmLink');
    $rootList = $container.find('.itemList-rootContainer');
    confirmUrl = $confirmLink.attr('data-url');

    $rootList.nestable({
      maxDepth: 5,
      group: 0,
      listNodeName: 'ol',
      itemNodeName: 'li',
      rootClass: 'itemList-rootContainer',
      listClass: 'itemList-list',
      itemClass: 'itemList-item',
      dragClass: 'itemList-item--dragged',
      handleClass: 'itemList-item-contents',
      collapsedClass: 'itemList-item--collapsed',
      placeClass: 'itemList-placeholder',
      noDragClass: 'itemList-noDrag',
      emptyClass: 'itemList-empty',
      expandBtnHTML: buttonExpandTemplate,
      collapseBtnHTML: buttonCollapseTemplate
    });

    $rootList.on('change', handleChange);
    $confirmLink.on('click', handleConfirm);
  }
};

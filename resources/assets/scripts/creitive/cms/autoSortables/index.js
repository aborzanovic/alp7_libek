/**
 * Initializes sortables and nestable sortables for the CMS.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var autoSortables = {};

autoSortables.sortables = require('./sortables');
autoSortables.nestableSortables = require('./nestableSortables');

/**
 * Initializes the auto sortables.
 *
 * @param {Object} apiReference
 * @return {Void}
 */
autoSortables.initialize = function(apiReference) {
  autoSortables.sortables.initialize(apiReference);
  autoSortables.nestableSortables.initialize(apiReference);
};

module.exports = autoSortables;

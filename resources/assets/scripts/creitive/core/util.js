/**
 * These are some basic utility functions for use wherever they are needed.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');
var URI = require('URIjs');
var keyCode = require('./keyCode');

/**
 * A UUID to be used for generating invisible `iframe`s, for AJAX file upload
 * purposes, and, perhaps in the future, other elements with unique IDs.
 *
 * This is basically just a counter that starts at 0, and should be incremented
 * each time its value is used to create some element.
 *
 * @type {Number}
 */
var uuid = 0;

/**
 * The `data-page` attribute of the `<body>` element. Initialized at startup.
 *
 * @type {String}
 */
var bodyDataPage = '';

/**
 * The current page's URI object.
 *
 * @type {URI}
 */
var currentUri = new URI();

var util = {

  /**
   * Returns the root URL of the current page, which refers to the complete
   * domain name, including the protocol.
   *
   * @return {String}
   */
  getRootUrl: function() {
    return this.getUrl('/');
  },

  /**
   * Returns the root URL for all AJAX calls, or an URL to a specific AJAX
   * endpoint, if the argument is provided.
   *
   * @param {String} url
   * @return {String}
   */
  getAjaxUrl: function(url) {
    if (typeof url === 'undefined') {
      return this.getUrl('/ajax/');
    }

    if (url.substr(0, 1) === '/') {
      url = url.substr(1);
    }

    return this.getUrl('/ajax/' + url);
  },

  /**
   * Returns a full URL, including the domain name, to the URL passed as an
   * argument.
   *
   * @param {String} url
   * @return {String}
   */
  getUrl: function(url) {
    return new URI(url).absoluteTo(currentUri).toString();
  },

  /**
   * Returns the hash in the URL of the current page, or an empty string, if
   * the hash doesn't exist.
   *
   * @return {String}
   */
  getHash: function() {
    return window.location.hash.substring(1);
  },

  /**
   * Redirects the client's browser to a new URL.
   *
   * @param {String} url
   * @return {Void}
   */
  redirect: function(url) {
    window.location.href = url;
  },

  /**
   * Refreshes the current page, forcing the browser to refresh from the
   * server, instead of using the cache.
   *
   * @return {Void}
   */
  refresh: function() {
    window.location.reload(true);
  },

  /**
   * Returns a new `<iframe>` element for asynchronous file upload purposes.
   *
   * @param {String} idPrefix
   * @return {Object}
   */
  getUploadIframe: function(idPrefix) {
    var iframeId;
    var iframe;

    idPrefix = idPrefix || 'upload-target';
    iframeId = idPrefix + '-' + uuid;

    iframe = $('<iframe/>').attr({
      id: iframeId,
      name: iframeId
    });

    iframe.hide();

    ++uuid;

    return iframe;
  },

  /**
   * Returns a new `<div>` with a `modal-background` class, prepended to the
   * `<body>` element (so it could overlay all other elements).
   *
   * The `modal-background` CSS style must be included for this to work.
   *
   * @return {Object}
   */
  getModalBackground: function() {
    var modalBackground = $('<div/>');

    modalBackground
      .addClass('modal-background')
      .prependTo($('body'));

    return modalBackground;
  },

  /**
   * Formats the price by dividing it by 100 (since we're storing integers in
   * a `number*100` format, so as to store 2 decimal digits), rounding it to 2
   * digits, and adding a dot as the decimal separator.
   *
   * @param {Number} price
   * @param {String} separator
   * @return {String}
   */
  formatPrice: function(price, separator) {
    if (typeof separator === 'undefined') {
      separator = '';
    }

    var newPrice = (price / 100).toFixed(2);
    var length = newPrice.length;
    var afterDecimal = newPrice.slice(length - 3);
    var beforeDecimal = newPrice.slice(0, -3);
    beforeDecimal = beforeDecimal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator);

    return beforeDecimal + afterDecimal;
  },

  /**
   * Checks the currently loaded page, by inspecting the `data-page` attribute
   * of the `<body>` element.
   *
   * @param {String} page
   * @return {Boolean}
   */
  isPage: function(page) {
    return (bodyDataPage === page);
  },

  /**
   * Initializes the module.
   *
   * @return {Void}
   */
  initialize: function() {
    bodyDataPage = $('body').attr('data-page');

    $(document).on('keyup', function(event) {
      if (event.keyCode === keyCode.escape) {
        $(document).trigger('escapekeyup');
      }
    });
  }

};

module.exports = util;

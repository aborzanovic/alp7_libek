/**
 * A cookie listener library.
 *
 * Allows other scripts to subscribe to changes to a certain cookie. The script
 * watches the cookie for changes, and each time it detects a change, it
 * triggers the callback registered for the event, passing the new cookie value
 * as the only argument.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');

/**
 * The default interval in which to check for cookie changes.
 *
 * @type {Number}
 */
var DEFAULT_INTERVAL = 2000;

/**
 * The array that will hold all the cookies for change checks.
 *
 * @type {Array}
 */
var cookieRegistry = [];

/**
 * The cookie listener function which executes repeatedly (via a `setInterval()`
 * call in the `listen()` function), and calls the passed callback whenever the
 * tracked cookie changes, passing it the new value of the cookie.
 *
 * @param {String} cookieName
 * @param {Function} callback
 * @return {*}
 */
var cookieListenerFunction = function(cookieName, callback) {
  var cookieValue = $.cookie(cookieName);

  if (cookieRegistry[cookieName]) {
    if (cookieValue !== cookieRegistry[cookieName]) {
      cookieRegistry[cookieName] = cookieValue;
      return callback(cookieValue);
    }
  } else {
    cookieRegistry[cookieName] = cookieValue;
    return callback(cookieValue);
  }
};

/**
 * Starts the cookie listener, which executes the provided callback each time
 * the targeted cookie changes its value. The interval is the time between
 * successive checks of the cookie being tracked.
 *
 * @param {String} cookieName
 * @param {Function} callback
 * @param {Number} interval
 * @return {Void}
 */
var listen = function(cookieName, callback, interval) {
  cookieRegistry[cookieName] = $.cookie(cookieName);

  setInterval(cookieListenerFunction, interval, cookieName, callback);
};

var cookieListener = {

  /**
   * Listens for changes to a desired cookie, and executes a callback when a
   * change is encountered.
   *
   * @param {String} cookieName
   * @param {Function} callback
   * @param {Number} interval
   * @return {Void}
   */
  listen: function(cookieName, callback, interval) {
    if (typeof interval === 'undefined') {
      interval = DEFAULT_INTERVAL;
    }

    if (!$.isFunction(callback)) {
      /**
       * A default no-op callback.
       *
       * @return {Function}
       */
      callback = function() {};
    }

    listen(cookieName, callback, interval);
  }

};

module.exports = cookieListener;

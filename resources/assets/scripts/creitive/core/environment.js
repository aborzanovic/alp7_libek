/**
 * Environment detection for JS scripts.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');

/**
 * The current environment.
 *
 * @type {String}
 */
var currentEnvironment = '';

/**
 * Parses the `meta[name="environment"]` tag in the document's `<head>` to fetch
 * the current environment.
 *
 * @return {String}
 */
var getEnvironmentFromMeta = function() {
  var parsedEnvironment = $('meta').filterByAttr('name', 'environment').attr('content');

  return parsedEnvironment || 'production';
};

module.exports = {

  /**
   * Gets the current environment.
   *
   * @return {String}
   */
  get: function() {
    if (!currentEnvironment) {
      throw new Error('The current environment has not been detected yet. Please wait for document.ready to fire.');
    }

    return currentEnvironment;
  },

  /**
   * Sets the current environment.
   *
   * @return {String}
   */
  set: function(newEnvironment) {
    currentEnvironment = newEnvironment;
  },

  /**
   * Checks whether the current environment is the one passed as an argument.
   *
   * @param {String} environment
   * @return {Boolean}
   */
  is: function(environment) {
    return (this.get() === environment);
  },

  /**
   * Initializes the module.
   *
   * @return {Void}
   */
  initialize: function() {
    currentEnvironment = getEnvironmentFromMeta();
  }

};

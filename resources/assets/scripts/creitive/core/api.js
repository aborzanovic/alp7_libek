/**
 * An API request library.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');
var csrf = require('./csrf');

/**
 * The API version in use.
 *
 * @type {Number|String}
 */
var version = 1;

/**
 * Gets the full URL to the specified API endpoint.
 *
 * This method is API-version-aware.
 *
 * If a complete URL (starting with "http://" or "https://") is passed, it won't
 * be modified.
 *
 * @param {String} url
 * @return {String}
 */
var getApiEndpoint = function(url) {
  if (url.startsWith('http://') || url.startsWith('https://')) {
    return url;
  }

  if (url.charAt(0) !== '/') {
    url = '/' + url;
  }

  return '/api/v' + version + url;
};

/**
 * Checks whether an AJAX request was cancelled by the user (which still
 * triggers an error).
 *
 * This is useful in error callbacks, so as to avoid displaying an error message
 * when there was no actual error.
 *
 * @link http://ilikestuffblog.com/2009/11/30/how-to-distinguish-a-user-aborted-ajax-call-from-an-error/
 *
 * @param {Object} xhr
 * @return {Boolean}
 */
var userAborted = function(xhr) {
  return !xhr.getAllResponseHeaders();
};

module.exports = {

  /**
   * Sets the API version in use.
   *
   * @param {Number|String} newVersion
   * @return {Void}
   */
  setVersion: function(newVersion) {
    version = newVersion;
  },

  /**
   * Gets the API version in use.
   *
   * @return {Number|String}
   */
  getVersion: function() {
    return version;
  },

  /**
   * Makes an AJAX request, automatically setting the API endpoint.
   *
   * @param {Object} options
   * @return {Void}
   */
  request: function(options) {
    options.url = getApiEndpoint(options.url);

    if ($.isFunction(options.callbackSuccess)) {
      options.success = options.callbackSuccess;
    }

    if ($.isFunction(options.callbackError) || $.isFunction(options.callbackAbort)) {
      /**
       * The error callack.
       *
       * @param {Object} xhr
       * @param {Number} status
       * @param {String} error
       * @return {Void}
       */
      options.error = function(xhr, status, error) {
        /**
         * Don't execute the error callback if it was a user aborted
         * request - because that's not an actual error. We'll instead
         * call a custom `callbackAbort` function, if it was defined.
         *
         * This basically fixed jQuery's broken handling of these cases.
         */

        if (userAborted(xhr)) {
          if ($.isFunction(options.callbackAbort)) {
            options.callbackAbort(xhr, status, error);
          }

          return;
        }

        if ($.isFunction(options.callbackError)) {
          options.callbackError(xhr, status, error);
        }
      };
    }

    if ($.isFunction(options.callbackComplete)) {
      options.complete = options.callbackComplete;
    }

    $.ajax(options);
  },

  /**
   * Makes a GET request.
   *
   * @param {Object} options
   * @return {Void}
   */
  get: function(options) {
    options.method = 'GET';

    this.request(options);
  },

  /**
   * Makes a POST request, automatically injecting the CSRF token.
   *
   * @param {Object} options
   * @return {Void}
   */
  post: function(options) {
    if (!options.hasOwnProperty('method')) {
      options.method = 'POST';
    }

    if (!options.hasOwnProperty('data')) {
      options.data = {};
    }

    options.data = csrf.inject(options.data);

    this.request(options);
  },

  /**
   * Makes a PUT request, automatically injecting the CSRF token.
   *
   * @param {Object} options
   * @return {Void}
   */
  put: function(options) {
    options.method = 'PUT';

    this.post(options);
  },

  /**
   * Makes a DELETE request, automatically injecting the CSRF token.
   *
   * @param {Object} options
   * @return {Void}
   */
  delete: function(options) {
    options.method = 'DELETE';

    this.post(options);
  }

};

/**
 * Browser sniffing library. Also tests feature support.
 *
 * This library should only be used as a small replacement for Modernizr, in
 * situations where Modernizr is too huge to be used, but some basic browser
 * sniffing and feature testing is required.
 *
 * User agent sniffing is unadvisable, and feature testing is a much better way
 * to find out if something can be used, but in certain cases it's inevitable,
 * which is why this library provides it.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

/**
 * Sniffs the user-agent. Taken from the jQuery Migrate plugin.
 *
 * `navigator.userAgent` should be passed to this function.
 *
 * @param {String} userAgent
 * @return {Object}
 */
var sniffUserAgent = function(userAgent) {
  var match;

  userAgent = userAgent.toLowerCase();

  match = /(chrome)[ \/]([\w.]+)/.exec(userAgent) ||
    /(webkit)[ \/]([\w.]+)/.exec(userAgent) ||
    /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(userAgent) ||
    /(msie) ([\w.]+)/.exec(userAgent) ||
    userAgent.indexOf('compatible') < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(userAgent) ||
    [];

  return {
    browser: match[1] || '',
    version: match[2] || '0'
  };
};

/**
 * Returns a variable that defines the current browser, just like `$.browser`
 * used to do in older jQuery versions.
 *
 * @return {Object}
 */
var getBrowser = function() {
  var matched = sniffUserAgent(navigator.userAgent);

  var browser = {};

  if (matched.browser) {
    browser[matched.browser] = true;
    browser.version = matched.version;
  }

  /**
   * Chrome is Webkit, but Webkit is also Safari.
   */
  if (browser.chrome) {
    browser.webkit = true;
  } else if (browser.webkit) {
    browser.safari = true;
  }

  return browser;
};

/**
 * An object containing various tests which may be called from the public
 * `supports()` method.
 *
 * @type {Object}
 */
var supportTests = {

  /**
   * Checks whether the user's browser natively supports placeholders.
   *
   * @return {Boolean}
   */
  nativePlaceholders: function() {
    var test = document.createElement('input');

    if ('placeholder' in test) {
      return true;
    } else {
      return false;
    }
  },

  /**
   * Checks whether the user's browser supports the HTML5 Files API.
   *
   * Test taken from the Modernizr library.
   *
   * Doesn't work in Safari 5 because of reasons.
   *
   * @return {Boolean}
   */
  filesApi: function() {
    return !!(window.File && window.FileList && window.FileReader);
  },

  /**
   * Checks whether the user's browser supports the XHR2.
   *
   * Test taken from the Modernizr library.
   *
   * @return {Boolean}
   */
  xhr2: function() {
    return !!(window.FormData);
  }

};

module.exports = {

  /**
   * Checks whether a feature is supported in the current browser.
   *
   * Should be passed one of methods available in `supportTests`.
   *
   * @param {String} feature
   * @return {Boolean}
   */
  supports: function(feature) {
    if (supportTests.hasOwnProperty(feature)) {
      return supportTests[ feature ]();
    }

    throw new Error('Test for feature "' + feature + '" is not supported yet.');
  },

  /**
   * Initializes the module.
   *
   * @return {Void}
   */
  initialize: function() {
    this.browser = getBrowser();
  }

};

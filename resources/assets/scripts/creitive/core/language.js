/**
 * This is a base for adding different language definitions for the system,
 * where each module is able to define its own language strings.
 *
 * The current site language is automatically detected and used.
 *
 * Libraries that implement internationalization should call
 * `language.getTranslator()`, to grab a new Translator instance, and use that
 * for setting and getting language strings. For more info on how to use the
 * Translator class, see that class's documentation.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');
var Translator = require('./language/Translator');

/**
 * The currently configured language.
 *
 * @type {String}
 */
var currentLanguage = 'en';

module.exports = {

  /**
   * Gets a new Translator instance.
   *
   * Each plugin should create their own translator and add translations to
   * it, so as to prevent naming conflicts between different plugins.
   *
   * @return {Translator}
   */
  getTranslator: function() {
    var translator = new Translator();

    translator.setLanguageResolver(this);

    return translator;
  },

  /**
   * Updates the current language in use to the specified language. If no
   * language was specified, the "lang" attribute of the <html> element is
   * used. If that isn't defined either, English is set as the default.
   *
   * @param {String} languageCode An ISO 639-1 language code (optional)
   */
  setLanguage: function(languageCode) {
    var htmlLang = $('html').attr('lang') || 'en_US';

    currentLanguage = languageCode || htmlLang;
  },

  /**
   * Returns the current language in use.
   *
   * @return {String} An ISO 639-1 language code
   */
  getLanguage: function() {
    return currentLanguage;
  },

  /**
   * Initializes the module.
   *
   * @return {Void}
   */
  initialize: function() {
    this.setLanguage();
  }

};

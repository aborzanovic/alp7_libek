@extends('layouts.front')

@section('content')
    <section class="hero" style="background-size: cover;">
        <div class="container">
            <p class="hero-text">Libertarijanski klub - Libek, uz podršku fondacija Global Philantropic Trust i Friedrich Naumann Stiftung, raspisuje</p>
            <h2 class="hero-subtitle">konkurs</h2>
            <p class="hero-text">za upis sedme generacije polaznika</p>
            <img src="{{ URL::to('/images/alp-logo.png') }}" width="114" height="135" alt="">
            <h1 class="hero-title">Akademija liberalne politike</h1>
            <a href="{{ URL::action('App\Applications\Http\Controllers\Front\Application\Controller@index') }}" class="btn btn-primary">Prijavi se!</a>
        </div>
    </section>

    <style>

        .font-weight
        {
            font-weight: 600;
        }

        .display-table{
            display: table;
            table-layout: fixed;
        }

        .display-cell{
            display: table-cell;
            vertical-align: middle;
            float: none;
        }

        .boreder-radius
        {
            border-radius: 12px;
        }

        .black-button
        {
            color: #000000 !important;
            border-color: #000000 !important;
            margin-top: 25px;
            margin-bottom: 90px;
        }

        .margin-top
        {
            margin-top: 35px;
        }

        .padding-left
        {
            padding-left: 50px;
        }

        .padding-right
        {
            padding-right: 50px;
        }

        .font-p-size
        {
            font-size: 21px;
        }


    </style>

    <section class="intro">
        <div class="container">
            <article class="col-xs-12">
                <p class="text">Sa velikim zadovoljstvom i ponosom vam predstavljamo sedmu Akademiju liberalne politike – unapređeni dvosemestralni program, koji će se odvijati od <strong>oktobra 2017.</strong> do <strong>juna 2018.</strong> godine.</p>
                <p class="text">Prijavi se na ALP 7 i budi u prilici da stekneš korisna akademska znanja, kao i niz poslovnih, aktivističkih i istraživačkih veština.</p>
                <p class="text">Postani deo Libekove alumni zajednice, upoznaj svog mentora i poboljšaj svoje šanse za zaposlenje u uglednoj domaćoj i stranoj kompaniji, staž u nekom od uticajnih istraživačkih centara ili upis na nekom od renomiranih svetskih univerziteta.</p>
                <p class="text">Polaznici Akademije imaće specijalan kanal apliciranja za stipendirane Master i PhD programe na CEVRO Institute u Pragu, Texas Tech University i Syracuse University u Sjedinjenim Američkim Državama.</p>
            </article>
            <article class="col-md-6">
                <h1 class="title">Glavni program</h1>
                <p class="text">Ugledni domaći i strani profesori, stručnjaci različitih profila, istraživači, društveni aktivisti, preduzetnici i novinari vodiće vas kroz svet najvažnijih društvenih rasprava i sa vama će podeliti svoja viđenja najaktuelnijih globalnih i lokalnih izazova.
                </p>
            </article>
            <article class="col-md-6">
                <h1 class="title">Mentorski program</h1>
                <p class="text">Mogućnost dodatnog usavršavanja kroz individualni rad sa mentorom iz različitih oblasti privrednog, javnog i medijskog života, zavisno od vaših ličnih i profesionalnih preferencija. Svake godine proširujemo listu mentora kako bismo izašli u susret vašim ambicijama.
                </p>
            </article>
        </div>
    </section>

    <section class="coursesIntro">
        <div class="container">
            <h1 class="title">Kreativni program</h1>

            <p class="text">Libekovi istraživači i saradnici organizovaće za vas čitalačke sekcije, projekcije tematskih igranih i dokumentarnih filmova, nagradno pisanje eseja i diskutovati o temama kao što su distopija, bioetika, globalni rizici, bezbednost i privatnost. Određeni broj neobaveznih sesija osmišljavaju sami polaznici, zavisno od svojih interesovanja.
            </p>
        </div>
    </section>

    <div class="row display-table" style="height: 600px;">
        <div class="col-md-6 col-xs-6 coursesIntro--alp6 display-cell" style=" height: 100%; background-size:cover; background-image: url(/images/ALP7-1.jpg) ">

        </div>
        <div class="col-md-5 display-cell" style="">
            <div class="padding-left" >
                <h1 style="font-size: 40px;"><b>POREDAK SLOBODE</b></h1>
                <p class="text " style="font-size: 21px;">
                    Tokom ovog novog kursa razgovaraćemo o političkoj filozofiji liberalizma
                    kao i o svim relevantnim kritičkim pristupima koji se bave društvenim,
                    ekonomskim i političkim pitanjima.
                </p>


            </div>


        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row display-table" style="height: 600px;">

        <div class="col-md-1"></div>
        <div class="col-md-5 display-cell" style="height: 100%; text-align: right">
            <div class="padding-right" >
                <h1 style="font-size: 40px;"><b>POP FOLK</b></h1>
                <h2 style="font-size: 30px;"><b>Kurs o savremenom populizmu</b></h2>
                <p class="text " style="font-size: 21px;">
                    Velika novina na ALP7 kojom ulazimo u složeni svet savremenog populizma.
                    Upoznaćemo se sa glavnim idejama, društvenim pokretima, političkim partijama i
                    populističkim liderima iz Evrope i Amerike. Relevantne teme koje vas posebno zanimaju obrađujete
                    i prezentujete individualno ili timski uz stručnu podršku naših saradnika.
                </p>


            </div>


            <!--<p class="text">Istraživački kursevi spadaju u obavezne aktivnosti za polaznike ALP 5. U okviru <em>Tehnika istraživanja javnog mnjenja</em>, u prvom, i <em>Pisanja predloga javnih politika</em>, u drugom semestru, pod stalnim mentorstvom Libekovih istraživača, sprovodićete istraživanja javnog mnjenja o relevantnim društvenim pitanjima i pisaćete predloge reformskih javnih politika.</p>
            <p class="text">U svakom od dva semestra imaćete mogućnosti da pohađate i po jedan novi, specijalizovani teorijski kurs: <em>Kapitalizam i sloboda</em>, u prvom, i <em>Objektivizam John Galt</em>, u drugom. Pohađanje <strong>jednog</strong> od ta dva kursa je obavezno za sve polaznike ALP 5, dok će određeni broj mesta na svakom od dva kursa biti otvoren za odabrane alumniste Libeka.</p>
            <p class="text">Značajna novina ovogodišnjeg programa jeste i praktični kurs ekonomskog novinarstva u drugom semestru, tokom koga ćete se osposobiti za pisanje i predstavljanje složenih analitičkih sadržaja, a kako biste razvili sposobnost što jasnije argumentacije i unapredili veštine javnog nastupa, višestruko nagrađivani debateri će za vas u prvom semestru organizovati debatne radionice sa atraktivnim temama. Oba kursa su <strong>izborna</strong> – kako za polaznike ALP 5, tako i alumniste Libeka.</p> -->
        </div>
        <div class="col-md-6 col-xs-6 coursesIntro--alp6 display-cell" style=" height: 100%; background-size:cover; background-image: url(/images/ALP7-2.jpg) ">

        </div>

    </div>

    <div class="row display-table" style="height: 600px;">
        <div class="col-md-6 col-xs-6 coursesIntro--alp6 display-cell" style=" height: 100%; background-size:cover; background-image: url(/images/ALP7-3.jpg) ">

        </div>
        <div class="col-md-5 display-cell" style="">
            <div class="padding-left" >
                <h1 style="font-size: 40px;"><b>ISTRAŽIVAČKI PROGRAM</b></h1>
                <p class="text " style="font-size: 21px;">
                    U okviru Pisanja predloga javnih politika u prvom i Tehnika istraživanja javnog mnjenja
                    u drugom semestru, pod stalnim mentorstvom Libekovih istraživača, sprovodićete istraživanja
                    javnog mnjenja o relevantnim društvenim pitanjima i pisaćete predloge reformskih javnih politika.
                </p>


            </div>

            <!--<p class="text">Istraživački kursevi spadaju u obavezne aktivnosti za polaznike ALP 5. U okviru <em>Tehnika istraživanja javnog mnjenja</em>, u prvom, i <em>Pisanja predloga javnih politika</em>, u drugom semestru, pod stalnim mentorstvom Libekovih istraživača, sprovodićete istraživanja javnog mnjenja o relevantnim društvenim pitanjima i pisaćete predloge reformskih javnih politika.</p>
            <p class="text">U svakom od dva semestra imaćete mogućnosti da pohađate i po jedan novi, specijalizovani teorijski kurs: <em>Kapitalizam i sloboda</em>, u prvom, i <em>Objektivizam John Galt</em>, u drugom. Pohađanje <strong>jednog</strong> od ta dva kursa je obavezno za sve polaznike ALP 5, dok će određeni broj mesta na svakom od dva kursa biti otvoren za odabrane alumniste Libeka.</p>
            <p class="text">Značajna novina ovogodišnjeg programa jeste i praktični kurs ekonomskog novinarstva u drugom semestru, tokom koga ćete se osposobiti za pisanje i predstavljanje složenih analitičkih sadržaja, a kako biste razvili sposobnost što jasnije argumentacije i unapredili veštine javnog nastupa, višestruko nagrađivani debateri će za vas u prvom semestru organizovati debatne radionice sa atraktivnim temama. Oba kursa su <strong>izborna</strong> – kako za polaznike ALP 5, tako i alumniste Libeka.</p> -->
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row display-table" style="height: 600px;">

        <div class="col-md-1"></div>
        <div class="col-md-5 display-cell" style="height: 100%; text-align: right">
            <div class="padding-right" >
                <h1 style="font-size: 40px;"><b>EKONOMSKO NOVINARSTVO</b></h1>
                <p class="text " style="font-size: 21px;">
                    Praktični kurs ekonomskog novinarstva, tokom koga ćete se osposobiti za
                    pisanje i predstavljanje složenih analitičkih sadržaja. Učićete iz iskustva
                    velikog broja urednika i novinara štampanih i elektronskih medija. Uz koordinaciju
                    Biljane Stepanović, glavne i odgovorne urednice magazina Nova ekonomija i direktorke Business
                    Info Group do sada smo ugostili urednike i novinare Bloomberga, N1, B92, Radija slobodna Evropa i druge.
                </p>


            </div>


            <!--<p class="text">Istraživački kursevi spadaju u obavezne aktivnosti za polaznike ALP 5. U okviru <em>Tehnika istraživanja javnog mnjenja</em>, u prvom, i <em>Pisanja predloga javnih politika</em>, u drugom semestru, pod stalnim mentorstvom Libekovih istraživača, sprovodićete istraživanja javnog mnjenja o relevantnim društvenim pitanjima i pisaćete predloge reformskih javnih politika.</p>
            <p class="text">U svakom od dva semestra imaćete mogućnosti da pohađate i po jedan novi, specijalizovani teorijski kurs: <em>Kapitalizam i sloboda</em>, u prvom, i <em>Objektivizam John Galt</em>, u drugom. Pohađanje <strong>jednog</strong> od ta dva kursa je obavezno za sve polaznike ALP 5, dok će određeni broj mesta na svakom od dva kursa biti otvoren za odabrane alumniste Libeka.</p>
            <p class="text">Značajna novina ovogodišnjeg programa jeste i praktični kurs ekonomskog novinarstva u drugom semestru, tokom koga ćete se osposobiti za pisanje i predstavljanje složenih analitičkih sadržaja, a kako biste razvili sposobnost što jasnije argumentacije i unapredili veštine javnog nastupa, višestruko nagrađivani debateri će za vas u prvom semestru organizovati debatne radionice sa atraktivnim temama. Oba kursa su <strong>izborna</strong> – kako za polaznike ALP 5, tako i alumniste Libeka.</p> -->
        </div>
        <div class="col-md-6 col-xs-6 coursesIntro--alp6 display-cell" style=" height: 100%; background-size:cover; background-position-y: 60%; background-image: url(/images/ALP7-4.jpg) ">

        </div>

    </div>

    <div class="row display-table" style="height: 600px; ">
        <div class="col-md-6 col-xs-6 coursesIntro--alp6 display-cell" style=" height: 100%; background-size:cover; background-image: url(/images/ALP7-5.jpg) ">

        </div>
        <div class="col-md-5 display-cell" style="">
            <div class="padding-left" >
                <h1 style="font-size: 40px;"><b>JAVNI NASTUP I PREZENTACIJSKE VEŠTINE</b></h1>
                <p class="text " style="font-size: 21px;">
                    U okviru novog prodornog kursa sa Reljom Deretom naučićete da efektno oblikujete svoje ideje
                    i da ih samouvereno izrazite u najrazličitijim profesionalnim i ličnim situacijama, tako da
                    inspirišu i pokrenu na razmišljanje i akciju, bez obzira da li pričate pred 10 ili 1000 ljudi,
                    ili da za obraćanje imate 20 sekundi ili 2 sata.
                </p>


            </div>

            <!--<p class="text">Istraživački kursevi spadaju u obavezne aktivnosti za polaznike ALP 5. U okviru <em>Tehnika istraživanja javnog mnjenja</em>, u prvom, i <em>Pisanja predloga javnih politika</em>, u drugom semestru, pod stalnim mentorstvom Libekovih istraživača, sprovodićete istraživanja javnog mnjenja o relevantnim društvenim pitanjima i pisaćete predloge reformskih javnih politika.</p>
            <p class="text">U svakom od dva semestra imaćete mogućnosti da pohađate i po jedan novi, specijalizovani teorijski kurs: <em>Kapitalizam i sloboda</em>, u prvom, i <em>Objektivizam John Galt</em>, u drugom. Pohađanje <strong>jednog</strong> od ta dva kursa je obavezno za sve polaznike ALP 5, dok će određeni broj mesta na svakom od dva kursa biti otvoren za odabrane alumniste Libeka.</p>
            <p class="text">Značajna novina ovogodišnjeg programa jeste i praktični kurs ekonomskog novinarstva u drugom semestru, tokom koga ćete se osposobiti za pisanje i predstavljanje složenih analitičkih sadržaja, a kako biste razvili sposobnost što jasnije argumentacije i unapredili veštine javnog nastupa, višestruko nagrađivani debateri će za vas u prvom semestru organizovati debatne radionice sa atraktivnim temama. Oba kursa su <strong>izborna</strong> – kako za polaznike ALP 5, tako i alumniste Libeka.</p> -->
        </div>
        <div class="col-md-1"></div>
    </div>

    <!--
    <section class="courses">
        <div class="courses-row courses-row--large">
            <article class="col-md-6 course course--large course--friedman">
                <h1 class="title">Kapitalizam i sloboda</h1>
                <img src="/images/courses/friedman-logo.png" class="course-image" width="242" height="204" alt="Stay Free to Choose">
                <p class="text">Tokom kursa razmatraćemo centralno pitanje društvenih procesa - zašto se kapitalizam i sloboda nalaze u osnovi svakog razvijenog društva i zašto njihovo odsustvo toliko mnogo košta.</p>
            </article>
            <article class="col-md-6 course course--large course--rand">
                <h1 class="title">Objektivizam John Galt</h1>
                <img src="/images/courses/rand-logo.png" class="course-image" width="234" height="187" alt="Who is John Galt?">
                <p class="text">Odgovarajući na pitanje Ko je Džon Galt upoznaćemo vas sa glavnim pretpostavkama ove filozofske škole libertarijanizma kao i sa njenim praktičnim implikacijama.</p>
            </article>
        </div>
        <div class="courses-row courses-row--small">
            <article class="col-md-3 course course--small course--debate">
                <h1 class="title course-title--small">Debata i javni nastup</h1>
            </article>
            <article class="col-md-3 course course--small course--research">
                <h1 class="title course-title--small">Tehnike istraživanja javnog mnjenja</h1>
            </article>
            <article class="col-md-3 course course--small course--journalism">
                <h1 class="title course-title--small">Ekonomsko novinarstvo</h1>
            </article>
            <article class="col-md-3 course course--small course--policy">
                <h1 class="title course-title--small">Pisanje predloga javnih politika</h1>
            </article>
        </div>
    </section>
        -->
    <section class="info">
        <div class="container">

            <h1 class="title"><b>Uslovi</b> </h1>
            <p class="text">Pravo prijave na konkurs za program ALP 7 ostvaruju svi studenti osnovnih i master studija univerziteta u Srbiji.
            <p class="text">Mesečna rata kotizacije iznosi <strong>2.400 din.</strong> Ukupno postoji devet mesečnih kotizacija, raspoređenih u dva semestra od oktobra 2017. do juna 2018. Moguće je dogovoriti individualnu dinamiku plaćanja mesečnih kotizacija. Kotizacija pokriva glavni, mentorski, kreativni i istraživački program kao i ostale kurseve. <strong>Ove godine dodelićemo tri cele</strong> i dve parcijalne stipendije budućim polaznicima ALP 7 posle završenog drugog kruga selekcije.
            <p class="text"><strong>Kao i do sada, novi kursevi otvoreni su i za sve naše alumniste. Upis alumnista na određeni kurs izvršiće se posle upisa polaznika tekuće generacije Akademije.</strong></p>
            <p class="text">Konkurs za ALP 7 otvoren je do <strong>20. septembra</strong>.</p>
            <p class="text">Program na Akademiji počeće <strong>3. oktobra</strong> predstavljanjem celokupnog dvosemestralnog programa odabranim polaznicima. Kandidate koji uspešno prođu prvi krug selekcije - evaluaciju elektronske prijave, očekuje intervju pred komisijom.</p>
            <p class="text">Nastava će se odvijati u popodnevnim terminima svake nedelje utorkom i četvrtkom u Libekovim prostorijama.</p>
            <p class="text">Za sve dodatne informacije pišite nam na <a href="mailto:jovana.stanisavljevic@libek.org.rs">jovana.stanisavljevic@libek.org.rs</a>.</p>
        </div>
    </section>


    <!--
    <section class="testimonials owl-carousel owl-theme">
        @foreach (trans('testimonials') as $testimonial)
            <article class="testimonial item">
                <div class="container">
                    <img src="/images/testimonials/{{ $testimonial['slug'] }}.png" width="193" height="193" alt="{{ $testimonial['name'] }}">
                    <p class="text">{{ $testimonial['message'] }}</p>
                    <hgroup>
                        <h1 class="testimonial-name">{{ $testimonial['name'] }}</h1>
                        <h2 class="testimonial-signature">{{ $testimonial['signature'] }}</h2>
                    </hgroup>
                </div>
            </article>
        @endforeach
    </section>
    -->
    <footer class="footer">
        <section class="footer-content">
            <!--<img src="{{ URL::to('/images/alp-logo.png') }}" class="footer-alpLogo" width="114" height="135" alt="">
            <h1 class="footer-title">ALP7</h1>-->
            <a style="margin-top: 40px;" href="{{ URL::action('App\Applications\Http\Controllers\Front\Application\Controller@index') }}" class="btn btn-primary">Prijavi se!</a>
        </section>
        <section class="footer-logos">
            <a class="footer-logo footer-logo--libek" href="https://libek.org.rs/">Libertarijanski klub - Libek</a>
            <a class="footer-logo footer-logo--naumann" href="http://www.westbalkan.fnst.org/">Friedrich-Naumann-Stiftung für die Freiheit</a>
        </section>
    </footer>
@stop

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Molimo Vas sačekajte | Akademija liberalne politike 5 - Libertarijanski Klub Libek</title>
        <link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:300&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #b0bec5;
                display: table;
                font-weight: 100;
                font-family: "Roboto Condensed", sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
                max-width: 767px;
            }

            .title {
                font-size: 48px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Previše uzastopnih pokušaja slanja forme, molimo Vas sačekajte malo pa pokušajte ponovo. Hvala na razumevanju.</div>
            </div>
        </div>
    </body>
</html>

<?php

$activeKey = $navigation('admin.main')->getActive();

?>

<nav class="sidebarNav sidebarNav--slideOut">
    <ul class="sidebarNav-list">
        @foreach ($navigation('admin.main')->getItems() as $key => $item)
            <?php $isActive = ($key === $activeKey); ?>
            <li class="sidebarNav-item">
                <a href="{{ $item['href'] }}" class="sidebarNav-link rippleButton {{ $isActive ? 'is-active' : '' }}">
                    <i class="fa fa-fw fa-{{ $item['icon'] }}"></i> {{ $item['label'] }}
                </a>
            </li>
        @endforeach
    </ul>
    <div class="sidebarNav-overlay"></div>
</nav>

@extends('layouts.master')

<?php

$url = URL::action('App\Http\Controllers\Admin\LoginController@login');

?>

@section('layoutContent')
<section class="container admin-login--container">
    <header class="admin-login--header">
        <h1><i class="fa fa-lock icon"></i> {{ trans('login.title') }}</h1>
    </header>

    {!! Form::open(['url' => $url, 'role' => 'form', 'autocomplete' => 'off']) !!}

        {{-- Email address --}}

        <div class="form-group">
            {!! Form::label('email', trans('login.labels.email')) !!}

            <i class="fa fa-envelope email-icon"></i>
            {!! Form::email('email', null, ['autofocus', 'class' => 'form-control', 'placeholder' => trans('login.placeholders.email'), 'tabIndex' => 1]) !!}
        </div>

        @if ($errors->has('email'))
            <div class="alert alert-danger">{{ $errors->first('email') }}</div>
        @endif

        {{-- Password --}}

        <div class="form-group">
            {!! Form::label('password', trans('login.labels.password')) !!}

            <i class="fa fa-lock password-icon"></i>
            {!! Form::password('password', ['class' => 'form-control', 'tabIndex' => 2]) !!}
        </div>

        @if ($errors->has('password'))
            <div class="alert alert-danger">{{ $errors->first('password') }}</div>
        @endif

        {{-- Remember me --}}

        <div class="checkbox">
            <label>
                {!! Form::checkbox('remember_me', '1', Input::has('remember_me'), ['tabIndex' => 3]) !!}
                {{ trans('login.labels.rememberMe') }}
            </label>
        </div>

        {{-- Submit button --}}

        <div class="form-group">
            {!!
                Form::button(
                    trans('login.labels.send.default'),
                    [
                        'class' => 'btn btn-lg btn-primary rippleButton',
                        'tabIndex' => 4,
                        'type' => 'submit',
                        'data-loading-text' => trans('login.labels.send.loading'),
                    ]
                )
            !!}
        </div>

    {!! Form::close() !!}

</section>
@stop

@extends('layouts.admin')

<?php

$url = URL::action('App\Auth\Http\Controllers\Admin\User\Controller@update', ['user' => $user->id]);

?>

@section('content')

{!! $breadcrumbs->render('admin::users.edit', $user) !!}

@if (!$errors->isEmpty())
    <div class="alert alert-danger">{{ trans('common.genericFormError') }}</div>
@endif

@foreach ($successMessages->all() as $message)
    <div class="alert alert-success">{{ $message }}</div>
@endforeach

{!! Form::open(['url' => $url, 'method' => 'put']) !!}

    {{-- Basic configuration --}}

    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('admin/users.panelTitles.basicConfiguration') }}</div>
        <div class="panel-body">

            {{-- Role --}}

            <div class="form-group">
                {!! Form::label('role', trans('admin/users.labels.role'), ['class' => 'control-label']) !!}
                {!! Form::select('role', $roleOptions, $user->role->slug, ['class' => 'form-control']) !!}
            </div>

            @if ($errors->has('role'))
                <div class="alert alert-danger">{{ $errors->first('role') }}</div>
            @endif


            {{-- Email --}}

            <div class="form-group">
                {!! Form::label('email', trans('admin/users.labels.email'), ['class' => 'control-label']) !!}
                {!! Form::email('email', $user->email, ['class' => 'form-control']) !!}
            </div>

            @if ($errors->has('email'))
                <div class="alert alert-danger">{{ $errors->first('email') }}</div>
            @endif


            {{-- Password --}}

            <div class="form-group">
                {!! Form::label('password', trans('admin/users.labels.password'), ['class' => 'control-label']) !!}
                {!! Form::password('password', ['class' => 'form-control']) !!}
                <p class="help-block">{{ trans('admin/users.help.password') }}</p>
            </div>

            @if ($errors->has('password'))
                <div class="alert alert-danger">{{ $errors->first('password') }}</div>
            @endif


            {{-- First name --}}

            <div class="form-group">
                {!! Form::label('first_name', trans('admin/users.labels.firstName'), ['class' => 'control-label']) !!}
                {!! Form::text('first_name', $user->first_name, ['class' => 'form-control']) !!}
            </div>

            @if ($errors->has('first_name'))
                <div class="alert alert-danger">{{ $errors->first('first_name') }}</div>
            @endif


            {{-- Last name --}}

            <div class="form-group">
                {!! Form::label('last_name', trans('admin/users.labels.lastName'), ['class' => 'control-label']) !!}
                {!! Form::text('last_name', $user->last_name, ['class' => 'form-control']) !!}
            </div>

            @if ($errors->has('last_name'))
                <div class="alert alert-danger">{{ $errors->first('last_name') }}</div>
            @endif

        </div>
    </div>

    {{-- Submit button --}}

    <div class="form-group">
        {!!
            Form::button(
                '<i class="fa fa-save"></i> '.trans('admin/users.labels.save.default'),
                [
                    'class' => 'btn btn-lg btn-primary rippleButton',
                    'type' => 'submit',
                    'data-loading-text' => '<i class="fa fa-clock-o fa-spin"></i> '.trans('admin/users.labels.save.loading'),
                ]
            )
        !!}
    </div>

{!! Form::close() !!}

@stop

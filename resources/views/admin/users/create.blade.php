@extends('layouts.admin')

<?php

$url = URL::action('App\Auth\Http\Controllers\Admin\User\Controller@store');

?>

@section('content')

{!! $breadcrumbs->render('admin::users.create') !!}

@if (isset($errors) && !$errors->isEmpty())
    <div class="alert alert-danger">{{ trans('common.genericFormError') }}</div>
@endif

{!! Form::open(['url' => $url]) !!}

    {{-- Basic configuration --}}

    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('admin/users.panelTitles.basicConfiguration') }}</div>
        <div class="panel-body">

            {{-- Role --}}

            <div class="form-group">
                {!! Form::label('role', trans('admin/users.labels.role'), ['class' => 'control-label']) !!}
                {!! Form::select('role', $roleOptions, $defaultRoleOption, ['class' => 'form-control']) !!}
            </div>

            @if ($errors->has('role'))
                <div class="alert alert-danger">{{ $errors->first('role') }}</div>
            @endif


            {{-- Email --}}

            <div class="form-group">
                {!! Form::label('email', trans('admin/users.labels.email'), ['class' => 'control-label']) !!}
                {!! Form::email('email', null, ['class' => 'form-control']) !!}
            </div>

            @if ($errors->has('email'))
                <div class="alert alert-danger">{{ $errors->first('email') }}</div>
            @endif


            {{-- Password --}}

            <div class="form-group">
                {!! Form::label('password', trans('admin/users.labels.password'), ['class' => 'control-label']) !!}
                {!! Form::password('password', ['class' => 'form-control']) !!}
            </div>

            @if ($errors->has('password'))
                <div class="alert alert-danger">{{ $errors->first('password') }}</div>
            @endif


            {{-- First name --}}

            <div class="form-group">
                {!! Form::label('first_name', trans('admin/users.labels.firstName'), ['class' => 'control-label']) !!}
                {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
            </div>

            @if ($errors->has('first_name'))
                <div class="alert alert-danger">{{ $errors->first('first_name') }}</div>
            @endif


            {{-- Last name --}}

            <div class="form-group">
                {!! Form::label('last_name', trans('admin/users.labels.lastName'), ['class' => 'control-label']) !!}
                {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
            </div>

            @if ($errors->has('last_name'))
                <div class="alert alert-danger">{{ $errors->first('last_name') }}</div>
            @endif

        </div>
    </div>

    {{-- Submit button --}}

    <div class="form-group">
        {!!
            Form::button(
                '<i class="fa fa-save"></i> '.trans('admin/users.labels.save.default'),
                [
                    'class' => 'btn btn-lg btn-primary rippleButton',
                    'type' => 'submit',
                    'data-loading-text' => '<i class="fa fa-clock-o fa-spin"></i> '.trans('admin/users.labels.save.loading'),
                ]
            )
        !!}
    </div>

{!! Form::close() !!}

@stop

@extends('layouts.admin')

<?php

$currentUrl = URL::action('App\Auth\Http\Controllers\Admin\User\Controller@index');
$createUrl = URL::action('App\Auth\Http\Controllers\Admin\User\Controller@create');

?>

@section('content')

{!! $breadcrumbs->render('admin::users') !!}

@foreach ($successMessages->all() as $message)
    <div class="alert alert-success">{{ $message }}</div>
@endforeach

<div class="btnUserContainer">
    <a href="{{ $currentUrl }}" class="btn btn-md btn-primary"><i class="fa fa-filter"></i> {{ trans('admin/users.index.links.roles.all') }}</a>
    <a href="{{ $currentUrl }}?roles[]=admin" class="btn btn-md btn-primary"><i class="fa fa-filter"></i> {{ trans('admin/users.index.links.roles.admins') }}</a>
    <a href="{{ $currentUrl }}?roles[]=user" class="btn btn-md btn-primary"><i class="fa fa-filter"></i> {{ trans('admin/users.index.links.roles.users') }}</a>
</div>

<div class="itemList">
    <div class="itemList-actions">
        <div class="pull-right">
            <a href="{{ $createUrl }}" class="itemList-iconLink btn btn-md btn-primary rippleButton">
                <i class="fa fa-fw fa-plus-square"></i>
                <span class="hidden-xs">{{ trans('admin/users.index.links.create') }}</span>
            </a>
        </div>
    </div>
    <div class="itemList-head">
        <div class="col-xs-1 itemList-column">
            {{ trans('admin/users.labels.id') }}
        </div>
        <div class="col-xs-3 itemList-column">
            {{ trans('admin/users.labels.fullName') }}
        </div>
        <div class="col-xs-3 itemList-column">
            {{ trans('admin/users.labels.email') }}
        </div>
        <div class="col-xs-2 itemList-column">
            {{ trans('admin/users.labels.roles') }}
        </div>
    </div>
    <div class="itemList-rootContainer">
        @include('admin.users.index.list', ['users' => $users])
    </div>
</div>

@stop

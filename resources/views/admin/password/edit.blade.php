@extends('layouts.admin')

@section('content')

{!! $breadcrumbs->render('admin::password') !!}

@foreach ($successMessages->all() as $message)
    <div class="alert alert-success">{{ $message }}</div>
@endforeach

@if (!$errors->isEmpty())
    <div class="alert alert-danger">{{ trans('common.genericFormError') }}</div>
@endif

{!! Form::open(['url' => $submitUrl, 'method' => 'put', 'role' => 'form', 'autocomplete' => 'off']) !!}

    {{-- Current password --}}

    <div class="form-group">
        {!! Form::label('current_password', trans('admin/password.labels.currentPassword')) !!}

        {!! Form::password('current_password', ['autofocus', 'class' => 'form-control']) !!}
    </div>

    @if ($errors->has('current_password'))
        <div class="alert alert-danger">{{ $errors->first('current_password') }}</div>
    @endif


    {{-- New password --}}

    <div class="form-group">
        {!! Form::label('new_password', trans('admin/password.labels.newPassword')) !!}

        {!! Form::password('new_password', ['class' => 'form-control']) !!}
    </div>

    @if ($errors->has('new_password'))
        <div class="alert alert-danger">{{ $errors->first('new_password') }}</div>
    @endif


    {{-- New password confirmation --}}

    <div class="form-group">
        {!! Form::label('new_password_confirmation', trans('admin/password.labels.newPasswordConfirmation')) !!}

        {!! Form::password('new_password_confirmation', ['class' => 'form-control']) !!}
    </div>

    @if ($errors->has('new_password_confirmation'))
        <div class="alert alert-danger">{{ $errors->first('new_password_confirmation') }}</div>
    @endif


    {{-- Submit button --}}

    <div class="form-group">
        {!!
            Form::button(
                trans('admin/password.labels.update.default'),
                [
                    'class' => 'btn btn-lg btn-primary rippleButton',
                    'type' => 'submit',
                    'data-loading-text' => trans('admin/password.labels.update.loading'),
                ]
            )
        !!}
    </div>

{!! Form::close() !!}

@stop

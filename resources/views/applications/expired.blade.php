@extends('layouts.front')

@section('content')

<section class="hero" style="background-size: cover;">
    <div class="container">
        <p class="hero-text">Libertarijanski klub - Libek, uz podršku fondacija Global Philantropic Trust i Friedrich Naumann Stiftung, raspisuje</p>
        <h2 class="hero-subtitle">konkurs</h2>
        <p class="hero-text">za upis sedme generacije polaznika</p>
        <img src="{{ URL::to('/images/alp-logo.png') }}" width="114" height="135" alt="">
        <h1 class="hero-title">Akademija liberalne politike</h1>
    </div>
</section>

<section class="form-container">
    <div class="container">
    <div class="alert alert-danger">{!! trans('applications.expiredNote') !!}</div>
    </div>
</section>

<footer class="footer">
    <section class="footer-content">
        <img src="{{ URL::to('/images/alp-logo.png') }}" class="footer-alpLogo" width="114" height="135" alt="">
        <h1 class="footer-title">ALP7</h1>
    </section>
    <section class="footer-logos">
        <a class="footer-logo footer-logo--libek" href="https://libek.org.rs/">Libertarijanski klub - Libek</a>
        <a class="footer-logo footer-logo--naumann" href="http://www.westbalkan.fnst.org/">Friedrich-Naumann-Stiftung für die Freiheit</a>
    </section>
</footer>

@stop

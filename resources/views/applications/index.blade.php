@extends('layouts.front')

@section('content')

<?php

$url = URL::action('App\Applications\Http\Controllers\Front\Application\Controller@store');

?>

<section class="hero" style="background-size: cover;">
    <div class="container">
        <p class="hero-text">Libertarijanski klub - Libek, uz podršku fondacija Global Philantropic Trust i Friedrich Naumann Stiftung, raspisuje</p>
        <h2 class="hero-subtitle">konkurs</h2>
        <p class="hero-text">za upis sedme generacije polaznika</p>
        <img src="{{ URL::to('/images/alp-logo.png') }}" width="114" height="135" alt="">
        <h1 class="hero-title">Akademija liberalne politike</h1>
    </div>
</section>

<section class="form-container">
    <div class="container">
        @if (isset($errors) && !$errors->isEmpty())
            <div class="alert alert-danger">{{ trans('common.genericFormError') }}</div>
        @endif

        {!! Form::open(['url' => $url, 'role' => 'form', 'files' => true]) !!}

            {{-- Personal info --}}

            <h2>{{ trans('applications.titles.personalInfo') }}</h2>

                {{-- Full name --}}

                <div class="form-group">
                    {!! Form::label('full_name', trans('applications.labels.fullName')) !!}
                    {!! Form::text('full_name', null, ['class' => 'form-control form-control--application-form']) !!}
                </div>

                @if ($errors->has('full_name'))
                    <div class="alert alert-danger">{!! trans('applications.errors.fullName') !!}</div>
                @endif


                {{-- Birthdate --}}

                <div class="form-group">
                    {!! Form::label('birthdate', trans('applications.labels.birthdate')) !!}
                    {!! Form::text('birthdate', null, ['class' => 'form-control form-control--application-form']) !!}
                </div>


                {{-- Sex --}}

                <div class="form-group">
                    <?php
                        $sexes = [
                            'm' => trans('applications.sexes.m'),
                            'f' => trans('applications.sexes.f'),
                        ];
                    ?>
                    {{ trans('applications.labels.sex') }}
                    {!! Form::inlineRadios('sex', $sexes, 'm') !!}
                </div>


                {{-- CV --}}

                <div class="form-group">
                    {!! Form::label('cv', trans('applications.labels.cv')) !!}
                    {!! Form::hidden('MAX_FILE_SIZE', 10485760) !!}
                    {!! Form::file('cv') !!}
                </div>

                @if ($errors->has('cv'))
                    <div class="alert alert-danger">{!! trans('applications.errors.cv') !!}</div>
                @endif


            {{-- Contact info --}}

            <h2>{{ trans('applications.titles.contactInfo') }}</h2>

                {{-- Email address --}}

                <div class="form-group">
                    {!! Form::label('email', trans('applications.labels.email')) !!}
                    {!! Form::email('email', null, ['class' => 'form-control form-control--application-form']) !!}
                </div>

                @if ($errors->has('email'))
                    <div class="alert alert-danger">{!! trans('applications.errors.email') !!}</div>
                @endif


                {{-- Phone --}}

                <div class="form-group">
                    {!! Form::label('phone', trans('applications.labels.phone')) !!}
                    {!! Form::text('phone', null, ['class' => 'form-control form-control--application-form']) !!}
                </div>


            {{-- Education --}}

            <h2>{{ trans('applications.titles.education') }}</h2>

                {{-- Faculty --}}

                <div class="form-group">
                    {!! Form::label('faculty', trans('applications.labels.faculty')) !!}
                    {!! Form::text('faculty', null, ['class' => 'form-control form-control--application-form']) !!}
                </div>


                {{-- Area of study --}}

                <div class="form-group">
                    {!! Form::label('area_of_study', trans('applications.labels.areaOfStudy')) !!}
                    {!! Form::textarea('area_of_study', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>


                {{-- History --}}

                <div class="form-group">
                    {!! Form::label('history', trans('applications.labels.history')) !!}
                    {!! Form::textarea('history', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>


                {{-- Impressions --}}

                <div class="form-group">
                    {!! Form::label('impressions', trans('applications.labels.impressions')) !!}
                    {!! Form::textarea('impressions', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>


                {{-- Libek history --}}

                <div class="form-group">
                    {!! Form::label('libek_history', trans('applications.labels.libekHistory')) !!}
                    {!! Form::textarea('libek_history', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>


                {{-- Memberships --}}

                <div class="form-group">
                    {!! Form::label('memberships', trans('applications.labels.memberships')) !!}
                    {!! Form::textarea('memberships', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>


            {{-- Personal attitude --}}

            <h2>{{ trans('applications.titles.personalAttitude') }}</h2>

                {{-- Problems --}}

                <div class="form-group">
                    {!! Form::label('problems', trans('applications.labels.problems')) !!}
                    {!! Form::textarea('problems', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>


                {{-- Solutions --}}

                <div class="form-group">
                    {!! Form::label('solutions', trans('applications.labels.solutions')) !!}
                    {!! Form::textarea('solutions', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>


                {{-- Future --}}

                <div class="form-group">
                    {!! Form::label('future', trans('applications.labels.future')) !!}
                    {!! Form::textarea('future', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>


                {{-- Ego --}}

                <div class="form-group">
                    {!! Form::label('ego', trans('applications.labels.ego')) !!}
                    {!! Form::textarea('ego', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>

            {{-- Other --}}

            <h2>{{ trans('applications.titles.other') }}</h2>

                {{-- Expectations --}}

                <div class="form-group">
                    {!! Form::label('expectations', trans('applications.labels.expectations')) !!}
                    {!! Form::textarea('expectations', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>


                {{-- Reference --}}

                <div class="form-group">
                    {!! Form::label('reference', trans('applications.labels.reference')) !!}
                    {!! Form::textarea('reference', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>


                {{-- Needs --}}

                <div class="form-group">
                    {!! Form::label('needs', trans('applications.labels.needs')) !!}
                    {!! Form::textarea('needs', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>

            {{-- Submit button --}}

            <div class="form-group">
                {!!
                    Form::button(
                        trans('applications.labels.send.default'),
                        [
                            'class' => 'btn btn-lg btn-success',
                            'type' => 'submit',
                            'data-loading-text' => trans('applications.labels.send.loading'),
                        ]
                    )
                !!}
            </div>

        {!! Form::close() !!}

        <div class="alert alert-info">{!! trans('applications.notes.problems') !!}</div>
        <div class="alert alert-info">{!! trans('applications.notes.privacy') !!}</div>
    </div>
</section>

<footer class="footer">
    <section class="footer-content">
        <img src="{{ URL::to('/images/alp-logo.png') }}" class="footer-alpLogo" width="114" height="135" alt="">
        <h1 class="footer-title">ALP7</h1>
    </section>
    <section class="footer-logos">
        <a class="footer-logo footer-logo--libek" href="https://libek.org.rs/">Libertarijanski klub - Libek</a>
        <a class="footer-logo footer-logo--naumann" href="http://www.westbalkan.fnst.org/">Friedrich-Naumann-Stiftung für die Freiheit</a>
    </section>
</footer>

@stop

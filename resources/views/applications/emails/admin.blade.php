<html>
<head>
    <meta charset="utf-8">
    <title>{{ $subject }}</title>
</head>
<body>

<h1>{{ $subject }}</h1>


<h2>{{ trans('applications.titles.personalInfo') }}</h2>

<b>{{ trans('applications.labels.fullName') }}</b>
<br />
{{ $data['fullName'] }}
<br />
<br />

<b>{{ trans('applications.labels.birthdate') }}</b>
<br />
{{ $data['birthdate'] }}
<br />
<br />

<b>{{ trans('applications.labels.sex') }}</b>
<br />
{{ trans('applications.sexes.'.$data['sex']) }}
<br />
<br />

<b>{{ trans('applications.cvInAttachment') }}</b>
<br />


<h2>{{ trans('applications.titles.contactInfo') }}</h2>

<b>{{ trans('applications.labels.email') }}</b>
<br />
{{ $data['email'] }}
<br />
<br />

<b>{{ trans('applications.labels.phone') }}</b>
<br />
{{ $data['phone'] }}
<br />
<br />


<h2>{{ trans('applications.titles.education') }}</h2>

<b>{{ trans('applications.labels.faculty') }}</b>
<br />
{{ $data['faculty'] }}
<br />
<br />

<b>{{ trans('applications.labels.areaOfStudy') }}</b>
<br />
{!! nl2br(htmlspecialchars($data['areaOfStudy'], ENT_QUOTES | ENT_HTML401)) !!}
<br />
<br />

<b>{{ trans('applications.labels.history') }}</b>
<br />
{!! nl2br(htmlspecialchars($data['history'], ENT_QUOTES | ENT_HTML401)) !!}
<br />
<br />

<b>{{ trans('applications.labels.impressions') }}</b>
<br />
{!! nl2br(htmlspecialchars($data['impressions'], ENT_QUOTES | ENT_HTML401)) !!}
<br />
<br />

<b>{{ trans('applications.labels.libekHistory') }}</b>
<br />
{!! nl2br(htmlspecialchars($data['libekHistory'], ENT_QUOTES | ENT_HTML401)) !!}
<br />
<br />

<b>{{ trans('applications.labels.memberships') }}</b>
<br />
{!! nl2br(htmlspecialchars($data['memberships'], ENT_QUOTES | ENT_HTML401)) !!}
<br />
<br />


<h2>{{ trans('applications.titles.personalAttitude') }}</h2>

<b>{{ trans('applications.labels.problems') }}</b>
<br />
{!! nl2br(htmlspecialchars($data['problems'], ENT_QUOTES | ENT_HTML401)) !!}
<br />
<br />

<b>{{ trans('applications.labels.solutions') }}</b>
<br />
{!! nl2br(htmlspecialchars($data['solutions'], ENT_QUOTES | ENT_HTML401)) !!}
<br />
<br />

<b>{{ trans('applications.labels.future') }}</b>
<br />
{!! nl2br(htmlspecialchars($data['future'], ENT_QUOTES | ENT_HTML401)) !!}
<br />
<br />

<b>{{ trans('applications.labels.ego') }}</b>
<br />
{!! nl2br(htmlspecialchars($data['ego'], ENT_QUOTES | ENT_HTML401)) !!}
<br />
<br />


<h2>{{ trans('applications.titles.other') }}</h2>

<b>{{ trans('applications.labels.expectations') }}</b>
<br />
{!! nl2br(htmlspecialchars($data['expectations'], ENT_QUOTES | ENT_HTML401)) !!}
<br />
<br />

<b>{{ trans('applications.labels.reference') }}</b>
<br />
{!! nl2br(htmlspecialchars($data['reference'], ENT_QUOTES | ENT_HTML401)) !!}
<br />
<br />

<b>{{ trans('applications.labels.needs') }}</b>
<br />
{!! nl2br(htmlspecialchars($data['needs'], ENT_QUOTES | ENT_HTML401)) !!}
<br />
<br />

</body>
</html>

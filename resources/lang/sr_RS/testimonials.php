<?php

return [
    [
        'slug' => 'milan-cirkovic',
        'name' => 'Milan Ćirković',
        'signature' => 'The Future of Humanity Institute, Oxford University, jedan od koordinatora Kreativnog programa',
        'message' => 'Kreativni program svojom raznolikošću sadržaja i živahnošću debata predstavlja pravi intelektualni praznik i oazu u pustinji savemenog srpskog obrazovanja.',
    ],
    [
        'slug' => 'lana-avakumovic',
        'name' => 'Lana Avakumović',
        'signature' => 'Alumnistkinja ALP',
        'message' => 'Pored predavanja vrednih pamćenja, mislim da je najveća vrednost ALP-a u tome što okuplja ambiciozne, pametne i entuzijastične mlade ljude, koji su voljni i sposobni da dovedu do promene u društvu.',
    ],
    [
        'slug' => 'natasa-filipovic',
        'name' => 'Nataša Filipović',
        'signature' => 'Direktorka Ovation BBDO i mentorka Libeka',
        'message' => 'Za transformaciju društva znanja nije dovoljno. Promene su moguće samo delanjem i interakcijom pojedinaca koji poznaju lokalne prilike i mladih ljudi koji imaju snage da nose nove ideje.',
    ],
    [
        'slug' => 'slavisa-tasic',
        'name' => 'Slaviša Tasić',
        'signature' => 'Profesor, University of Mary, USA',
        'message' => 'Edukacija utemeljena na idealima slobodnog i otvorenog društva danas je neophodnija nego ikad i Libek nam svima čini veliku uslugu radom na obrazovanju naredne generacije lidera.',
    ],
    [
        'slug' => 'andrea-subotic',
        'name' => 'Andrea Subotić',
        'signature' => 'Alumnistkinja ALP',
        'message' => 'Akademija, između ostalog, pruža čvrste smernice za razumevanje složenih fenomena tržišta, ljudskog delovanja i poretka.',
    ],
    [
        'slug' => 'nemanja-cubrilo',
        'name' => 'Nemanja Čubrilo',
        'signature' => 'Alumnista ALP',
        'message' => 'Ono što sam dobio od ALP mnogo je više od samog znanja, a najdragocenijim smatram povezivanje sa ljudima koji su izuzetni i koji su spremni da svoj aktivizam usmere ka tome da urade dobre i plemenite stvari!',
    ],
];

<?php

return [

    'applicationTitle' => 'Prijava za Akademiju liberalne politike 5',

    'email' => [
        'subject' => 'Prijava za Akademiju liberalne politike 5 [:random]',
        'to' => [
            'address1' => 'jovana.stanisavljevic@libek.org.rs',
            'address2' => 'petar.cekerevac@gmail.com',
            'address3' => 'milos.nikolic@libek.org.rs',
            'address4' => 'trikosh@libek.org.rs',
            'address5' => 'webmonks@hotmail.com',
            'name' => 'Libertarijanski Klub Libek',
        ],
    ],

    'titles' => [
        'personalInfo' => 'Lični podaci',
        'contactInfo' => 'Kontakt informacije',
        'education' => '(Ne)formalno obrazovanje',
        'personalAttitude' => 'Lični stav',
        'other' => 'Ostalo',
    ],

    'labels' => [
        'fullName' => 'Ime i prezime',
        'birthdate' => 'Datum rođenja',
        'sex' => 'Pol',
        'cv' => 'Vaš CV',
        'email' => 'Email adresa',
        'phone' => 'Broj telefona',
        'faculty' => 'Fakultet i smer',
        'areaOfStudy' => 'Za koju oblast ste posebno zainteresovani u okviru nauke kojom se bavite?',
        'history' => 'Da li ste do sada pohađali neke kurseve, seminare, radionice, konferencije i ako jeste, koje?',
        'impressions' => 'Koja pozitivna, odnosno negativna iskustva ste imali na kursevima, seminarima i radionicama koje ste do sada pohađali?',
        'libekHistory' => 'Da li ste se do sada prijavljivali na neki od Libekovih programa?',
        'memberships' => 'Članstvo u nekoj organizaciji ili političkoj stranci?',
        'problems' => 'Koji su po Vama glavni problemi s kojima se suočava naše društvo?',
        'solutions' => 'Na koji način mislite da Vi možete da doprinesete rešavanju tih problema?',
        'future' => 'Uzimajući u obzir Vaša dosadašnja interesovanja i iskustva, šta je ono čime biste voleli da se bavite u životu?',
        'ego' => 'Za koje svoje osobine smatrate da Vas čine dobrim kandidatom?',
        'expectations' => 'Koja su Vaša očekivanja od ALP?',
        'reference' => 'Kako ste saznali za ALP?',
        'needs' => 'Imate li neke specijalne potrebe? (ishrana, invaliditet...)',
        'send' => [
            'default' => 'Pošaljite',
            'loading' => 'Slanje u toku...',
        ]
    ],

    'cvInAttachment' => 'CV se nalazi u attachment-u.',

    'sexes' => [
        'm' => 'M',
        'f' => 'Ž',
    ],

    'errors' => [
        'fullName' => 'Morate uneti svoje ime i prezime.',
        'email' => 'Morate uneti svoju email adresu.',
        'cv' => 'Morate dodati validan fajl. Dozvoljene vrste fajlova su: <code>pdf</code>, <code>doc</code>, <code>docx</code>, i <code>odt</code>. Maksimalna dozvoljena veličina fajla je <code>10MB</code>.',
    ],

    'success' => 'Uspešno ste se prijavili za Akademiju liberalne politike 5. Rezultate selekcije ćete dobiti nakon isteka roka za prijave na email adresu koju ste ostavili u prijavi. Za sva dodatna pitanja kontaktirajte nas na <a href="mailto:kontakt@libek.org.rs">kontakt@libek.org.rs</a>. Hvala Vam na interesovanju!',

    'notes' => [
        'problems' => '<strong>NAPOMENA:</strong> Ukoliko imate bilo kakvih problema sa slanjem prijave, slobodno nam pišite na <a href="mailto:admin@libek.org.rs">admin@libek.org.rs</a>, a mi ćemo Vam u najkraćem mogućem roku odgovoriti, kako bismo rešili problem.</h3>',
        'privacy' => '<strong>PRIVATNOST:</strong> Radi zaštite privatnosti kandidata, prikupljeni podaci neće nikada biti prosleđivani trećim licima, niti korišćeni u bilo koju svrhu osim izbora učesnika za seminar. Kontakt informacije će biti korišćene isključivo za komunikaciju sa učesnicima.</h3>',
    ],

    'expiredNote' => 'Nažalost, rok za prijavu za ovaj konkurs je istekao. Ukoliko imate dodatnih pitanja, javite nam se na <a href="mailto:kontakt@libek.org.rs">kontakt@libek.org.rs</a>. Hvala Vam na razumevanju.',

];

<?php

return [

    'hello' => 'Hello, :name',

    'logout' => [
        'default' => 'Logout',
        'loading' => 'Logging out...',
    ],

];

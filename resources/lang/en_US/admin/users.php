<?php

return [

    'module' => 'Users',

    'titles' => [
        'index' => 'Users',
        'create' => 'Create a new user',
        'edit' => 'Edit',
        'delete' => 'Delete',
    ],

    'index' => [
        'links' => [
            'create' => 'Create a new user',
            'edit' => 'Edit',
            'delete' => 'Delete',
            'roles' => [
                'all' => 'All users',
                'admins' => 'Administrators',
                'users' => 'Users',
            ],
        ],
    ],

    'panelTitles' => [
        'basicConfiguration' => 'Basic configuration',
    ],

    'labels' => [
        'id' => 'ID',
        'fullName' => 'Full name',
        'firstName' => 'First name',
        'lastName' => 'Last name',
        'email' => 'Email',
        'password' => 'Password',
        'role' => 'Role',
        'roles' => 'Roles',
        'save' => [
            'default' => 'Save',
            'loading' => 'Saving...',
        ],
    ],

    'help' => [
        'password' => 'If you don\'t want to change the user\'s password, just leave this field blank.',
    ],

    'confirmDelete' => [
        'message' => 'Are you sure you want to delete this user?',
        'confirm' => [
            'default' => 'Confirm',
            'loading' => 'Deleting...',
        ],
        'cancel' => [
            'default' => 'Cancel',
            'loading' => 'Canceling...',
        ],
    ],

    'successMessages' => [
        'create' => 'You have successfully created a new user.',
        'edit' => 'You have successfully updated this user.',
        'delete' => 'You have successfully deleted the user ":fullName".',
    ],

];

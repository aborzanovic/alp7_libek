# Compass
require "compass/import-once/activate"

# Require any additional compass plugins here.
require "breakpoint-slicer"

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "public/styles"
sass_dir = "resources/assets/styles"
images_dir = "public/images"
javascripts_dir = "public/scripts"

# You can select your preferred output style here (can be overridden via the command line):
output_style = :compressed

# Set default line endings
unix_newlines = true

# Don't generate a sourcemap
sourcemap = :none

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false
